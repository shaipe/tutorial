
import pandas as pd


class xx:

    def modify_time(self, x):
        # DepTime
        str = x.DepTime.replace(".0", "").zfill(4)

        x.DepTime = str[:2] + ":" + str[2:]
        # CRSDepTime
        str = x.CRSDepTime.replace(".0", "").zfill(4)

        x.CRSDepTime = str[:2] + ":" + str[2:]
        # ArrTime
        str = x.ArrTime.replace(".0", "").zfill(4)

        x.ArrTime = str[:2] + ":" + str[2:]
        # CRSArrTime
        str = x.CRSArrTime.replace(".0", "").zfill(4)

        x.CRSArrTime = str[:2] + ":" + str[2:]

        # ActualElapsedTime
        str = x.ActualElapsedTime.replace(".0", "").zfill(4)

        x.ActualElapsedTime = str[:2] + ":" + str[2:]

        # CRSElapsedTime
        str = x.CRSElapsedTime.replace(".0", "").zfill(4)

        x.CRSElapsedTime = str[:2] + ":" + str[2:]

        # AirTime
        str = x.AirTime.replace(".0", "").zfill(4)

        x.AirTime = str[:2] + ":" + str[2:]

        print(str)
        return x


    def test(self):
        print("start")
        df = pd.read_csv("2008.csv")
        print("loaded")

        df['DepTime'] = df['DepTime'].astype('str')
        df['CRSDepTime'] = df['CRSDepTime'].astype('str')
        df['ArrTime'] = df['ArrTime'].astype('str')
        df['CRSArrTime'] = df['CRSArrTime'].astype('str')
        df['ActualElapsedTime'] = df['ActualElapsedTime'].astype('str')
        df['CRSElapsedTime'] = df['CRSElapsedTime'].astype('str')
        df['AirTime'] = df['AirTime'].astype('str')
        df = df[df['DepTime'] != 'nan']
        df = df[df['CRSDepTime'] != 'nan']
        df = df[df['ArrTime'] != 'nan']
        df = df[df['CRSArrTime'] != 'nan']
        df = df[df['ActualElapsedTime'] != 'nan']
        df = df[df['CRSElapsedTime'] != 'nan']
        df = df[df['AirTime'] != 'nan']
        df = df.apply(lambda x: self.modify_time(x), axis=1)
        df.to_csv("2008-1.csv")


if __name__ == '__main__':
    xx().test()

