Java开发
---

Java开发学习步骤

1. 开发环境的搭建
   1. Jdk的安装，目前使用教多的是OpenJdk的版本，具体安装请参照 [JDK安装](./jdk.md)
2. 开发工具配置
   1. Java开发工具推荐使用 IDEA [IDEA下载地址](https://www.jetbrains.com/idea/download/)
   2. 使用Maven作为Java开发过程中包依赖管理使用  [Maven使用教程](./maven.md)
3. 示例Demo的开发
   1. DemoGit地址[HelloWord](https://gitee.com/shaipe/java_examples)



## 开发环境搭建

### Mac下开发环境搭建

1. Mac自带的jdk版本老了，所以需要到oracle官网（http://www.oracle.com）去下载新的jdk，具体下载那个版本看个人需求，然后安装。（傻瓜式下一步）安装完成之后打开 终端Terminal,执行命令：java -version 即可查看到我们所安装的jdk版本

2. 安装jdk成功之后，我们还需要配置jdk环境变量。

在Terminal 中使用命令whereis java可以看到一个路径，但这个路径并不是配置中需要的路径，这里我们可以直接使用命令：/usr/libexec/java_home -v 查看我们的jdk真实路径，复制下来。

```bash
# 查看java的运行路径
> whereis java
/usr/bin/java
# 查看java的路径
> ls -l /usr/bin/java
lrwxr-xr-x  1 root  wheel  74 10  6 19:04 /usr/bin/java -> /System/Library/Frameworks/JavaVM.framework/Versions/Current/Commands/java
# 查看java_home的真实路径
> /usr/libexec/java_home 
/Library/Java/JavaVirtualMachines/jdk-10.0.2.jdk/Contents/Home
```