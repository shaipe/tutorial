# Elasticsearch

Elasticsearch (ES) 是一个实时的分布式搜索和分析引擎，它可以用于全文搜索，结构化搜索以及分析。它是一个建立在全文搜索引擎 Apache Lucene 基础上的搜索引擎，使用 Java 语言编写.它具有以下特点：

查询：允许执行和合并多种类型的搜索 — 结构化、非结构化、地理位置、度量指标 — 搜索方式随心而变。
分析：Elasticsearch 聚合让您能够从大处着眼，探索数据的趋势和模式，实时分析。
速度：很快，可以做到亿万级的数据，毫秒级返回，分布式实时文件存储，并将 每一个字段 都编入索引。
文档导向，所有的对象全部是文档
可扩展性：可以在笔记本电脑上运行，也可以在承载了 PB 级数据的成百上千台服务器上运行。
弹性：运行在一个分布式的环境中，从设计之初就考虑到了这一点。
灵活性：具备多个案例场景。支持数字、文本、地理位置、结构化、非结构化，所有的数据类型都欢迎。
高可用性，易扩展，支持集群（Cluster）、分片和复制（Shards 和 Replicas）。见图 2 和图 3
接口友好，支持 JSON

![集群](./images/img002.png)

![分片和复制](/Users/shaipe/workspace/tutorial/java/elk/images/img003.png)

## ES 安装

### ES 下载

[下载地址](https://www.elastic.co/downloads/elasticsearch)

windows选择ZIP，linux选择tar，ubuntu选择DEB

测试服务器是ubuntu，直接下载deb包，然后安装即可

```bash
wget https://artifacts.elastic.co/downloads/elasticsearch/elasticsearch-7.10.0-amd64.deb

sudo dpkg -i elasticsearch-5.2.2.deb
```

#### 2.安装后需要将es服务更新随系统启动

- 对于Debian/Ubuntu系统
   执行: `update-rc.d elasticsearch defaults`
   系统服务控制: `/etc/init.d/elasticsearch start/stop/restart`
- 对于redhat/centos系统
   执行: `chkconfig -add elasticsearch`
   系统服务控制: `service elasticsearch start/stop/restart`
- 若需要修改es启动参数，可直接在`/etc/init.d/elasticsearch`脚本中修改然后从其服务启动



#### ES目录解释

```bash
# ubuntu 安装目录为：/usr/share/elasicsearch
elasticsearch                     -- path.home, es的安装目录
├─bin                             -- ${path.home}/bin, 启动脚本方式目录
├─config                          -- ${path.home}/config, 配置文件目录
├─data                            -- ${path.home}/data, 数据存放目录
│  └─elasticsearch                -- ${path.home}/data/${cluster.name}
├─lib                             -- ${path.home}/lib, 运行程序目录
├─logs                            -- ${path.home}/logs, log目录 
└─plugins                         -- ${path.home}/plugins, 插件目录
    ├─head
    │  └─...
    └─marvel
        └─...
```



### ES安装后的目录

| 类型    | 描述                                                         | 默认路经                         | Setting      |
| ------- | ------------------------------------------------------------ | -------------------------------- | ------------ |
| home    | Elasticsearch home directory or $ES_HOME                     | /usr/share/elasticsearch         |              |
| bin     | Binary scripts including elasticsearch to start a node and elasticsearch-plugin to install plugins | /usr/share/elasticsearch/bin     |              |
| conf    | Configuration files including elasticsearch.yml              | /etc/elasticsearch               | path.conf    |
| conf    | Environment variables including heap size, file descriptors. | /etc/default/elasticsearch       |              |
| data    | The location of the data files of each index / shard allocated on the node. Can hold multiple locations. | /var/lib/elasticsearch           | path.data    |
| logs    | Log files location.                                          | /var/log/elasticsearch           | path.logs    |
| plugins | Plugin files location. Each plugin will be contained in a subdirectory. | /usr/share/elasticsearch/plugins |              |
| repo    | Shared file system repository locations. Can hold multiple locations. A file system repository can be placed in to any subdirectory of any directory specified here. | Not configured                   | path.repo    |
| script  | Location of script files.                                    | /etc/elasticsearch/scripts       | path.scripts |