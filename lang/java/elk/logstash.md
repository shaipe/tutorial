# Logstash

Logstash 主要用于收集服务器日志，它是一个开源数据收集引擎，具有实时管道功能。使用 JRuby 语言编写。可以动态地将来自不同数据源的数据统一起来，并将数据标准化到您所选择的目的地。

主要特点

几乎可以访问任何数据
可以和多种外部应用结合
支持弹性扩展
它由三个主要部分组成，见图 4：

Shipper－发送日志数据
Broker－收集数据，缺省内置 Redis
Indexer－数据写入

Logstash 收集数据的过程主要分为以下三个部分：

输入：数据（包含但不限于日志）往往都是以不同的形式、格式存储在不同的系统中，而 Logstash 支持从多种数据源中收集数据（File、Syslog、MySQL、消息中间件等等）。
过滤器：实时解析和转换数据，识别已命名的字段以构建结构，并将它们转换成通用格式。
输出：Elasticsearch 并非存储的唯一选择，Logstash 提供很多输出选择。



![Logstash基本组成](/Users/shaipe/workspace/tutorial/java/elk/images/img004.png)

[下载地址](https://www.elastic.co/cn/downloads/logstash)

