# ELK

ELK 其实并不是一款软件，而是一整套解决方案，是三个软件产品的首字母缩写，Elasticsearch，Logstash 和 Kibana。这三款软件都是开源软件，通常是配合使用，而且又先后归于 Elastic.co 公司名下，故被简称为 ELK 协议栈.
对于日志来说，最常见的需求就是收集、存储、查询、展示，
开源社区正好有相对应的开源项目：Logstash（收集）、Elasticsearch（存储 + 搜索）、Kibana（展示），
我们将这三个组合起来的技术称之为 ELKStack，
所以说 ELKStack 指的是 Elasticsearch、Logstash、Kibana 技术栈的结合。

#### ELK工作流程

![elk工作流程示意图](./images/image001.png)

#### ELK实现方案

通常情况下我们的服务都部署在不同的服务器上，那么如何从多台服务器上收集日志信息就是一个关键点了。本篇文章中提供的解决方案如下图所示：

图 2. 本文提供的 ELK 实现方案
![elk实现方案示意图](./images/image002.png)

如上图所示，整个 ELK 的运行流程如下：

在微服务（产生日志的服务）上部署一个 Logstash，作为 Shipper 角色，主要负责对所在机器上的服务产生的日志文件进行数据采集，并将消息推送到 Redis 消息队列。
另用一台服务器部署一个 Indexer 角色的 Logstash，主要负责从 Redis 消息队列中读取数据，并在 Logstash 管道中经过 Filter 的解析和处理后输出到 Elasticsearch 集群中存储。
Elasticsearch 主副节点之间数据同步。
单独一台服务器部署 Kibana 读取 Elasticsearch 中的日志数据并展示在 Web 页面。
通过这张图，相信您已经大致清楚了我们将要搭建的 ELK 平台的工作流程，以及所需组件。

### 虚拟机准备

#### Ubuntu 系统
```
docker run -idt \
--name elk \
--restart always \
-v ~/docker/elk/data:/data \
shaipe/ubuntu \
/bin/bash

```

#### 参考资料

[集中式日志系统 ELK 协议栈详解](https://developer.ibm.com/zh/technologies/analytics/articles/os-cn-elk/)


