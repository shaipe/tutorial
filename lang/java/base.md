Java基础功能使用
---

#### 在Eclips中引用本地jar

1. 右击“项目”→选择Properties，在弹出的对话框左侧列表中选择Java Build Path，如下图所示：选择Add External JARs，就可以逐个（也可以选择多个jar，但是限制在同一个文件夹中）添加第三方引用jar包。

2. 右击“项目”→选择Properties，在弹出的对话框左侧列表中选择Java Build Path，弹出如上图所示的对话框，这里选择"Add Library"，弹出如下图所示对话框，选择"User Library"，然后选择→Next