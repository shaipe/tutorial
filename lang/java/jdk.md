# jdk

jdk (java development kit) 目前分为二个版本：
1. OpenJDK（java.net）
2. Java SE Downloads（Oracle)

### 1. openjdk

#### 1.1 openjdk 15 install for mac

1. 下载jdk文件 [download jdk 15](https://jdk.java.net/15/)
2. 下载地址 [openjdk-15.0.1_osx-x64_bin.tar.gz](https://download.java.net/java/GA/jdk15.0.1/51f4f36ad4ef43e39d0dfdbaf6549e32/9/GPL/openjdk-15.0.1_osx-x64_bin.tar.gz)
3. 解压缩文件得到 **jdk-15.0.1.jdk**文件夹
4. 将文件复制到 **/Library/java/JavaVirtualMachines**
5. 测试jdk安装是否成功**java --version**



#### 1.2 bash

```bash
#!/bin/bash
# install openjdk15 shell
curl https://download.java.net/java/GA/jdk15.0.1/51f4f36ad4ef43e39d0dfdbaf6549e32/9/GPL/openjdk-15.0.1_osx-x64_bin.tar.gz
unzip openjdk-15.0.1_osx-x64_bin.tar.gz
copy -r jdk-15.0.1.jdk /Library/java/JavaVirtualMachines
java --version
```

#### 1.4 install jdk 15 for ubuntu

```bash
apt update

apt search openjdk

```