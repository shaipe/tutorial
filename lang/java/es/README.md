

# Docker - 通过容器部署Elasticsearch环境教程

2020-03-11发布：hangge阅读：829

 **ElasticSearch** 是一个基于 **Lucene** 的分布式、高扩展、高实时的搜索与数据分析引擎。之前我写过文章介绍如何在 **CentOS** 下使用官方的原生安装包进行安装（[点击查看](https://www.hangge.com/blog/cache/detail_2811.html)），本文介绍另一种方式：通过 **Docker** 来快速搭建 **Elasticsearch** 环境，使用这种方式可以免去很多安装配置上的麻烦。

### 1，拉取镜像

首先执行如下命令将镜像下载到本地：

```
docker pull docker.elastic.co/elasticsearch/elasticsearch:7.6.0
```



### 2，启动容器 

（1）执行如下命令实例化 **ElasticSearch** 服务：

**注意**： **9200** 是供 **http** 访问端口，**9300** 是供 **tcp** 访问的端口，如果不做端口映射，浏览器就不能访问 **elasticsearch** 的服务。

```
docker run --name es -d -p 9200:9200 -p 9300:9300 -e ``"discovery.type=single-node"` `docker.elastic.co/elasticsearch/elasticsearch:7.6.0
```



（2）如果需要实现数据持久化的话，可以在启动时通过 **-v** 参数将 **docker host** 上的目录 **mount** 到 **Elasticsearch** 容器里。

**注意**：其中 **/usr/local/es** 是宿主机的目录地址。

```
docker run --name es -d -v /usr/local/es:/usr/share/elasticsearch/data -p 9200:9200 -p 9300:9300 -e ``"discovery.type=single-node"` `docker.elastic.co/elasticsearch/elasticsearch:7.6.0
```

（3）接着执行 **docker ps** 命令查看下 **ElasticSearch** 容器确实已经成功运行：

[![原文:Docker - 通过容器部署Elasticsearch环境教程](https://www.hangge.com/blog_uploads/202002/2020022922152988879.png)](https://www.hangge.com/blog/cache/detail_2813.html#)

（4）打开浏览器访问 **http://IP:9200**，如果可以看到如下结果则说明服务启动成功：

[![原文:Docker - 通过容器部署Elasticsearch环境教程](https://www.hangge.com/blog_uploads/202002/202002281928485800.png)](https://www.hangge.com/blog/cache/detail_2813.html#)

## 附：ElasticSearch Head 管理界面

  如果需要可视化管理页面的话，我们可以通过 **Docker** 拉取一个 **ElasticSearch Head** 镜像运行。如果觉得麻烦可以使用谷歌浏览器插件：**ElasticSearch Head**，通过谷歌应用商店即可安装。安装完成后，就能查看当前集群的信息。

[![原文:Docker - 通过容器部署Elasticsearch环境教程](https://www.hangge.com/blog_uploads/202002/2020022910575086347.png)](https://www.hangge.com/blog/cache/detail_2813.html#)


原文出自：[www.hangge.com](https://www.hangge.com/) 转载请保留原文链接：https://www.hangge.com/blog/cache/detail_2813.html