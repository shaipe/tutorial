在接触Elasticsearch之前，我对分布式搜索这块也是一脸懵，不过在快速上手之后就掌握了目前全文搜索引擎No.1的基本使用。现在来总结一下它的一些基本概念以及使用docker快速部署并测试步骤。

# Elasticsearch的概念

Elasticsearch是一个分布式搜索服务，提供RESTful API，很多语言都可以通过RESTful API以及9200端口和Elasticsearch进行通信、操作，你可以通过HTTP方法GET、POST、PUT、DELETE进行数据的操作。Elasticsearch是面向文档的，即用户存入的是整个对象以及文档，在Elasticsearch中，用户对文档进行索引、检索，和普通的行列数据库不同，因此这是一种完全不同的思考数据的方式，也是Elasticsearch能够支持复杂全文搜索的原因。

我们来通过下图和普通行列数据库进行对比，对比之前先讲几个名词的概念，以存储员工信息为例，一个**文档**代表一个员工数据，数据存储到Elasticsearch的行为叫做**索引**，每个索引可以包含多个**类型**。

[![概念图](https://www.rossontheway.com/2020/05/24/Elasticsearch%E7%9A%84%E4%BB%8B%E7%BB%8D%E4%B8%8EDocker%E9%83%A8%E7%BD%B2%E5%B9%B6%E6%B5%8B%E8%AF%95API/index.png)](https://www.rossontheway.com/2020/05/24/Elasticsearch的介绍与Docker部署并测试API/index.png)

Elasticsearch中的索引就相当于普通行列数据库的各个数据库，而类型就类似于普通数据库的每张表，文档就相当于表中的每一条记录，属性就相当于每一个字段，这样就方便我们理解Elasticsearch的存储结构。

Elasticsearch使用JSON作为文档的序列化格式，JSON序列化被绝大多数编程语言支持，并且已经成为NoSQL领域的标准格式。下面的JSON文档代表存入Elasticsearch的一个User对象。

```
{
    "email":      "john@smith.com",
    "first_name": "John",
    "last_name":  "Smith",
    "info": {
        "bio":         "Eco-warrior and defender of the weak",
        "age":         25,
        "interests": [ "dolphins", "whales" ]
    },
    "join_date": "2014/05/01"
}
```

# Docker部署Elasticsearch进行测试

和之前一样，为了方便我们进行快速上手，这里仍然使用docker部署的方式进行。首先使用搜索命令在docker hub中查找镜像。

```
docker search elasticsearch
```

由于下载elasticsearch镜像需要指定版本，我们这里采用elasticsearch6.0版本进行测试。

```
docker pull elasticsearch:6.8.9
```

由于默认启动elasticsearch会占用2G的内存，通常在Linux虚拟机上不会配置这么高的内存，因此直接启动会报错，我们手动设置它的启动占用内存参数。

```
docker run -e ES_JAVA_OPTS="-Xms256m -Xmx256m" -p 9200:9200 -p 9300:9300 elasticsearch:6.8.9 ES
```

在这里肯定会有小伙伴发现这个容器启动一会过后便会崩掉，不要慌，这时候我们来查看一下容器的日志，查找原因。

```
docker logs -f 容器id
```

发现错误信息如下：

```
ERROR: [1] bootstrap checks failed
    [1]: max virtual memory areas vm.max_map_count [65530] is too low, increase to at least
```

显示虚拟机的max_map_count参数的值默认给的太小了，我们手动将它设置为262144，首先查看默认的max_map_count的值为多少。

```
cat /proc/sys/vm/max_map_count
65530
```

我们用下面的命令将它设置为262144。

```
sysctl -w vm.max_map_count=262144
vm.max_map_count=262144
```

然后重新启动容器就能正常运行了。

```
docker restart 容器id
```

在浏览器中输入服务器地址或者虚拟机地址，端口为9200，看到如下图的信息，说明Elasticsearch已经正常启动。

[![效果](https://www.rossontheway.com/2020/05/24/Elasticsearch%E7%9A%84%E4%BB%8B%E7%BB%8D%E4%B8%8EDocker%E9%83%A8%E7%BD%B2%E5%B9%B6%E6%B5%8B%E8%AF%95API/browser.png)](https://www.rossontheway.com/2020/05/24/Elasticsearch的介绍与Docker部署并测试API/browser.png)

# 使用Postman调用API接口进行简单搜索测试

## 添加员工信息

我们使用PUT请求，将员工信息保存到Elasticsearch中，每个员工都是employee类型，每个类型都放入索引megacorp中。因此，请求地址的写法如下图所示，请求的请求体使用JSON数据格式封装。

[![添加](https://www.rossontheway.com/2020/05/24/Elasticsearch%E7%9A%84%E4%BB%8B%E7%BB%8D%E4%B8%8EDocker%E9%83%A8%E7%BD%B2%E5%B9%B6%E6%B5%8B%E8%AF%95API/put.png)](https://www.rossontheway.com/2020/05/24/Elasticsearch的介绍与Docker部署并测试API/put.png)

## 检索员工

检索单个员工只需要用GET请求，并且指定员工的id即可。

[![单个员工](https://www.rossontheway.com/2020/05/24/Elasticsearch%E7%9A%84%E4%BB%8B%E7%BB%8D%E4%B8%8EDocker%E9%83%A8%E7%BD%B2%E5%B9%B6%E6%B5%8B%E8%AF%95API/get.png)](https://www.rossontheway.com/2020/05/24/Elasticsearch的介绍与Docker部署并测试API/get.png)

检索所有员工的信息将员工id替换为_search。

[![检索所有员工](https://www.rossontheway.com/2020/05/24/Elasticsearch%E7%9A%84%E4%BB%8B%E7%BB%8D%E4%B8%8EDocker%E9%83%A8%E7%BD%B2%E5%B9%B6%E6%B5%8B%E8%AF%95API/getall.png)](https://www.rossontheway.com/2020/05/24/Elasticsearch的介绍与Docker部署并测试API/getall.png)

有条件的检索我们使用POST请求，并且将条件写到请求体中，用json格式填写筛选条件，下图是一个示例，查出所有last_name是Smith的员工。

[![条件查询](https://www.rossontheway.com/2020/05/24/Elasticsearch%E7%9A%84%E4%BB%8B%E7%BB%8D%E4%B8%8EDocker%E9%83%A8%E7%BD%B2%E5%B9%B6%E6%B5%8B%E8%AF%95API/title.png)](https://www.rossontheway.com/2020/05/24/Elasticsearch的介绍与Docker部署并测试API/title.png)

# 总结

在这里我们进行了Elasticsearch检索功能的入门，更多的功能例如复杂的全文检索小伙伴们可以参照官方文档进行学习。