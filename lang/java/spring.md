# 微服务框架

|组件|版本|备注|
|:--|--|--|
|Jdk|15.0.1|openjdk|
|Spring Cloud| Hoxton.SR8| |
|Spring Boot|2.3.6||
|Spring Cloud Zookeeper|2.2.3.RELEASE||
|Spring Cloud Gateway|2.2.5.RELEASE||
|Spring Cloud Config|2.2.5.RELEASE||
|Mysql-connector|8.0.22|MySQL驱动|
|mybatis-spring-boot-starter|2.1.3||
||||
||||

1. 开发环境数据库
2. ZooKeeper 
3. 中央库-Maven仓库
4. 模块的命名
5. Demo是否需要登录
6. 规范未定，模块化如何模块化？模块的命名具体的命名？比如加个前缀的定义？
7. 项目规范



前端

1. 需要的接口





本周关键成果

1. 项目规范1.0版本 100% 袁
2. 配置中心（Config）搭建 100% 袁
3. 注册中心服务（zookeeper)搭建 100% 吴袁
4. 项目集成MyBatis与MySql数据库的交互 100%   邓
5. 集成基于MyBatis的代码生成工具 100%
6. 集成接口文档工具（Swagger）100%
7. 代码仓库Gitlab环境搭建 100%
8. 项目完成与配置中心、注册中心、网关之间的互通 100%
9. 基于Vue3.0 搭建开发配置环境并完成登录和框架页面 100%
10. 与产品部讨论界面规范 80%
11. 基于Vue的前端开发规范 100% 



下周

1. 前后端完成用户登录
2. 通过界面实现菜单（多级）袁良锭  + 用户管理（只管用户名 + 密码）吴锋
3. 实现简化静态表单 + 数据列表 (文章) 邓建波
4. Jmeter对接口进行压力测试 （谢）

每个应用可以至少独立部署2个

下周四每个人提交一份蓝图



接口规范和定义？

| 名称     | 说明                                                         | 备注                                                 |
| -------- | ------------------------------------------------------------ | ---------------------------------------------------- |
| 风格     | Restful （GET《查询 》、POST《新增》、DELETE《删除》、PUT《修改》） |                                                      |
| 数据格式 | JSON                                                         |                                                      |
| URL规范  | 名词级串联，用复数词态                                       |                                                      |
| 请求参数 | url参数、                                                    |                                                      |
| 返回参数 | {code: int, data: object, message: string}                   | code大范围与httpstatus一至，错误代码根据具体业务协定 |
|          |



服务怎么分 - 服务的颗粒度需要到什么级别？



分几层？

| 文件夹名   | 名称       | 备注                                       |
| ---------- | ---------- | ------------------------------------------ |
| Controller | 控制层     | 需要有完整的错误处理，包括错误和正确的返回 |
| BizService | 业务服务层 | 事务在此层定义                             |
| SpiService | 内部服务层 | 小颗粒度数据逻辑，涉及单表业务             |
| Repository | 数据访问层 | 包括局部缓存方案                           |

全局异常处理？



日志？



工具类， Utils？

| 名称     | 备注 |
| -------- | ---- |
| Json     |      |
| 加解密   |      |
| 日期     |      |
| 验证     |      |
| IP       |      |
| Redis    |      |
| 上传下载 |      |
| 消息队列 |      |
|          |      |

模块的命名规范 - 袁良锭

基于idea的代码规范配置 - 袁良锭





























### Spring Cloud

version: Hoxton.SR8

#### Gitlab

docker run -d \
-p 443:443 \
-p 80:80 \
-p 222:22 \
--name gitlab \
--restart always \
-v /srv//gitlab/config:/etc/gitlab \
-v /srv/gitlab/logs:/var/log/gitlab \
-v /srv/gitlab/data:/var/opt/gitlab \
gitlab/gitlab-ce

pwd: siexiepeng