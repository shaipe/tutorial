# 日常使用中的问题记录

### 1. 应用程序池“xxxxx”提供服务的进程在与 Windows Process Activation Service 通信时出现严重错误。该进程 ID 为“8648”。数据字段包含错误号。

Reply: 【终极解决方案】为应用程序池“XXX”提供服务的进程在与 Windows Process Activation Service 通信时出现严重错误。该进程 ID 为“XXXX”。数据字段包含错误号。 - jackchain - 博客园
[http://www.cnblogs.com/qidian10/p/6028784.html](http://www.cnblogs.com/qidian10/p/6028784.html) 

#### 解决应用程序特定权限设置并未向在应用程序容器
[https://jingyan.baidu.com/article/c275f6ba3d4d57e33d7567bd.html](https://jingyan.baidu.com/article/c275f6ba3d4d57e33d7567bd.html)

#### 解决Runtimebroker进程启动和激活权限无法编辑
[解决Runtimebroker进程启动和激活权限无法编辑](https://jingyan.baidu.com/article/2d5afd69db97a385a2e28eae.html)
