//! 简单的Socket连接程序

// 引入需要使用到的标准库
use std::io::prelude::*;
use std::net::{Shutdown, TcpListener, TcpStream};
use std::thread;

/// 对接收到客户端请求流进行处理
fn handle_client(mut stream: TcpStream) {
    // 打印有连接请求进来
    println!("connected client! {:?}", stream);
    // 定义接收缓冲
    let mut buffer = [0; 1024];
    while match stream.read(&mut buffer) {
        Ok(size) => {
            // 接收信息转为字符
            let res = format!("{}", String::from_utf8_lossy(&buffer[0..size]));
            println!("recieved {:?}", res);
            // echo 回复
            stream
                .write(format!("response :: {}", res).as_bytes())
                .unwrap();
            true
        }
        Err(_) => {
            println!("error {}", stream.peer_addr().unwrap());
            stream.shutdown(Shutdown::Both).unwrap();
            false
        }
    } {}
}

/// 应用程序运行入口方法
fn main() -> std::io::Result<()> {
    // 定义一个Tcp监听器并绑定到当前ip的7878端口
    let listener = TcpListener::bind("127.0.0.1:7888")?;

    // 对来自客户端连接请求进行处理
    for stream in listener.incoming() {
        // 使用分支对接收到的Result流进行成功与否判断
        match stream {
            Ok(s) => {
                thread::spawn(move || {
                    // 成功后调用客户请求处理方法
                    handle_client(s);
                });
            }
            // 错误时在屏幕上打印出错误信息
            Err(e) => println!("{:?}", e),
        }
    }
    Ok(())
}
