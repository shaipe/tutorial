# 常见问题收集



#### 1.  "libc.so.6: version'GLIBC_2.14'not found"

```bash

# 查看glibc版本，发现已升级到2.17版本
ldd --version
strings /lib64/libc.so.6 |grep GLIBC_


下载地址
http://ftp.gnu.org/gnu/glibc/

# ===============================================================
# 如下升级glibc版本到2.18做法
wget http://ftp.gnu.org/gnu/glibc/glibc-2.18.tar.gz
tar -xvf glibc-2.18.tar.gz 
cd glibc-2.18
mkdir build && cd build && ../configure --prefix=/usr && make -j4 && make install
```
### 2.  libssl.so.1.1: cannot open shared object file



**https://pkgs.org/download/libssl.so.1.1()(64bit) 这个方案没有测试过,可以尝试一下** 



**1. Download and extract:**

```
cd ~
wget https://www.openssl.org/source/openssl-1.1.0f.tar.gz
tar -xzf openssl-1.1.0f.tar.gz
```

**2. Compile and install:**

```
cd openssl-1.1.0f
./config
make
sudo make install
```

**Now if you try to run openssl, you will get this error:**

```
/usr/local/bin/openssl version
/usr/local/bin/openssl: error while loading shared libraries: libcrypto.so.1.1: cannot open shared object file: No such file or directory
```

**To fix it, we need to do the next step.**

**3. Create links to libssl:**

```
sudo ln -s /usr/local/lib64/libssl.so.1.1 /usr/lib64/
sudo ln -s /usr/local/lib64/libcrypto.so.1.1 /usr/lib64/
```

**4. Finally create link to new openssl**

```
sudo ln -s /usr/local/bin/openssl /usr/bin/openssl_latest
```

**Here I called it ‘openssl_latest’, you can change it to whatever you want.**

**5. Now let’s check version of our ‘openssl_latest’, which should be ‘1.1.0f’ at the time of writing:**

```
openssl_latest version
OpenSSL 1.1.0f 25 May 2017
```

**Additional tips**

you can also rename the old openssl and rename the latest openssl to the old name.

```
cd /usr/bin/
mv openssl openssl_old
mv openssl_latest openssl
```

### openssl error

要是你的程序依赖原生库，需要设置一个环境变量`CC_x86_64_unknown_linux_musl=x86_64-linux-musl-gcc`，所以完整的编译命令如下

```
CC_x86_64_unknown_linux_musl="x86_64-linux-musl-gcc" cargo build --release --target=x86_64-unknown-linux-musl
```

要是你的程序使用了OpenSSL类库，这是一个麻烦的事情，目前普遍做法是在`Cargo.toml`文件中添加依赖

Cargo.toml

```
[dependencies]
openssl = { version = "0.10", features = ["vendored"] }

```



#### failed to run custom build command for `openssl-sys v0.9.67`
openssl-sys = {version="0.9.67",features = ["vendored"]}
