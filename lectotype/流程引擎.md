# 一、现有框架基本分析
## 1.自定义菜单

+ 1.1列表:
	+ /system/form/list
	+ 对应数据库表:	sys_custom_form
	+ 数据库字段(图片)
	+ 作用:存储自定义表单并指定唯一的form_key , 用户流程定义的时候动态指定表单
		+ ![自定义表单表](images\sys_custom_form.png)

+ 1.2新建: ..

## 2.流程定义	/processDefinition

+ 2.1列表
	
	+ /processDefinition/list 直接通过Activiti列表查询
+ 2.2详情
	+ /processDefinition/getDefinitionXML
	+ eg: 	..?deploymentId=cdae2da1-57c5-11eb-bcc9-7eb27ddcc067&resourceName=CreateWithBPMNJS.bpmn
	+ 参数介绍:deploymentId=部署id, resourceName = 资源名字
	+ 通过流的形式返回xml:

+ 2.3挂起
	+ /processDefinition/suspendOrActiveApply
	+ `{"id":"leave:1:6c935aa5-5718-11eb-8e81-7eb27ddcc067","suspendState":1}`
	+ 参数介绍:id 流程部署id , suspendState状态
	
+ 2.4删除
	
+ /processDefinition/remove/{deploymentId}
	
+ **2.5在线流程绘制**
	+ 绘制:前端绘制
	+ 使用自定义表单
		+ ![使用自定义表单表](images\use_form_key.png)
	+ 使用监听器
		+ 图同上 ,使用表单右边的监听器绑定Java类即可
	+ 保存并部署:
		+ 	/processDefinition/uploadStreamAndDeployment 以文件形式完成
	+ 导出/导入: 略...

+ **2.6缺失的部分**
	+ 需求中是存在模板库的,因此在 在线流程绘制之前对模板库的查询选择模板库,以及模板库的其他操作,需要在之前进行管理
	+ 模板库的管理预想:
		+ 存放: 数据库存放定义好的xml 
		+ 查询: 列表/详情等与流程的列表详情基本一致
		+ 操作: 同样是和详情接口一样加载出xml,然后进行操作
		+ 需要注意的点: 模板是否允许操作 , 判定标准是什么
##3.我的OA(流程申请)
+ 这里以系统的请假进行示例: /workflow/leave 
+ 请假流程业务表:
	+ ![自定义表单表](images\workflow_leave.png)
	+ 表中的instance_id用于查询时候使用
+ 列表
	+ /list
	+ `List<Task> tasks = taskService.createTaskQuery().processInstanceIdIn(collect).list();` collect是List<InstanceId>集合
+ 新增(启动流程)
	+ /	put方法	
	+ 参数示例 : `{"id":null,"type":"病假","title":"muhua的病假申请","reason":"1","leaveStartTime":"2021-01-12","leaveEndTime":"2021-01-13","instanceId":null,"state":null,"createBy":null,"createTime":null,"updateTime":null}`
	+ 新增逻辑
		+ 存储业务数据workflowLeave,并获得返回的业务ID
		+ 查询下节点负责人,并转成特定格式 : String join = '..,..,..'
		+ 启动流程:绑定业务ID和下节点负责人
		+ <pre>ProcessInstance processInstance = processRuntime.start(ProcessPayloadBuilder
                .start()
                .withProcessDefinitionKey("leave")
                .withName(workflowLeave.getTitle())
                .withBusinessKey(id)
                .withVariable("deptLeader",join)
                .build()); </pre>
+ 查看审批进度(高亮显示正在执行节点)
	+ 1./processDefinition/getDefinitions/{instanceId}
		获得部署id和资源名称 
		`{deploymentID: "cdae2da1-57c5-11eb-bcc9-7eb27ddcc067", resourceName: "CreateWithBPMNJS.bpmn"}`
	+ 2./processDefinition/getDefinitionXML 获得流程XMl文件	
		参数: `?deploymentId=cdae2da1-57c5-11eb-bcc9-7eb27ddcc067&resourceName=CreateWithBPMNJS.bpmn`
	+ 3./activitiHistory/gethighLine 获得需要高亮显示节点  
		`?instanceId=2b7abeda-5967-11eb-bdb8-7eb27ddcc067`
	
+ 审批详情
	+ 1./workflow/leave/{id} 获得请假详情信息(业务信息)
	<pre>{"msg":"操作成功","code":200,
		+ "data":[{"taskNodeName":"部门领导审批","createName":"muhua","createdDate":"2021-01-16 15:02:14","formHistoryDataDTO":[{"title":"是111否同意","value":"true"},{"title":"说明信息","value":"null"}]}]}</pre>
	+ 2./historyFromData/ByInstanceId/{businessKey} 获得审批的动态表单详情
		+ businessKey = '51d9dcaa-b9c0-4ed9-b90c-17d02754dbe6'请假表中的对应的id
		+ 逻辑
			+ 1.根据businessKey查询动态表单数据: 查询动态表单  	act_workflow_formdata
		<pre>List<ActWorkflowFormData> ActWorkflowFormData = actWorkflowFormDataService.selectActWorkflowFormDataByBusinessKey(businessKey);</pre>
	
+ 导出/搜索...

##4.待办任务
+ 列表:
	+ /task/list 
+ 审批:
	+ 1./task/formDataShow/{taskID} 
		+ 作用:渲染表单(动态表单数据)
		+ 
	+ 2./workflow/leave/{id} 获得请假详情信息(业务信息)
	
+ **审批确认:**
	+ /task/formDataSave/{taskID} 还需传递动态表单数据
		+ 1.完成任务
		+ 2.审批信息写入数据库(动态表单表)  act_workflow_formdata
		+ ![动态表单表](images\workflow_leave.png)
		+ 注意参数的设置
	+ 这里需要扩展的是动态设置下节点任务人
	

##5.请假历史
+ 列表
	+ /workflow/leave/listAll	查询请假表
+ 审批详情同上


##6.动态表单结合业务表和动态表单数据表三者间关系

![动态表单表](images\relation.png)

# 二、现有框架需要添加/修改部分

## 1.新建流程时流程模板的列表选择等

## 2.流程的动态添加

## 3.流程节点的跳转等

## 4.消息的通知

## 5.加入权限系统




​		