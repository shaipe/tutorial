# 工作流流程设计器选型资料收集



# BPMN.io 全套实现

BOMN官方推出的流程设计器,其功能和代码方面比较全面.代码支持方面推出了多个版本,包含纯JS,Vue,React,angular等支持

> 文章：https://www.vue-js.com/topic/5f6c4af84590fe0031e591ef
>
> source: https://github.com/griabcrh/react_bpmnjs
>
> https://github.com/bpmn-io
>
> Vue: https://github.com/bpmn-io/vue-bpmn
>
> Vue-element: https://github.com/fengli01/vue-bpmn-element

# Goofflow

一个用来在WEB端构建流程图的在线流程设计器，目前有Js原生、Vue和React三个版本。可设计各种流程图、逻辑流图，数据流图，或是应用系统中需要走流程的功能实现。优秀的用户体验使得操作界面很容易上手，无论开者或用户都可轻松使用。

> Demo：https://gooflow.gitee.io/vue/#/
>
> Source: https://gitee.com/gooflow/gooflow
>
> Site: https://gooflow.xyz/
>
> Doc: https://gooflow.xyz/vueDoc/

![img](https://p6.toutiaoimg.com/origin/pgc-image/768268d654d648d1af999789deb5f224)

# gucflow

一款基于Vue写的在线流程设计器

> demo：https://280780363.github.io/gucflow.designer/
>
> source: https://github.com/280780363/gucflow.designer

![img](https://p6.toutiaoimg.com/origin/pgc-image/a81a093e3fb44e7dab610efd78422912)

# Easy-flow VUE + Element

- 支持拖拽添加节点
- 点击线进行设置条件
- 支持给定数据加载流程图
- 支持画布拖拽
- 支持连线样式、锚点、类型自定义覆盖
- 支持力导图

> Demo: http://xiaoka2017.gitee.io/easy-flow/#?_blank
>
> Source: https://gitee.com/xiaoka2017/easy-flow
>
> https://github.com/BiaoChengLiu/easy-flow

![img](https://mp.toutiao.com/mp/agw/article_material/open_image/get?code=NmMxMjE4M2EzNTQyN2MyOTg5YmU2ZmNlNDA5M2ExMzAsMTYyNzA4MzgxMjc2Mw==)

# Flowchart Vue

一款基于既有Vue也有React的流程设计工具,界面美观度不是很好.

> Demo: https://joyceworks.github.io/flowchart-vue/
>
> Source: https://github.com/joyceworks/flowchart-vue

![img](https://p5.toutiaoimg.com/origin/pgc-image/fe46366a67d44844810e30054e141e24)

# Jeecg-boot 整套开发框架

基于BPM的低代码平台！前后端分离架构 SpringBoot 2.x，SpringCloud，Ant Design&Vue，Mybatis-plus，Shiro，JWT，支持微服务。强大的代码生成器让前后端代码一键生成，实现低代码开发！

> source: https://github.com/zhangdaiscott/jeecg-boot
>
> 官网及Demo：http://www.jeecg.com/

![img](https://p5.toutiaoimg.com/origin/pgc-image/c84e6125a4954429817e0dffd7e87fd6)

# Vue-bpmn-element

```
1、调整设计器UI布局，基于vue-elementui美化属性面板
2、支持设置任务变量、表达式、分支条件等，满足90%以上的业务需求
3、支持flowable和activiti
```

> 介绍：https://blog.csdn.net/oJiDiLang/article/details/107254708
>
> Source: https://github.com/fengli01/vue-bpmn-element

![img](https://p6.toutiaoimg.com/origin/pgc-image/e43b5b46ae154a71ac2f60efba14cee3)

# SDesigner

- 兼容Acitiviti5和Activiti6
- 图形化流程绘制
- 根据流程图形生成XML
- 根据XML生成流程图
- 方便灵活扩展

> https://gitee.com/tianya_studio/ESTDesigner
>
> https://gitee.com/tianya_studio/ESTDesigner-VUE

# eeplus 无源码

采用了目前极为流行的扁平化响应式的设计风格，UI框架使用Vue、Element UI、Uniapp，可以完美兼容电脑，pad，手机等多个平台,无需要担心兼容性问题.

> http://demo1.jeeplus.org/#/flowable/process/ModelList



![img](https://mp.toutiao.com/mp/agw/article_material/open_image/get?code=NzQyYmJiZmM1ODE5NDUxYTQ2ZjY3YjZmZDdjYWQ5OGYsMTYyNzA4MzgxMjc2Mw==)



# VFD 有源码，无Demo

- VUE FLOW DESIGN流程设计器，基于Vue + Ant Design Vue + JSPlumb，该组件目的是为了使用在Vue项目中，它前身是原生版流程设计器。

> https://github.com/ZFSNYJ/VFD

# 其他

> 官网：http://www.fhadmin.org/index.html
>
> Source: https://github.com/antvis/G6
>
> Demo: https://www.bthlt.com/go/#/report
>
> https://github.com/fengli01/bpmn-spring-boot
>
> http://xboot.exrick.cn/activiti/model-manage
>
> https://test.agilebpm.cn/index.html
>
> https://juejin.cn/post/6844904138950590471
>
> https://user-gold-cdn.xitu.io/2020/4/27/171b90a3278a4f18
>
> https://joyceworks.github.io/flowchart-vue/
>
> https://www.jianshu.com/p/d6db49c0faad
>
> https://www.jianshu.com/p/36c251c580c4
>
> https://www.jianshu.com/p/13093a1cd6a8



### BPMN.io 全套实现

官网：https://bpmn.io/

Demo: https://demo.bpmn.io/

源代码地址：https://github.com/bpmn-io/bpmn-js

开发资料：    https://github.com/bpmn-io/bpmn-js-examples

国内开发资料： https://www.jianshu.com/p/5b6567fe6f4d （很详细）

很多流程框架的渲染功能在用这个，包括

 jeeplus(http://www.jeeplus.org/)，

Agilebpm(https://test.agilebpm.cn/)

Roy(https://www.ruoyi.vip/)

文章： https://www.vue-js.com/topic/5f6c4af84590fe0031e591ef

source: https://github.com/griabcrh/react_bpmnjs

https://github.com/bpmn-io

Vue: https://github.com/bpmn-io/vue-bpmn

Vue-element: https://github.com/fengli01/vue-bpmn-element



### FH Admin

官网：http://www.fhadmin.org/index.html

可以花几百块钱购买其源码，有Spring 的后端整合也有Vue的前端设计器



### G6 主要是以React实现，Vue需要自行适配

Source: https://github.com/antvis/G6

Demo: https://www.bthlt.com/go/#/report

介绍文章： https://cloud.tencent.com/developer/article/1414633?from=information.detail.vue%E5%8F%AF%E6%8B%96%E6%8B%BD%E6%B5%81%E7%A8%8B%E5%9B%BE



### 基于Flowable的实现

https://github.com/fengli01/bpmn-spring-boot



###  camunda-bpmn.js

Camunda的渲染器，其实就是基于bpmn.io，目前还没梳理出来，camunda.在上面加了什么功能。但是bpmn.io是隶属于camunda的

 

### fowablel-modeler

官网：[https://flowable.com/ ](https://flowable.com/open-source/) flowable配套的，和 

Demo: http://114.67.95.118:8082/modeler

源代码地址：https://github.com/flowable/flowable-engine/tree/master/modules

开发资料：    https://github.com/flowable/flowable-engine/tree/master/docs/userguide/src/zh_CN/bpmn （页面渲染方面的体系化资料目前比较少）

国内开发资料： https://gblfy.blog.csdn.net/article/details/102886712

 

### activiti-designer

最后更新是2015年

Activiti的一些开发插件也很久没更新，idea的插件使用的时候经常报错

 

(不是bpmn规范的，比如canvas直接画出来的那种，没有放到候选项)

 

**初步结论：**

(1) 大厂camunda也基于bpmn.js。在bpmn渲染方面的权威性比较高(官方域名 bpmn.io 的霸气也能感性的看出来。实际上bpmn.io是隶属于camunda的，由camunda来维护)

(2) 在gitee上的开源工作流项目，初步感觉有百分之八九十都是使用bpmn.js做渲染。

(3) Bpmn.js在api方面，数据的交互，是以整个完整的xml进行提交。在和后端接口交互方面实现会比较简单。

(4) bpmn.js的开发资料，也找到了比较完整的中文资料。

 



### 其他：

http://xboot.exrick.cn/activiti/model-manage

https://test.agilebpm.cn/index.html

https://juejin.cn/post/6844904138950590471

https://user-gold-cdn.xitu.io/2020/4/27/171b90a3278a4f18

https://joyceworks.github.io/flowchart-vue/

https://www.jianshu.com/p/d6db49c0faad

https://www.jianshu.com/p/36c251c580c4

https://www.jianshu.com/p/13093a1cd6a8

