# 接口管理系统梳理



### CrapApi

 这是国内的一个开源的API管理系统，提供了文档管理、项目/组织管理相关的功能，在测试管理与代码管理这块是缺失的。

**功能特性**

- 谷歌浏览器插件，支持post、get、put、自定义json调试,支持调试数据保存，支持历史记录查看，中英双语
- HTTPS、私有项目、加密项目、MD5加盐、全面保护数据安全
- 系统完全免费、完全开源
- 系统完全免费、完全开源
- 阿里云安全的云端存储，定时备份数据，支持本地部署

> 项目官网:  [http://api.crap.cn/](http://api.crap.cn/)
>
> 开源地址: https://gitee.com/CrapApi/CrapApi
>
> 插件源码: https://gitee.com/CrapApi/ApiDebug

**功能预览:**

![](./images/api_crap.png)

![](./images/crap_client.png)



### APIPost

ApiPost是一个支持团队协作，并可直接生成文档的API调试、管理工具,支持模拟POST、GET、PUT等常见请求，是后台接口开发者或前端、接口测试人员不可多得的工具

**功能特性**

- 有客户端工具
- 快速生成、一键导出API文档
- 支持携带COOKIE请求，模拟登录
- 清晰的使用流程，跨平台支持

> 项目官网: https://www.apipost.cn/
>
> https://docs.apipost.cn/view/80a19543855094ec#3786481

**功能预览**
![](./images/apipost.png)

![](./images/apipost_view.png)



### ApiZZa


极客专属的接口协作管理工具

**功能特性**

- 模拟请求 - http / websocket，本地，在线接口，都可以调
- 自动生成代码: 指数级提升开发的工作效率
- 云端存储: 安全可靠的云端存储服务，随时随地查看
- 团队协作: 免费的团队协作工具，极致的文档编写体验，加快开发效率
- 测试校验: 对接口进行多方位的测试校验
- 导入导出: 支持Postman，Swagger格式，快速生成文档。可导出多种格式文档
- Mock: 根据文档自动生成返回结果，提供独立URL方便前端测试
- 支持多种文档: 支持多种文档，http / websocket接口文档，markdown说明文档

> 项目官网: https://www.apizza.net/
>

**功能预览**
![](./images/apizza.png)



###  YApi

YApi 是高效、易用、功能强大的 api 管理平台，旨在为开发、产品、测试人员提供更优雅的接口管理服务。它可以帮助开发者轻松创建、发布、以及维护API。除此之外，YApi 还为用户提供了优秀的交互体验，开发人员只需利用平台提供的接口数据写入工具以及简单的点击操作就可以实现接口的管理

**功能特性：**

- 基于 Json5 和 Mockjs 定义接口返回数据的结构和文档，效率提升多倍
- 扁平化权限设计，即保证了大型企业级项目的管理，又保证了易用性
- 类似 postman 的接口调试
- 自动化测试, 支持对 Response 断言
- MockServer 除支持普通的随机 mock 外，还增加了 Mock 期望功能，根据设置的请求过滤规则，返回期望数据
- 支持 postman, har, swagger 数据导入
- **免费开源，内网部署，信息再也不怕泄露了**

> **项目主页：**https://yapi.baidu.com/
>
> **开源地址:**  https://github.com/YMFE/yapi

**功能预览**
![](./images/yapi.png)



### eoLinker

eolinker也挺好用，UI好看，是一个开箱即用的API研发管理方案，0代码实现API自动化测试。

**功能特性：**

- **开箱即用的API研发管理方案：**无需繁琐的配置，支持读取代码注解生成API文档，或者是通过UI界面快速创建全面的API文档。通过Mock API、API变更通知、版本管理等服务，让团队更敏捷。
- **强大的“0代码”API测试 / 自动化测试：**全面支持HTTPS、Restful、Web Service等类型API。强大的API自动化测试和用例管理功能，让你不写代码实现API自动化测试，实时生成测试报告，提高测试覆盖率。
- **实用的团队协作功能：**提供强大的人员权限管理功能，你可以为不同的成员设置允许执行的操作，系统会自动记录成员的所有操作日志。并且可以通过丰富的插件系统来扩展现有的系统功能。
- **开放的平台设计：**一键导入Postman、Swagger等产品数据，让您快速享受到EOLINKER API Studio的服务。您还可以通过Open API将EOLINKER API Studio对接到各类企业内部系统中，实现开发、测试、运维一体化。
- **支持离线的私有化部署：**EOLINKER API Studio支持离线的私有化部署，产品以及数据均储存在企业内部，您可以根据部门/分公司设立相互隔离的工作空间，并且通过Open API对接Jenkins等各类持续集成系统。



> **项目主页：** https://www.eolinker.com/
>
> **开源支持：**[https://www.eolinker.com/#/os/download](https://s.growingio.com/QyNY8O) 
>
> **Github：**https://github.com/eolinker 
>
> **码云：**https://gitee.com/eoLinker
>
> **Blog：**[http://blog.eolinker.com](http://blog.eolinker.com/) 
>
> **视频教程：**http://blog.eolinker.com/#/course/ 
>
> **帮助手册：**[http://help.eolinker.com](http://help.eolinker.com/)

**功能预览**

![](./images/eolinker.png)
![](./images/eolinker_view.png)



### ShowDoc

ShowDoc 是一个非常适合IT团队的在线文档分享工具，它可以加快团队之间沟通的效率。**免费开源**！

**功能特性：**

- **API文档：** 随着移动互联网的发展，BaaS（后端即服务）越来越流行。服务端提供API，APP端或者网页前端便可方便调用数据。用 ShowDoc可以非常方便快速地编写出美观的API文档。
- **数据字典：** 一份好的数据字典可以很方便地向别人说明你的数据库结构，如各个字段的释义等。
- **说明文档：** 你完全可以使用showdoc来编写一些工具的说明书,也可以编写一些技术规范说明文档以供团队查阅

> **项目主页：** https://www.showdoc.cc/

![](./images/showdoc.png)



### XXL-API

XXL-API 是一个强大易用的API管理平台，提供API的”管理”、”文档”、”Mock”和”测试”等功能。现已开放源代码，开箱即用。

**功能特性:**

- 1、极致简单：交互简洁，一分钟上手；
- 2、项目隔离：API以项目为维度进行拆分隔离；
- 3、分组管理：单个项目内的API支持自定义分组进行管理；
- 4、标记星级：支持标注API星级，标记后优先展示；
- 5、API管理：创建、更新和删除API；
- 6、API属性完善：支持设置丰富的API属性如：API状态、请求方法、请求URL、请求头部、请求参数、响应结果、响应结果格式、响应结果参数、API备注等等；
- 7、markdown：支持为API添加markdown格式的备注信息；
- 8、Mock：支持为API定义Mock数据并制定数据响应格式，从而快速提供Mock接口，加快开发进度；
- 9、在线测试：支持在线对API进行测试并保存测试数据，提供接口测试效率；
- 10、权限控制：支持以业务线为维度进行用户权限控制，分配权限才允许操作业务线下项目接口和数据类型，否则仅允许查看；

> 项目官网: https://www.xuxueli.com/xxl-api/
>
> 开源地址: https://gitee.com/xuxueli0323/xxl-api

**功能预览:**

![](./images/xxl-api.png)



### RAP

 `RAP2`是在`RAP1`基础上重做的新项目，它能给你提供方便的接口文档管理、Mock、导出等功能，包含两个组件(对应两个 Github Repository)。

目前RAP2由`阿里妈妈前端团队`研发，由多个合作团队（包括开源社区）在维护。

> 演示地址: http://rap2.taobao.org/
>
> 开源地址: https://github.com/thx/rap2-delos

**功能预览**

![](./images/rap_taobao.png)



### DocWay

简单好用的接口文档管理

**功能特性**
- 多功能编辑器
- Mock Server
- 多版本/团队协作
- 分享&导入导出

> 项目官网: http://www.docway.net/
>
> 演示地址: http://www.docway.net/project/demo

**功能预览**
![](./images/docway.png)



### XAPI

opiping 旗下开源接口管理平台，为程序开发者提供一个灵活，方便，快捷的API管理工具，让API管理变的更加清晰、明朗.

**功能特性**

- 基于golang开发，为您带来极速的体验

- 支持多团队、多项目、多环境，更符合技术开发场景

- 无缝MOCK，让多用户协助更加的便捷

- 功能权限、数据权限，让Api管理更加收放自如

- 支持应用扩展，及定制化服务

> 项目官网: https://xapimanager.opiping.com/
>
> 项目演示地址: https://xapi.smaty.net/
>
> 开源地址: https://github.com/duolatech/xapimanager

**功能预览**
![](./images/xapi.png)



### Api-mom

如母爱一般管理用户的API，以艺术家的品位思考用户体验

> 项目官网: https://www.api-mom.com/

**功能预览**
![](./images/api_mom.png)



### easydoc

**易文档**也是，从需求文档、API文档、部署文档到使用手册，支持多种定制文档编辑器；同时也支持接口在线测试，一键生成文档、调用示例、mock配置。

> **项目主页：** https://easydoc.xyz/
>
> 开源地址: https://github.com/easyapi

**功能预览**
![](./images/easydoc.png)



### doclerver

Restful,Query,Header,Body,Raw信息一应俱全JSON层次采用可视化编辑,结构清晰.
项目版本和接口快照保证你可以实时回溯到任何状态.https,接口加密，文件上传，so easy！
独有的proxy技术加持为您冲破内网的束缚.

**功能特性**

- 接口快照回滚,项目版本控制
- 兼容最新版Swagger,PostMan等平台数据
- 接口文档自动在线生成
- Restful,Query,Header,Body,Raw信息一应俱全,独有的proxy技术加持为您冲破内网的束缚

> 项目官网: http://www.doclever.cn/

**功能预览**

![](./images/doclerver.png)



## API测试的工具

### 1. postman

官网地址：[https://www.getpostman.com](https://www.getpostman.com)

有 Mac, Windows, Linux, and Chrome 各平台对应的软件，可以支持API接口的记录和测试。另外也支持接口的文档化与监控。

### 2. soapui

官网地址：[https://www.soapui.org/](https://www.soapui.org/)

自称是最好的REST & SOAP 测试工具，跟Swagger一样都是smartbear这个公司做的产品。可以支持做接口的功能测试、压力测试、安全测试、模拟测试。

