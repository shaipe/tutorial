# 在线文档选型



http://silianpan.cn/index.php/2019/10/25/uniapp_file_online_preview/



https://www.yozodcs.com/page/index.html



https://gitee.com/253281376/kkFileViewOfficeEdit



https://blog.csdn.net/Aria_Miazzy/article/details/103612401



## 1、[PageOffice]( http://www.zhuozhengsoft.com/PageOffice/ )

​        PageOffice目前支持的Web编程语言及架构有：Java(JSP、SSH、MVC等)，ASP.NET(C#、VB.NET、MVC、Razor等)，PHP，ASP。 

### 1.1、PageOffice对客户端要求

 PageOffice虽然在Web服务器端不需要安装MS Office软件，但要求客户端的电脑必须安装Office软件，以及PageOffice控件。 

 **PageOffice对客户端的软件配置要求** 

| 类型       | 要求                                                         |
| ---------- | ------------------------------------------------------------ |
| 操作系统   | WindowsXP、Windows7、Windows8.1、Windows10等                 |
| 浏览器     | IE8.0及以上版本、搜狗、傲游、MyIE、猎豹、百度、360、世界之窗；Chrome和FireFox |
| Office软件 | Office2003及以上版本，例如Office2003、Office2007、Office2010、<br/>Office2013、Office2016等（非家庭版，非学生版） |

### 1.2、 PageOffice V5 for Java版本

http://www.zhuozhengsoft.com/java/

## 2、SOAOffice

暂无资料

## 3、[DSOframer]( https://gitee.com/liuyuantao/hello_dsoframer )

注：.NET开发

## 4、[NTKO Office]( http://ieoffice.ntko.com/pro/show/mid/1_8/pid/2731 )

​        NTKO OFFICE文档控件能够在IE、Chrome、Firefox等浏览器（能使用IE在线编辑查看文档、其他浏览器没有验证）中直接编辑MS Office、WPS、金山电子表、永中Office等文档并保存到WEB服务器，实现文档和电子表格的统一管理。并具备痕迹保留、模板套红、二维码、pdf及tif阅读等办公自动化系统的必备功能。 

演示地址：http://www.ntko.com:8000/admin/ocv14_test.nsf

## 5、[dzzoffice](https://github.com/zyx0814/dzzoffice)

​         理论上是开源软件

​         在线 Word 、Excel、PPT等文档协作工具。前端做了一套模板管理，用于企业添加自己的常用文档模板，如空白合同。后端支持 office online server，onlyoffice，collaboraoffice 来实现文档预览与协同编辑 

演示地址： http://demo.dzzoffice.com/index.php?mod=word 

## 6、office online

开源

示例：https://github.com/ethendev/wopihost 

## 7、金山文档、腾讯文档

可能无法在内网使用

## 8、[一起写](

### WebOffice

http://www.officectrl.com/

需要下载控件来使用



### **畅写云端Office集成**

**sdk.51changxie.com**



永中

https://www.yozodcs.com/

冰蓝

https://cloud.e-iceblue.cn/welcome.html

毕升

https://bishengoffice.com/