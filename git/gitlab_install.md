# GitLab 安装



## 说明

这是使用官网提供的omnibus packages方式安装（不是从源代码开始，逐步配置）
 GitLab搭建：

- 参见官网[https://about.gitlab.com/installation](https://link.jianshu.com?t=https%3A%2F%2Fabout.gitlab.com%2Finstallation)
- 或，GitLab中文网（翻译不完全）：[https://www.gitlab.com.cn/installation/](https://link.jianshu.com?t=https%3A%2F%2Fwww.gitlab.com.cn%2Finstallation%2F)
   如Debian: [https://about.gitlab.com/installation/](https://link.jianshu.com?t=https%3A%2F%2Fabout.gitlab.com%2Finstallation%2F)

GitLab的基本组成包括：

- gitlab-shell：处理Git命令和修改authorized keys列表
- gitlab-workhorse: 轻量级的反向代理服务器
- nginx：静态web服务器，进行反向代理
- unicorn: Ruby应用的HTTP服务器
- logrotate：日志文件管理工具
- postgresql：数据库
- redis：缓存数据库



## 安装

### 1. 安装依赖包[非必须]



```bash
sudo apt update
sudo apt install -y curl openssh-server ca-certificates
#  邮件（可选）
sudo apt install -y postfix
```

### 2. 下载GitLab软件包

**方式1： 手动下载离线安装包**
 软件包的网址：

- 官网：[https://packages.gitlab.com/gitlab/gitlab-ce](https://packages.gitlab.com/gitlab/gitlab-ce)；
- 清华镜像：[https://mirrors.tuna.tsinghua.edu.cn/gitlab-ce](https://mirrors.tuna.tsinghua.edu.cn/gitlab-ce);

以Debian 9镜像为例（使用清华镜像）



```cpp
wget https://mirrors.tuna.tsinghua.edu.cn/gitlab-ce/debian/pool/stretch/main/g/gitlab-ce/gitlab-ce_10.7.1-ce.0_amd64.deb
sudo dpkg -i gitlab-ce_10.7.1-ce.0_amd64.deb
```

安装成功后显示如图：



**方式2：使用APT/YUM等方式**
 可以参见[清华镜像帮助中的说明](https://mirror.tuna.tsinghua.edu.cn/help/gitlab-ce)，使用：

- Debian 7, 8, 9
- Ubuntu 14.04, 16.04
- RHEL/CentOS

这里以Debian 9为例：



```php
# 添加GitLab的GPG公钥
curl https://packages.gitlab.com/gpg.key 2> /dev/null | sudo apt-key add - &>/dev/null
# 添加软件源
sudo echo "deb http://mirrors.tuna.tsinghua.edu.cn/gitlab-ce/debian stretch main" > /etc/apt/sources.list.d/gitlab-ce.list
# 更新并安装
sudo apt-get update
sudo apt-get install gitlab-ce
```

**方式3：官网教程**
 步骤如下：



```csharp
# 下载脚本并执行
curl https://packages.gitlab.com/install/repositories/gitlab/gitlab-ee/script.deb.sh | sudo bash
# url指搭建的gitlab对外访问的域名；官网下载的是gitlab-ee版本
sudo EXTERNAL_URL="http://gitlab.example.com" apt-get install gitlab-ce
```

说明：curl步骤执行容易因网络等问题而下载失败：推荐下载离线包（前两种方式）

### 3. 个性化设置

初始化GitLab：



```undefined
sudo gitlab-ctl reconfigure
```

初始化成功后（已经启动相关服务），打开浏览器，输入`localhost`，初次启动显示如图，需要设置root密码：

![img](https:////upload-images.jianshu.io/upload_images/447966-78776f1a9d2250b7.png?imageMogr2/auto-orient/strip|imageView2/2/w/1200/format/webp)

浏览器打开GitLab(本地：http://localhost)



配置root密码之后，可以以root用户登录，推荐新建一个用户使用。



![img](https:////upload-images.jianshu.io/upload_images/447966-ebfc6529707092fb.png?imageMogr2/auto-orient/strip|imageView2/2/w/1200/format/webp)

登录GitLab或注册新用户

其他电脑需要登录此网站，可以使用此机的IP，如果配置external_url也可以使用（默认是`http://gitlab.example.com`，无法再公网访问）

#### 4. 使用GitLab

类似GitHub，这里举一个简单的例子：

1. 添加用户的SSH key
2. 在GitLab中建立项目demo；
3. 本地：新建工程配置后并上传



```csharp
git init
Git remote add origin git@gitlab.example.com:username/demo.git
Git add .
Git commit -m "message"
Git push -u origin master (-u用于指定origin主机，下次可使用git push)
```



## GitLab配置说明

GitLab的配置文件是`/etc/gitlab/gitlab.rb`，详细的参数配置可以参见官网。
 重要的有：

- 网址`external_url`: 默认是`'http://gitlab.example.com'`可以在安装时设置，也可以之后再修改
- 数据目录`git_data_dir`: 代码仓库数据存放的位置；默认是`/var/opt/gitlab/git-data`；如果需要修改，县取消注释，然后修改path中值;

对于配置文件修改，需要执行以下命令进行生效：



```bash
# 如果有运行相关服务，先停止
sudo gitlab-ctl stop
# 配置生效，同时会启动相关服务
sudo gitlab-ctl reconfigure
```

## 其他

查看GitLab版本：



```undefined
cat /opt/gitlab/embedded/service/gitlab-rails/VERSION
```

汉化
 参见：[https://gitlab.com/larryli/gitlab](https://gitlab.com/larryli/gitlab)

root密码重置：略

其他相关：GitLab-CI, GitLab Runner





