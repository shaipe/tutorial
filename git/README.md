git操作手册
===

### 客户端安装

[客户端下载地址](https://git-scm.com/downloads)


##### 1. 准备工作
```powershell
# Git 全局设置:
git config --global user.name "谢鹏"  # 把这里的名字改成自己的名字
git config --global user.email "shaipe@sina.com" # 在此修改为自己的邮箱地址
```
##### 2. 第一次获取（克隆项目）

git clone http://192.168.17.231/orion/document.git

##### 3. 切换分支
```powershell
git branch -a # 查看所有的分支信息
git checkout 0.1.0 # 切换到0.1.0分支为工作空间的分支
```
##### 4. 提交推送
```powershell
git pull  # 先拉取线上最线的代码
git add . # 添加本地新增加的文件到修改空间中
git commit -a -m '些处写修改的日志信息'  # 提交本地修改信息到本地git仓库
git push  # 将本地修过过的信息推送到服务器上
```
##### 5. 在现在项目中添加远程地址
```powershell
git remote add origin http://192.168.17.231/orion/document.git  # 添加git地址
git push -u origin master # 推送到主分支
```
##### 6. 处理冲突

以VSCode为例：在左侧的git那个图标的地方会显示需要处理的冲突文件，在每一个文件上点击右键就可以选择是保留本地或远程，也可以双击打开冲突文件对每一个冲突区域进行一一处理。后面需要处理的冲突时再截图展示

##### 7. 设定需要忽略的git配置

在项目的根目录中添加了下名为.gitignore在这个文件中进和配置如下：

```plain
.DS_Store # 此处就是排队项目中所有的这个文件
*.log # 排除所有的log文件
# 忽略 .a 文件
*.a
# 但否定忽略 lib.a, 尽管已经在前面忽略了 .a 文件
!lib.a
# 仅在当前目录下忽略 TODO 文件， 但不包括子目录下的 subdir/TODO
/TODO
# 忽略 build/ 文件夹下的所有文件
build/
# 忽略 doc/notes.txt, 不包括 doc/server/arch.txt
doc/*.txt
# 忽略所有的 .pdf 文件 在 doc/ directory 下的
doc/**/*.pdf
```
##### 8. 带用户名密码的处理

```bash
mkdir repo
cd repo
git init
git config user.email "email"
git config user.name "user"
git pull https://user:password@github.com/name/repo.git master

git config remote.origin.url https://{USERNAME}:PASSWORD}@github.com/{USERNAME}/{REPONAME}.git

http://ops:ops123456@192.168.17.231/documents/prd.git


{
"workdir": "/srv/devops/prd",
"command": ["git pull", "cp -rf ./L1PRD /srv/docker/nginx/html/prd"]
}
```




