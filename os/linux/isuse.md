# Linux系统的常见问题

#### 1. ifconfig command not found

```bash
sudo yum install net-tools
```


### 2. OpenSSL


Download and compile OpenSSL:

cd /usr/local/src
curl --remote-name https://www.openssl.org/source/openssl-1.1.1d.tar.gz
tar -xzvf openssl-1.1.1d.tar.gz
cd openssl-1.1.1d.tar.gz
./config --prefix=/usr/local/openssl/
make
make install
Verify:

openssl version -a