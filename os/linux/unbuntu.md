##  新版ubuntu 使用root用户登录系统

### 一、设置root用户密码
在桌面上右键鼠标选择Open in Terminal打开终端模拟器
执行 ``sudo passwd root``
然后输入设置的密码，输入两次，这样就完成了设置root用户密码了

### 二、修改配置文件

#### 2.1、修改50-ubuntu.conf
执行 ``sudo vim /usr/share/lightdm/lightdm.conf.d/50-ubuntu.conf `` 把配置改为如下所示
```shell
[Seat:*]
user-session=ubuntu
greeter-show-manual-login= true
all-guest=false #这个可以 不用配置
```
#### 2.2、修改gdm-autologin和gdm-password
执行 ``sudo vim /etc/pam.d/gdm-autologin ``注释掉auth required pam_succeed_if.so user != root quiet_success这一行(第三行左右)

```shell
#%PAM-1.0
auth    requisite       pam_nologin.so
#auth   required        pam_succeed_if.so user != root quiet_success
auth    optional        pam_gdm.so
auth    optional        pam_gnome_keyring.so
auth    required        pam_permit.so
```
执行sudo vim /etc/pam.d/gdm-password注释掉 auth required pam_succeed_if.so user != root quiet_success这一行(第三行左右)


```shell
#%PAM-1.0
auth    requisite       pam_nologin.so
#auth   required        pam_succeed_if.so user != root quiet_success
@include common-auth
auth    optional        pam_gnome_keyring.so
@include common-account
```

#### 2.3：修改/root/.profile文件

执行``sudo vim /root/.profile``修改配置文件如下

```bash
# ~/.profile: executed by Bourne-compatible login shells.

if [ "$BASH" ]; then
  if [ -f ~/.bashrc ]; then
    . ~/.bashrc
  fi
fi
tty -s && mesg n || true
mesg n || true
```

### 三、 修改ssh配置

#### 3.1、 首先要安装ssh-server

```bash
sudo apt install ssh-server
```

#### 3.2、 修改ssh配置

```bash
sudo vim /etc/ssh/sshd_conifg
```

找到下面相关配置：

```conf

Authentication:
LoginGraceTime 120
PermitRootLogin prohibit-password
StrictModes yes

```
更改为：

```conf
Authentication:
LoginGraceTime 120
#PermitRootLogin prohibit-password
PermitRootLogin yes
StrictModes yes

```

#### 3.3、 重启ssh

```bash
sudo service ssh restart
```