# 用yum将安装包及其依赖包下载到本地的方法


用yum安装软件默认是不保存软件包的，如果要保存的话只需修改配置文件/etc/yum.conf，将keepcache的值改为1（默认为0）：keepcache=1

即可。软件包默认保存在/var/cache/yum/x86_64/6/base/packages目录下（目录会因机器而异，最好find一下，如我yum install -y screen，这时系统自动下载了screen-1-4.0.3-16.e16.x86_64.rpm包，只需要用命令：finad / -name screen-1-4.0.3-16.e16.x86_64.rpm即可找到该文件的位置 ）



方法一： downloadonly插件
yum install xxx 会下载并且安装软件包，如何实现yum之下载不安装呢?

可以下载yum-downloadonly插件包。

(1) yum install yum-downloadonly

(2) yum install xxx --downloadonly --downloaddir=/xxx -y

安装软件包xxx到/xxx位置，然后记得指定--downloadonly这个。

方法二：yum-utils中的yumdownloader

yum-utils包含着一系列的yum的工具，比如 debuginfo-install, package-cleanup, repoclosure, repodiff, repo-graph, repomanage, repoquery, repo-rss, reposync, repotrack, verifytree, yum-builddep, yum-complete-transaction, yumdownloader, yum-debug-dump 和 yum-groups-manager.

1. 安装yum-utils.noarch

yum -y install yum-utils

2. 使用yumdownloader

yumdownloader httpd

呵呵，就这么简单。

方法三：利用yum的缓存功能

用yum安装了某个工具后，我们想要这个工具的包。那yum安装的过程其实就已经把包给下载了，只是没有保持而已。

所以，我们要做的，是将其缓存功能打开。

1.vi /etc/yum.conf  将其中 keepcache=0改为keepcache=1，保存退出。

2./etc/init.d/yum-updatesd restart

3.yum install httpd

4.cat /etc/yum.conf |grep cachedir
   cachedir=/var/cache/yum

5.跳到上术目录 cd cachedir=/var/cache/yum && tree ./

6.这个时候的目录树中应该可以找到你需要的安装包了。

==================================================================================================

配置yum源结束之后，要清空yum 缓存，并重建yum缓存，执行以下命令：

yum clean all && yum clean metadata && yum clean dbcache && yum makecache && yum update

### 参考资料
https://blog.csdn.net/GX_1_11_real/article/details/80694556