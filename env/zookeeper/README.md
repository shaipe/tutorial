# Zookeeper安装配置

### Zookeeper 典型应用场景

Zookeeper 从设计模式角度来看，是一个基于观察者模式设计的分布式服务管理框架，它负责存储和管理大家都关心的数据，然后接受观察者的注册，一旦这些数据的状态发生变化，Zookeeper 就将负责通知已经在 Zookeeper 上注册的那些观察者做出相应的反应，从而实现集群中类似 Master/Slave 管理模式。

## 启动Zookeeper集群

```bash
# 启动所有Zookeeper服务
docker-compose -f docker-compose-zookeeper.yaml up -d

# 移除所有Zookeeper服务
docker-compose -f docker-compose-zookeeper.yaml rm -sf

```