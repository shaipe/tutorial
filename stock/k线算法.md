

##### 写在前面的话

本人android开发股线图时基于MpAndroidChart开发的，所以后面用到的算法中CandleEntry， Entry等都是MpAndroidChart的API，均使用java实现，算法经过对比验证，和其他证券平台的指标结果一致。
 目前只研究了MA、EMA、BOLL、MACD、KDJ、RSI几种指标。

### 一、MA：

英文(Moving average)的简写，叫移动平均线指标，算法最简单的一个指标。

##### 计算方式：

1.N日MA=N日[收市价](https://links.jianshu.com/go?to=https%3A%2F%2Fbaike.baidu.com%2Fitem%2F%E6%94%B6%E5%B8%82%E4%BB%B7)的总和/N(即算术平均数)
 2.要设置多条[移动平均线](https://links.jianshu.com/go?to=https%3A%2F%2Fbaike.baidu.com%2Fitem%2F%E7%A7%BB%E5%8A%A8%E5%B9%B3%E5%9D%87%E7%BA%BF)，一般参数设置为N1=5,N2=10,N3=20,N4=60,N5=120,N6=250



```csharp
    // n日均线MA,
     public static List<Entry> getMA(List<CandleEntry> entries, int n) {
        List<Entry> result = new ArrayList<>();
        for (int i = 0, len = entries.size(); i < len; i++) {
            if (i < n - 1) {
                continue;
            }
            float sum = 0;
            for (int j = 0; j < n; j++) {
                sum += entries.get(i - j).getClose();
            }
            result.add(new Entry(entries.get(i).getX(), sum / n));
        }
        return result;
    }
```

### 二、EMA：

指数移动平均值。也叫[EXPMA指标](https://links.jianshu.com/go?to=https%3A%2F%2Fbaike.baidu.com%2Fitem%2FEXPMA%E6%8C%87%E6%A0%87)，它也是一种趋向类指标，指数移动平均值是以指数式递减加权的移动平均

##### 计算方式：

EMAtoday=α * Pricetoday + ( 1 - α ) * EMAyesterday;
 其中，α为平滑指数，一般取作2/(N+1)。在计算MACD指标时，EMA计算中的N一般选取12和26天，因此α相应为2/13和2/27



```csharp
   /**
     * EMA算法
     * EMA(N) = 2/(N+1)*C + (N-1)/(N+1)*EMA', EMA'为前一天的ema; 通常N取12和26
     *
     * @param entries
     * @param n
     * @return
     */
    public static List<Entry> getEMA(List<CandleEntry> entries, int n) {
        List<Entry> result = new ArrayList<>();
        float lastEma = entries.get(0).getClose();// 第一个EMA为第一个数据的价格
        result.add(new Entry(0, lastEma));

        float[] emaFactor = getEMAFactor(n);
        for (int i = 1; i < entries.size(); i++) {
            float ema = emaFactor[0] * entries.get(i).getClose() + emaFactor[1] * lastEma;
            result.add(new Entry(entries.get(i).getX(), ema));
            lastEma = ema;
        }
        return result;
    }


   /**
     * 获取EMA计算时的相关系数 (后续多个地方需要这个系数 抽取出来用）
     * @param n
     * @return
     */
    private static float[] getEMAFactor(int n) {
        return new float[]{2f / (n + 1), (n - 1) * 1.0f / (n + 1)};
    }
```



### 三、BOLL:

BOLL指标，其英文全称是“Bollinger Bands”

在所有的指标计算中，BOLL指标的计算方法是最复杂的之一，其中引进了统计学中的标准差概念，涉及到中轨线（MB）、上轨线（UP）和下轨线（DN）的计算。另外，和其他指标的计算一样，由于选用的计算周期的不同，BOLL指标也包括日BOLL指标、周BOLL指标、月BOLL指标年BOLL指标以及分钟BOLL指标等各种类型

##### 计算公式

中轨线=N日的移动平均线
 上轨线=中轨线+两倍的标准差
 下轨线=中轨线－两倍的标准差

以日 BOLL指标计算为例，其计算方法如下:

日BOLL指标的计算公式为:

中轨线=N日的移动平均线

上轨线=中轨线+两倍的标准差

下轨线=中轨线-两倍的标准差

日BOLL指标的计算过程:

(1)计算MA。

MA=N日内的收盘价之和÷N

(2)计算标准差MD。

MD=平方根(N-I)日的(C-MA)的两次方之和除以N

(3)计算MB、UP、DN线。

MB = (N-1)日的MA

UP=MB+k×MD

DN=MB-k×MD

(k为参数，可根据股票的特性来做相应的调整，一般默认为2)

```csharp
   /**
     * 布林带BOLL（n， k） 一般n默认取20，k取2, mb为计算好的中轨线
     * 中轨线MB: n日移动平均线 MA(n)
     * 上轨线：MB + 2*MD
     * 下轨线：MB - 2*MD
     * MD：n日方差
     *
     * @param entries
     * @param n
     * @param k
     * @return
     */
    public static List<Entry>[] getMB(List<CandleEntry> entries, int n, int k) {
        ArrayList<Entry> resultMB = new ArrayList<>();  // 中轨线
        ArrayList<Entry> resultUp = new ArrayList<>(); // 上轨线
        ArrayList<Entry> resultDn = new ArrayList<>(); // 下轨线
        for (int i = 0, len = entries.size(); i < len; i++) {
            if (i < n - 1) {
                continue;
            }
            float sumMB = 0;
            float sumMD = 0;
            for (int j = n - 1; j >= 0; j--) {
                float thisClose = entries.get(i - j).getClose();
                sumMB += thisClose;
            }
            float mb = sumMB / n;
            float x = entries.get(i).getX();
            resultMB.add(new Entry(x, mb));
            for (int j = n - 1; j >= 0; j--) {
                float thisClose = entries.get(i - j).getClose();
                float cma = thisClose - mb; // C-MB
                sumMD += cma * cma;
            }

            float md = (float) Math.pow(sumMD / (n - 1), 1.0 / k); //MD=前n日C-MB的平方和来开根
            resultUp.add(new Entry(x, mb + 2 * md)); // UP=MB+2*MD
            resultDn.add(new Entry(x, mb - 2 * md)); // DN=MB+2*MD
        }
        return new ArrayList[]{resultMB, resultUp, resultDn};
    }
```

### 四、MACD

MACD对技术流投资者的重要性不言而喻。然而，很多资料对其详细算法都语焉不详。尤其是第一天和第二天的MACD的处理方式，很多说法有差别。今天查了查资料，终于搞清楚了其计算方法。用该方法计算理工检测，法因数控等股票MACD，和大智慧或者飞狐交易师上面显示的DIFF,DEA以及MACD完全吻合。

关键的一点是：新股上市首日，其DIFF,DEA以及MACD都为0，因为当日不存在前一日，无法做迭代。而计算新股上市第二日的EMA时，前一日的EMA需要用收盘价（而非0）来计算。另外，需要注意，计算过程小数点后四舍五入保留4位小数，最后显示的时候四舍五入保留3位小数。

具体[计算公式](https://link.zhihu.com/?target=http%3A//www.dtjr.com/Article/Class166/20061114035204.html)及例子如下：

EMA（12）= 前一日EMA（12）×11/13＋今日收盘价×2/13

EMA（26）= 前一日EMA（26）×25/27＋今日收盘价×2/27

DIFF=今日EMA（12）- 今日EMA（26）

DEA（MACD）= 前一日DEA×8/10＋今日DIF×2/10

BAR=2×(DIFF－DEA)



MACD在应用上,，先计算出快速移动平均线即12日的EMA1，和慢速移动平均线，即26日的EMA2,，以这两个数值之间的差值得出DIFF,，然后再求出DIFF的9日[平滑移动平均线](https://link.zhihu.com/?target=https%3A//www.baidu.com/s%3Fwd%3D%E5%B9%B3%E6%BB%91%E7%A7%BB%E5%8A%A8%E5%B9%B3%E5%9D%87%E7%BA%BF%26tn%3DSE_PcZhidaonwhc_ngpagmjz%26rsv_dl%3Dgh_pc_zhidao)DEA.，最后得出MACD=2×(DIFF－DEA). 。

<1>计算12日和26日移动平均线EMA1和EMA2
当日EMA(12)=前一日EMA(12)×11/13＋当日收盘价×2/13
当日EMA(26)=前一日EMA(26)×25/27＋当日收盘价×2/27
<2>计算离差值(DIFF)
DIFF=当日EMA(12)－当日EMA(26)
<3>计算9日离差平均值DEA
当日DEA=前一日DEA×8/10＋当日DIFF×2/10
<4>计算MACD
MACD=2×(DIFF－DEA)

☆ 离差值DIFF和离差平均值DEA是研判MACD的主要工具,，其计算方法比较烦琐,。由于目前这些数值在股市分析软件上都由计算机自动完成,。因此投资者只要了解其运算过程即可，,更重要的是掌握它的研判功能。.另外和其它技术指标一样,，由于选取的计算周期的不同,，[MACD指标](https://link.zhihu.com/?target=https%3A//www.baidu.com/s%3Fwd%3DMACD%E6%8C%87%E6%A0%87%26tn%3DSE_PcZhidaonwhc_ngpagmjz%26rsv_dl%3Dgh_pc_zhidao)也包括日MACD、,周MACD、,月MACD、,年[MACD指标](https://link.zhihu.com/?target=https%3A//www.baidu.com/s%3Fwd%3DMACD%E6%8C%87%E6%A0%87%26tn%3DSE_PcZhidaonwhc_ngpagmjz%26rsv_dl%3Dgh_pc_zhidao),以及5分钟,、15分钟,、30分钟,、60分钟等分时MACD.、常被用于股市研判的是日[MACD指标](https://link.zhihu.com/?target=https%3A//www.baidu.com/s%3Fwd%3DMACD%E6%8C%87%E6%A0%87%26tn%3DSE_PcZhidaonwhc_ngpagmjz%26rsv_dl%3Dgh_pc_zhidao)和周MACD指标,虽然它们计算时的取值有所不同,但计算方法基本相同。.

异同移动平均线，是从双指数移动平均线发展而来的，由快的指数移动平均线（EMA12）减去慢的指数移动平均线（EMA26）得到快线DIF，再用2×（快线DIF-DIF的9日加权移动均线DEA）得到MACD柱。



```csharp
 /**
     * MACD算法：
     * DIF：EMA(short) - EMA(long) 一般short取12，long取26
     * DEA: EMA(DIF, mid), mid一般取9
     * MACD:(DIF-DEA)*2
     *
     * @param entries
     * @param s  short
     * @param l long
     * @param m  mid
     * @return
     */
    public static List[] getMACD(List<CandleEntry> entries, int s, int l, int m) {
        ArrayList<Entry> listDIF = new ArrayList<>();
        ArrayList<Entry> listDEA = new ArrayList<>();
        ArrayList<BarEntry> listMACD = new ArrayList<>();

        float lastEmaS = entries.get(0).getClose();
        float lastEmaL = lastEmaS;
        float lastDIF = 0;
        listDIF.add(new Entry(0, 0));
        listDEA.add(new Entry(0, 0));
        listMACD.add(new BarEntry(0, 0));

        float[] factorShort = getEMAFactor(s);
        float[] factorLong = getEMAFactor(l);
        float[] factorMid = getEMAFactor(m);
        for (int i = 1; i < entries.size(); i++) {
            float x = entries.get(i).getX();
            // 短线EMA
            float valueS = factorShort[0] * entries.get(i).getClose() + factorShort[1] * lastEmaS;
            lastEmaS = valueS;
            // 长线EMA
            float valueL = factorLong[0] * entries.get(i).getClose() + factorLong[1] * lastEmaL;
            lastEmaL = valueL;
            // DIF：EMA(short) - EMA(long)
            float valueDIF = valueS - valueL;
            listDIF.add(new Entry(x, valueDIF));
            // EMA(DIF, mid)
            float valueDEA = factorMid[0] * valueDIF + factorMid[1] * lastDIF;
            listDEA.add(new Entry(x, valueDEA));
            lastDIF = valueDEA;
            // MACD:(DIF-DEA)*2
            listMACD.add(new BarEntry(x, (valueDIF - valueDEA) * 2));
        }
        return new ArrayList[]{listDIF, listDEA, listMACD};
    }

    /**
     * 获取EMA计算时的相关系数
     * @param n
     * @return
     */
    private static float[] getEMAFactor(int n) {
        return new float[]{2f / (n + 1), (n - 1) * 1.0f / (n + 1)};
    }
```



### 五、KDJ

通过一个特定的周期（常为9日、9周等）内出现过的最高价、最低价及最后一个计算周期的收盘价及这三者之间的比例关系，来计算最后一个计算周期的未成熟随机值RSV，然后根据平滑移动平均线的方法来计算K值、D值与J值

#### 计算方式 （摘抄自百度百科）

KDJ的计算比较复杂，首先要计算周期（n日、n周等）的RSV值，即未成熟值，然后再计算K值、D值、J值等。以n日KDJ数值的计算为例，其计算公式为

n日RSV=（Cn－Ln）/（Hn－Ln）×100
 公式中，**Cn为第n日收盘价**；**Ln为n日内的最低价**；**Hn为n日内的最高价**。
 其次，计算K值与D值：
 当日K值=2/3×前一日K值+1/3×当日RSV
 当日D值=2/3×前一日D值+1/3×当日K值
 若无前一日K 值与D值，则可分别用50来代替。
 J值=3*当日K值-2*当日D值

以9日为周期的KD线为例，即未成熟随机值，计算公式为
 9日RSV=（C－L9）÷（H9－L9）×100
 公式中，C为第9日的收盘价；L9为9日内的最低价；H9为9日内的最高价。
 K值=2/3×第8日K值+1/3×第9日RSV
 D值=2/3×第8日D值+1/3×第9日K值
 J值=3*第9日K值-2*第9日D值



```dart
/**
     * kdj 9,3,3
     * N:=9; P1:=3; P2:=3;
     * RSV:=(CLOSE-L(LOW,N))/(H(HIGH,N)-L(LOW,N))*100;
     * K:SMA(RSV,P1,1);
     * D:SMA(K,P2,1);
     * J:3*K-2*D;
     * @param entries 数据集合
     * @param n 指标周期 9
     * @param m 权重 1
     * @param P1 参数值为3
     * @param P2 参数值为3
     * @return 
     */
    public static List[] getKDJ(List<CandleEntry> entries, int n, int P1, int P2, int m) {
        List<Entry> kValue = new ArrayList();
        List<Entry> dValue = new ArrayList();
        List<Entry> jValue = new ArrayList();

        List<Entry> maxs = getPeriodHighest(entries, n);
        List<Entry> mins = getPeriodLowest(entries, n);
        //确保和 传入的list size一致，
        int size = entries.size() - maxs.size();
        for (int i = 0; i < size; i++) {
            maxs.add(0, new Entry());
            mins.add(0, new Entry());
        }
        float rsv = 0;
        float lastK = 50;
        float lastD = 50;

        for (int i = n - 1; i < entries.size(); i++) {
            float x = entries.get(i).getX();
            if (i >= maxs.size())
                break;
            if (i >= mins.size())
                break;
            float div = maxs.get(i).getY() - mins.get(i).getY();
            if (div == 0) {
                //使用上一次的
            } else {
                rsv = ((entries.get(i).getClose() - mins.get(i).getY())
                        / (div)) * 100;
            }

            float k = countSMA(rsv, P1, m, lastK);
            float d = countSMA(k, P2, m, lastD);
            float j = 3 * k - 2 * d;
            lastK = k;
            lastD = d;
            kValue.add(new Entry(x, k));
            dValue.add(new Entry(x, d));
            jValue.add(new Entry(x, j));
        }

        return new List[]{kValue, dValue, jValue};
    }

    /**
     * SMA(C,N,M) = (M*C+(N-M)*Y')/N
     * C=今天收盘价－昨天收盘价    N＝就是周期比如 6或者12或者24， M＝权重，一般取1
     *
     * @param c   今天收盘价－昨天收盘价
     * @param n   周期
     * @param m   1
     * @param sma 上一个周期的sma
     * @return
     */
    private static float countSMA(float c, float n, float m, float sma) {
        return (m * c + (n - m) * sma) / n;
    }

   /**
     * n周期内最低值集合
     * @param entries
     * @param n
     * @return
     */
    private static List<Entry> getPeriodLowest(List<CandleEntry> entries, int n) {
        List<Entry> result = new ArrayList<>();
        float minValue = 0;
        for (int i = n - 1; i < entries.size(); i++) {
            float x = entries.get(i).getX();
            for (int j = i - n + 1; j <= i; j++) {
                if (j == i - n + 1) {
                    minValue = entries.get(j).getLow();
                } else {
                    minValue = Math.min(minValue, entries.get(j).getLow());
                }
            }
            result.add(new Entry(x, minValue));
        }
        return result;
    }

    /**
     *  N周期内最高值集合
     * @param entries
     * @param n
     * @return
     */
    private static List<Entry> getPeriodHighest(List<CandleEntry> entries, int n) {
        List<Entry> result = new ArrayList<>();
        float maxValue = entries.get(0).getHigh();
        for (int i = n - 1; i < entries.size(); i++) {
            float x = entries.get(i).getX();
            for (int j = i - n + 1; j <= i; j++) {
                if (j == i - n + 1) {
                    maxValue = entries.get(j).getHigh();
                } else {
                    maxValue = Math.max(maxValue, entries.get(j).getHigh());
                }
            }
            result.add(new Entry(x, maxValue));
        }
        return result;
    }
```

### 六、RSI

相对强弱指数RSI是根据一定时期内上涨点数和涨跌点数之和的比率制作出的一种技术曲线。能够反映出市场在一定时期内的景气程度。

##### 计算方式

RSI:= SMA(MAX(Close-LastClose,0),N,1)/SMA(ABS(Close-LastClose),N,1)*100
 （看下面的算法似乎很简单，但是个人觉得这个指标计算反而是比较难缠的一个。。。）



```java
   /**
     * RSI（n)
     * RSI(N):= SMA(MAX(Close-LastClose,0),N,1)/SMA(ABS(Close-LastClose),N,1)*100
     *
     * @param entries
     * @param n
     * @param m 加权 1
     * @return
     */
    public static List<Entry> getRSI(List<CandleEntry> entries, int n, int m) {
        List<Entry> result = new ArrayList();
        float preIn = 0;
        float preAll = 0;
        for (int i = 1; i < entries.size(); i++) {
            float diff = entries.get(i).getClose() - entries.get(i - 1).getClose();
            preIn = countSMA(Math.max(diff, 0), n, m, preIn);
            preAll = countSMA(Math.abs(diff), n, m, preAll);
            if (i >= n) {
                float x = entries.get(i).getX();
                result.add(new Entry(x, preIn / preAll * 100));
            }
        }
        return result;
    }
```

