#!/bin/sh
 
/usr/bin/mongo --version > /data/www/backup/mongo_version.txt
DUMP=/usr/bin/mongodump    #mongodump path
OUT_DIR=/data/www/backup/temp   #temp backup file
TAR_DIR=/data/www/backup/final    #backup file
 
DATE=`date +%Y_%m_%d_%H_%M_%S`
TAR_BAK="mongodb_backup_$DATE.tar.gz"
 
DB_HOST=localhost:27017
DB_USER=
DB_PASS=
DAYS=15
 
cd $OUT_DIR
rm -rf $OUT_DIR/*
mkdir -p $OUT_DIR/$DATE
 
 
echo "backup start" >> /data/www/backup/bp.log
# $DUMP -h $DB_HOST --authenticationDatabase "admin" -o $OUT_DIR/$DATE
$DUMP -h $DB_HOST -o $OUT_DIR/$DATE
echo "ing...." >> /data/www/backup/bp.log
tar -zcvf $TAR_DIR/$TAR_BAK $OUT_DIR/$DATE
find $TAR_DIR/ -mtime +$DAYS -delete
echo "backup end" >> /data/www/backup/bp.log
 
cd /data/www/backup
cmd=`date +%Y-%m-%d:%H:%M:%S`
cmd01=${cmd}
echo "$cmd01" >> /data/www/backup/bp.log
 
exit
 
# 挂载docker-compose:/data/www/backup: /data/www/backup
# sudo docker exec -it 0be535e8f2ba /bin/bash
# crontab -e
# 1 */4 * * * /data/www/backup/monbp.sh >> /data/www/backup/bp.log 2>&1