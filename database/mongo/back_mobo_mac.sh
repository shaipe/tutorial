#!/bin/bash
 
/usr/local/bin/mongo --version > mongo_version.txt
DUMP=/usr/local/bin/mongodump    #mongodump命令路径，非常重要，不然/bin/bash找不到路径
OUT_DIR=/data/backup/temp   #临时备份目录
TAR_DIR=/data/backup/final    #备份存放路径
TAR_BAK="mongodb_backup_$DATE.tar.gz"    #最终保存的数据库备份文件
 
DATE=`date +%Y_%m_%d`   #获取当前系统时间
DB_HOST=localhost:27017
DB_USER=    #数据库账号
DB_PASS=    #数据库密码
DAYS=15    #DAYS=15代表删除15天前的备份，即只保留近15天的备份
 
# 进入临时目录删空文件夹内容，根据当前时间重建文件
cd $OUT_DIR
rm -rf $OUT_DIR/*
mkdir -p $OUT_DIR/$DATE
 
# 备份全部数据库，并压缩
echo "backup start"
$DUMP -h $DB_HOST --authenticationDatabase "admin" -o $OUT_DIR/$DATE
$DUMP -h $DB_HOST --authenticationDatabase "admin" -o $TAR_DIR/$DATE
 
# 压缩为.tar.gz格式
tar -zcvf $TAR_DIR/$TAR_BAK $OUT_DIR/$DATE
# 解压格式
# tar -xzvf $TAR_DIR/$TAR_BAK $OUT_DIR/$DATE
 
#删除DAYS天前的备份文件
find $TAR_DIR/ -mtime +$DAYS -delete
 
echo "backup end"
 
cd /data/backup
cmd=`date +%Y-%m-%d:%H:%M:%S`
cmd01=${cmd}
echo "$cmd01" >> time.txt
exit