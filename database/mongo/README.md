# MongoDb

## MongoDb For Docker

    需要先安装好Docker

### MongoDb安装

```
docker pull mongo  // 安装mongo的官方镜像

docker images       // 查看是否已经安装好mongo

```

### 运行 mongo

```
docker start 容器id
```

### 创建 mongo 容器

```bash
# mac
docker run -d \
-p 27017:27017 \
-v ~/docker/mongo/conf:/data/conf \
-v ~/docker/mongo/data:/data/db \
--name cocker-mongo \
mongo:latest

# centos
docker run -d \
--name mongo-27017 \
--restart always \
-p 27017:27017 \
-v /srv/mongo/conf:/data/conf \
-v /srv/mongo/data:/data/db \
mongo --auth

```

```bash

# 进入mongo容器
docker exec -it 容器Id bash

# 执行添加用户命令
db.createUser({ 
    user: 'mongo-admin', 
    pwd: 'mongo.admin', 
    roles: [ { role: "userAdminAnyDatabase", db: "admin" } ] });


# 不带授权的方式
docker run -d -p 27017:27017 -v mongo_configdb:/data/configdb -v mongo_db:/data/db --name mongo docker.io/mongo
# 需要授权的方式
docker run --name <YOUR-NAME> -p 27017:27017 -v /data/db:/data/db -d mongo:3.4 --auth


```

### RUN参数说明

参数及值|说明
--|--
—name mynane |指定库的名字，如果不指定会使用一串随机字符串。
-p 27017:27017 | 官方的镜像已经暴露了 ``27017`` 端口，我们将它映射到主机的端口上。如果你不使用默认端口，将 : 前面的数字改成自定义端口。
-v /data/db:/data/db | 冒号前面的是主机上的文件路径，将它挂载到库中的文件夹下，实际对文件的读写就会在主机文件上操作。
-d | 在后台运行。
mongo:3.4 |指定镜像版本，默认是 latest 。建议总是自己指定版本。
—auth | 以 auth 模式运行 mongo。


### 新建管理员

现在我们需要进入 mongo shell 操作：

```bash
docker exec -it <YOUR-NAME> mongo admin
> db.createUser({ user: '<USER>', pwd: '<PASSWORD>', roles: [ { role: 'userAdminAnyDatabase', db: 'admin' } ]});
Successfully added user: {
    "user" : "<USER>",
    "roles" : [
        {
            "role" : "userAdminAnyDatabase",
            "db" : "admin"
        }
    ]
}
```
以后想以管理员身份登入 mongo shell 就可以运行：

```bash
docker exec -it <YOUR-NAME> mongo -u <USER> -p <PASSWORD> --authenticationDatabase admin
```
### MongoDb 常用命令

```shell
> mongo # 进入mongo的命令行
> show dbs  #显示数据库列表 
> show collections  #显示当前数据库中的集合（类似关系数据库中的表）
> show users  #显示用户
> use <db name>  #切换当前数据库，如果数据库不存在则创建数据库。 
> db.help()  #显示数据库操作命令，里面有很多的命令 
> db.foo.help()  #显示集合操作命令，同样有很多的命令，foo指的是当前数据库下，一个叫foo的集合，并非真正意义上的命令 
> db.foo.find()  #对于当前数据库中的foo集合进行数据查找（由于没有条件，会列出所有数据） 
> db.foo.find( { a : 1 } )  #对于当前数据库中的foo集合进行查找，条件是数据中有一个属性叫a，且a的值为1
> db.dropDatabase()  #删除当前使用数据库
> db.cloneDatabase("127.0.0.1")   #将指定机器上的数据库的数据克隆到当前数据库
> db.copyDatabase("mydb", "temp", "127.0.0.1")  #将本机的mydb的数据复制到temp数据库中
> db.repairDatabase()  #修复当前数据库
> db.getName()  # 查看当前使用的数据库，也可以直接用db
> db.stats()  # 显示当前db状态
> db.version()  # 当前db版本
> db.getMongo()   # 查看当前db的链接机器地址
> db.serverStatus()  # 查看数据库服务器的状态

```

### 创建数据库

```js
use testdb

db.createUser(  
    {  
        "user":"root",  
        "pwd":"123456",  
        "roles":[{"role" : "readWrite", "db":"testdb"}]  
    }  
) 

db.auth(  
    {  
        "user":"root",  
        "pwd":"123456"  
    }  
) 

```

### Mongo 批量插入
update() 方法用于更新已存在的文档，语法如下：
```js
db.collection.update(
   <query>,
   <update>,
   {
     upsert: <boolean>,
     multi: <boolean>,
     writeConcern: <document>
   }
)
```
  
参数说明：

- query: update的查询条件，类似sql update查询内where后面的。

- update: update的对象和一些更新的操作符，也可以理解为sql update查询内set后面的。

- upsert: 可选，这个参数的意思是，如果不存在update的记录，是否插入objNew, true为插入，默认是false，不插入。

- multi: 可选，mongodb默认是false，只更新找到的第一条记录，如果这个参数为true，则更新所有按条件查出来的多条记录。

- writeConcern: 可选，抛出异常的级别。

```js
// 向member表中批量插入一个is_admin的字段,并给定默认值为0
db.member.update(
    {"is_admin" : {$exists : false}},
    {"$set" : {"is_admin" : 0}},
    false,
    true
)
```

### Mongo 查询


左边是mongodb查询语句，右边是sql语句。对照着用，挺方便。
Mongo语句| Sql语句
-|-|-|
db.users.find() |select * from users
db.users.find({"age" : 27}) |select * from users where age = 27
db.users.find({"username" : "joe", "age" : 27}) |select * from users where "username" = "joe" and age = 27
db.users.find({}, {"username" : 1, "email" : 1}) |select username, email from users
db.users.find({}, {"username" : 1, "_id" : 0}) // no case  // 即时加上了列筛选，_id也会返回；必须显式的阻止_id返回db.users.find({"age" : {"$gte" : 18, "$lte" : 30}}) |select * from users where age >=18 and age <= 30 // $lt(<) $lte(<=) $gt(>) $gte(>=)
db.users.find({"username" : {"$ne" : "joe"}}) |select * from users where username <> "joe"
db.users.find({"ticket_no" : {"$in" : [725, 542, 390]}}) |select * from users where ticket_no in (725, 542, 390)
db.users.find({"ticket_no" : {"$nin" : [725, 542, 390]}}) |select * from users where ticket_no not in (725, 542, 390)
db.users.find({"$or" : [{"ticket_no" : 725}, {"winner" : true}]}) |select * form users where ticket_no = 725 or winner = true
db.users.find({"id_num" : {"$mod" : [5, 1]}}) |select * from users where (id_num mod 5) = 1
db.users.find({"$not": {"age" : 27}}) |select * from users where not (age = 27)
db.users.find({"username" : {"$in" : [null], "$exists" : true}}) |select * from users where username is null 

```js
// 如果直接通过find({"username" : null})进行查询，那么连带"没有username"的纪录一并筛选出来
db.users.find({"name" : /joey?/i}) 
// 正则查询，value是符合PCRE的表达式
db.food.find({fruit : {$all : ["apple", "banana"]}}) 
// 对数组的查询, 字段fruit中，既包含"apple",又包含"banana"的纪录
db.food.find({"fruit.2"js : "peach"}) 
// 对数组的查询, 字段fruit中，第3个(从0开始)元素是peach的纪录
db.food.find({"fruit" : {"$size" : 3}}) 
// 对数组的查询, 查询数组元素个数是3的记录，$size前面无法和其他的操作符复合使用
db.users.findOne(criteria, {"comments" : {"$slice" : 10}}) 
// 对数组的查询，只返回数组comments中的前十条，还可以{"$slice" : -10}， {"$slice" : [23, 10]}; 分别返回最后10条，和中间10条
db.people.find({"name.first" : "Joe", "name.last" : "Schmoe"})  
// 嵌套查询db.blog.find({"comments" : {"$elemMatch" : {"author" : "joe", "score" : {"$gte" : 5}}}}) 
// 嵌套查询，仅当嵌套的元素是数组时使用,
db.foo.find({"$where" : "this.x + this.y == 10"}) 
// 复杂的查询，$where当然是非常方便的，但效率低下。对于复杂查询，考虑的顺序应当是 正则 -> MapReduce -> $where
db.foo.find({"$where" : "function() { return this.x + this.y == 10; }"}) 
// $where可以支持javascript函数作为查询条件
db.foo.find().sort({"x" : 1}).limit(1).skip(10); 
// 返回第(10, 11]条，按"x"进行排序; 三个limit的顺序是任意的，应该尽量避免skip中使用large-number
```

#### 1) . 大于，小于，大于或等于，小于或等于

- $gt:大于
- $lt:小于
- $gte:大于或等于
- $lte:小于或等于

##### 例子：
```js
db.collection.find({ "field" : { $gt: value } } ); // greater than : field > value
db.collection.find({ "field" : { $lt: value } } ); // less than : field < value
db.collection.find({ "field" : { $gte: value } } ); // greater than or equal to : field >= value
db.collection.find({ "field" : { $lte: value } } ); // less than or equal to : field <= value
如查询j大于3,小于4:

db.things.find({j : {$lt: 3}});
db.things.find({j : {$gte: 4}});
也可以合并在一条语句内:

db.collection.find({ "field" : { $gt: value1, $lt: value2 } } ); // value1 < field < value
```
#### 2) 不等于 $ne

例子：
```js
db.things.find( { x : { $ne : 3 } } );
```

#### 3) in 和 not in ($in $nin)

语法：
```js
db.collection.find( { "field" : { $in : array } } );
```
例子：
```js
db.things.find({j:{$in: [2,4,6]}});
db.things.find({j:{$nin: [2,4,6]}});
```

#### 4) 取模运算$mod

如下面的运算：
```js
db.things.find( "this.a % 10 == 1")
```
可用$mod代替：
```js
db.things.find( { a : { $mod : [ 10 , 1 ] } } )
```

#### 5)  $all

$all和$in类似，但是他需要匹配条件内所有的值：

如有一个对象：
```js
{ a: [ 1, 2, 3 ] }
// 下面这个条件是可以匹配的：

db.things.find( { a: { $all: [ 2, 3 ] } } );
// 但是下面这个条件就不行了：

db.things.find( { a: { $all: [ 2, 3, 4 ] } } );
```
#### 6)  $size

$size是匹配数组内的元素数量的，如有一个对象：{a:["foo"]}，他只有一个元素：

下面的语句就可以匹配：
```js
db.things.find( { a : { $size: 1 } } );
```
官网上说不能用来匹配一个范围内的元素，如果想找$size<5之类的，他们建议创建一个字段来保存元素的数量。

You cannot use $size to find a range of sizes (for example: arrays with more than 1 element). If you need to query for a range, create an extra size field that you increment when you add elements.

#### 7）$exists

$exists用来判断一个元素是否存在：

如：
```js
db.things.find( { a : { $exists : true } } ); // 如果存在元素a,就返回
db.things.find( { a : { $exists : false } } ); // 如果不存在元素a，就返回
```
#### 8)  $type

$type 基于 bson type来匹配一个元素的类型，像是按照类型ID来匹配，不过我没找到bson类型和id对照表。
```js
db.things.find( { a : { $type : 2 } } ); // matches if a is a string
db.things.find( { a : { $type : 16 } } ); // matches if a is an int
```

#### 9）正则表达式

mongo支持正则表达式，如：
```js
db.customers.find( { name : /acme.*corp/i } ); // 后面的i的意思是区分大小写
```
#### 10)  查询数据内的值

下面的查询是查询colors内red的记录，如果colors元素是一个数据,数据库将遍历这个数组的元素来查询。
```js
db.things.find( { colors : "red" } );
```

#### 11) $elemMatch

如果对象有一个元素是数组，那么$elemMatch可以匹配内数组内的元素：
```js
> t.find( { x : {
     $elemMatch : { a : 1, b : { $gt : 1 } } } } ) 
{ "_id" : ObjectId("4b5783300334000000000aa9"), 
"x" : [ 
    { "a" : 1, "b" : 3 }, 7, { "b" : 99 }, { "a" : 11 } 
    ]
}
$elemMatch : { a : 1, b : { $gt : 1 } } 所有的条件都要匹配上才行。
//注意，上面的语句和下面是不一样的。

> t.find( { "x.a" : 1, "x.b" : { $gt : 1 } } )
// $elemMatch是匹配{ "a" : 1, "b" : 3 }，而后面一句是匹配{ "b" : 99 }, { "a" : 11 } 
```

#### 12)  查询嵌入对象的值

db.postings.find( { "author.name" : "joe" } );
注意用法是author.name，用一个点就行了。更详细的可以看这个链接： dot notation

举个例子：
```js
> db.blog.save({ title : "My First Post", author: {name : "Jane", id : 1}})
// 如果我们要查询 authors name 是Jane的, 我们可以这样：

> db.blog.findOne({"author.name" : "Jane"})
// 如果不用点，那就需要用下面这句才能匹配：

db.blog.findOne({"author" : {"name" : "Jane", "id" : 1}})
下面这句：

db.blog.findOne({"author" : {"name" : "Jane"}})
// 是不能匹配的，因为mongodb对于子对象，他是精确匹配。
```

#### 13) 元操作符 $not 取反

如：
```js
db.customers.find( { name : { $not : /acme.*corp/i } } );db.things.find( { a : { $not : { $mod : [ 10 , 1 ] } } } ); mongodb还有很多函数可以用，如排序，统计等，请参考原文。
```
mongodb目前没有或(or)操作符，只能用变通的办法代替，可以参考下面的链接：

http://www.mongodb.org/display/DOCS/OR+operations+in+query+expressions


## MongoDB数据备份

在Mongodb中我们使用mongodump命令来备份MongoDB数据。该命令可以导出所有数据到指定目录中。

mongodump命令可以通过参数指定导出的数据量级转存的服务器。

语法
mongodump命令脚本语法如下：

>mongodump -h dbhost -d dbname -o dbdirectory
-h：
MongDB所在服务器地址，例如：127.0.0.1，当然也可以指定端口号：127.0.0.1:27017

-d：
需要备份的数据库实例，例如：test

-o：
备份的数据存放位置，例如：c:\data\dump，当然该目录需要提前建立，在备份完成后，系统自动在dump目录下建立一个test目录，这个目录里面存放该数据库实例的备份数据。

-u 用户名
-p 数据库授权密码

```

mongodump -h 127.0.1 -u root -p password -d dbname -o outdir

```

## MongoDB数据恢复

mongodb使用 mongorestore 命令来恢复备份的数据。

语法
mongorestore命令脚本语法如下：

>mongorestore -h <hostname><:port> -d dbname <path>
--host <:port>, -h <:port>：
MongoDB所在服务器地址，默认为： localhost:27017

--db , -d ：
需要恢复的数据库实例，例如：test，当然这个名称也可以和备份时候的不一样，比如test2

--drop：
恢复的时候，先删除当前数据，然后恢复备份的数据。就是说，恢复后，备份后添加修改的数据都会被删除，慎用哦！

<path>：
mongorestore 最后的一个参数，设置备份数据所在位置，例如：c:\data\dump\test。

你不能同时指定 <path> 和 --dir 选项，--dir也可以设置备份目录。

--dir：
指定备份的目录

你不能同时指定 <path> 和 --dir 选项。

--username, -u 用户名
--password, -p 密码
