# postgres 数据库学习笔记

### upsert

```sql
create table test(id int primary key, info text, crt_time timestamp, hits int DEFAULT 0);


insert into test values (1,'test',now(),0) on conflict (id) do update set   
info=excluded.info,crt_time=excluded.crt_time,hits=test.hits+1;  
I
```

### 一、查询Postgres数据库中的所有表信息（表名、备注）
```sql
select relname as tabname,cast(obj_description(relfilenode,‘pg_class’) as varchar) as comment from pg_class c
where relkind = ‘r’ and relname not like ‘pg_%’ and relname not like ‘sql_%’ order by relname
```
`注意：`过滤掉分表：加条件 and relchecks=0 即可

### c二、查询Postgres数据库中的表字段名、类型、注释、注释是否为空
```sql
SELECT col_description(a.attrelid,a.attnum) as comment,format_type(a.atttypid,a.atttypmod) as type,a.attname as name, a.attnotnull as notnull FROM pg_class as c,pg_attribute as a where c.relname = ‘表名’ and a.attrelid = c.oid and a.attnum>0
```
`注意`：如果直接复制spl去运行的话，如果不能运行，请把引号改英文的。



