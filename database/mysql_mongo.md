# MySQL 与 MongoDB语法表

## 术语和概念

### 结构对比

表结构|MySQL|MongoDB
--|--|--
数据库|database|database
表|table|collection
行|row	|document 或 BSON document
列|column|field
索引|index|index
连接|table连接|document嵌套和连接
主键|primary key	|primary key
主键说明|指定单独一列或者列组合作为primary key	|在MongoDB中primary key会自动设置到——id field中
分组|aggregation（例如 group by）	|aggregation pipeline
约束|无|主键约束

### 数据类型对比

数据类型对比|MongoDB|MySQL
--|--|--
整形|NumberInt("3"),NumberLong("3")	|TINYINT, SMALLINT, MEDIUMINT, INT, BIGINT
浮点|默认使用64位浮点型数值	|FLOAT, DOUBLE, DECIMAL
字符|utf8 字符串|VARCHAR, CHAR
日期/时间|new Date(), 自新纪元依赖经过的毫秒数，不存储时区|DATE, DATETIME, TIMESTAMP
NULL|null|不支持（null与null不相等）
布尔类型|true/false|不支持
正则表达式|支持 { "x" : /foobar/i }|不支持
数组|支持 { "x" : ["a", "b", "c"]}|不支持
二进制数据|支持 GridFS|BLOB, TEXT
代码片段|{ "x" : function() { /... / } }|不支持

### Shell终端对比

对比项|MongoDB|MySQL
--|--|--
启动|mongo|mysql -u root -p
查看库|show dbs|show databases
使用库|use test|use test
查看表|show collections|show tables

### 函数对比

函数对比|MongoDB|MySQL
--|--|--
COUNT|db.foo.count()|SELECT COUNT(id) FROM foo;
DISTINCT|db.runCommand({ "distinct": "people", "key": "age" })	|SELECT DISTINCT(age) FROM people;
MIN	|db.sales.aggregate( [ { $group: { _id: {}, minQuantity: { $min: "$quantity" } } } ]); 结果： { "_id" : { }, "minQuantity" : 1 }|	SELECT MIN(quantity) FROM sales;
MAX	|db.sales.aggregate( [ { $group: { _id: {}, maxQuantity: { $max: "$quantity" } } } ]);|	SELECT MAX(quantity) FROM sales;
AVG	|db.sales.aggregate( [ { $group: { _id: {}, avgQuantity: { $avg: "$quantity" } } } ]);|	SELECT AVG(quantity) FROM sales;
SUM	|db.sales.aggregate( [ { $group: { _id: {}, totalPrice: { $sum: "$price" } } } ]);|	SELECT SUM(price) FROM sales;

### 查询对比

查询对比|MongoDB|	MySQL
--|--|--
检索单列|	db.users.find({ "age" : 27 })|	SELECT * FROM users WHERE age = 27;
检索多列|db.users.find({ "age" : 27, "username" : "joe" })|SELECT * FROM users WHERE age = 27 and username = 'joe';
指定需要返回的键|db.users.find({}, { "username" : 1, "email" : 1 })	|SELECT username, email FROM users;
范围检索	|db.users.find({"age" : { "$gte" : 18, "$lte" : 30 }}) $lt, $lte, $gt, $gte 分别对应 <, <=, >, >=	|SELECT * FROM users WHERE age >= 18 AND age <=30;
不匹配检索|	db.users.find({ "username" : { "$ne" : "joe" } })	|SELECT * FROM users WHERE username <> 'joe';
IN 操作符	|db.raffle.find({ "ticket_no" : { "$in" : [725, 542, 390] } }) $in非常灵活，可以指定不同类型 的条件和值。 例如在逐步将用户的ID号迁移成用户名的过程中， 查询时需要同时匹配ID和用户名|	SELECT ticket_no FROM raffles WHERE ticket_no IN (725, 542, 390);
NOT IN 操作符	|db.raffle.find({ "ticket_no" : { "$nin" : [725, 542, 390] } })	|SELECT * FROM raffles WHERE ticket_no not in (725, 542, 390);
OR 操作符	|db.raffle.find({ "$or" : [{ "ticket_no" : 725 }, { "winner" : true }] })|	SELECT * FROM raffles WHERE ticket_no = 725 OR winner = 'true';
空值检查	|db.c.find({"y" : null}) null不仅会匹配某个键的值为null的文档 ，而且还会匹配不包含这个键的文档。 所以，这种匹配还会返回缺少这个键的所有文档。 如果 仅想要匹配键值为null的文档， 既要检查改建的值是否为null, 还要通过 $exists 条件 判定键值已经存在 db.c.find({ "z" : { "$in" : [null], "$exists" : true }})	|SELECT * FROM cs WHERE z is null;
多列排序	|db.c.find().sort({ username : 1, age: -1 })	|SELECT * FROM cs ORDER BY username ASC, age DESC;
AND操作符	|db.users.find({ "$and" : [{ "x" : { "$lt" : 1 }, { "x" : 4 } }] }) 由于查询优化器不会对 $and进行优化， 所以可以改写成下面的 db.users.find({ "x" : { "$lt" : 1, "$in" : [4] } })	|SELECT * FROM users WHERE x > 1 AND x IN (4);
NOT 操作符	|db.users.find({ "id_num" : { "$not" : { "$mod" : [5,1] } } })|	SELECT * FROM users WHERE id_num NOT IN (5,1);
LIKE 操作符(正则匹配)	|db.blogs.find( { "title" : /post?/i } ) MongoDB 使用Perl兼容的正则表达式(PCRE) 库来匹配正则表达式， 任何PCRE支持表达式的正则表达式语法都能被MongoDB接受	|SELECT * FROM blogs WHERE title LIKE "post%";


## 语法关系
假设存在名为user的collection，它的文档包含如下属性
```js
{
  _id:ObjectId("509a8fb2f3f4948bd2f983a0"),
  user_id:"abc123",
  age:55,
  status:'A'
}
```
### 新建表/集合

**SQL**

```sql
CREATE TABLE users (
    id MEDIUMINT NOT NULL
        AUTO_INCREMENT,
    user_id Varchar(30),
    age Number,
    status char(1),
    PRIMARY KEY (id)
)
```

**MongoDB**

```js
// 在第一次进行insert()操作的时候会进行隐式创建。
// 如果不指定_id字段的话，_id将会被自动创建
db.users.insert({
    user_id:"abc123",
    age:55,
    status:"A"
})
// 当然也可以单独创建一个collection
db.createCollection("users")
```

### 添加新列/字段
**SQL**

```sql
ALTER TABLE users
ADD join_date DATETIME
```
**MongoDB**

```js
// collection并不会对它的document结构进行约束
// 在collection层面上并不会有结构的变化
// 但是在document层面上，update()操作可以利用$set为现有的document添加field
db.users.update(
{},
{ $set:{ join_date:newDate()}},
{ multi:true}
)
```

### 删除列/字段

**SQL**

```sql
ALTER TABLE users
DROP COLUMN join_date
```
**MongoDB**
```js
// collection并不会对它的document结构进行约束
// 在collection层面上并不会有结构的变化
// 但是在document层面上，update()操作可以利用$unset为现有的document移除field
db.users.update(
{},
{ $unset:{ join_date:""}},
{ multi:true}
)
```
### 创建索引
**SQL**
```sql
CREATE INDEX idx_user_id_asc
ON users(user_id)
```

**MongoDB**

```js
db.users.createIndex({ user_id:1})
```
### 创建联合索引
**SQL**
```sql
CREATE INDEX
       idx_user_id_asc_age_desc
ON users(user_id, age DESC)
```
**MongoDB**
```js
db.users.createIndex({ user_id:1, age:-1})
```
### 删除表/集合
**SQL**
```sql
DROP TABLE users
```
**MongoDB**
```js
db.users.drop()
```
### 插入
**SQL**
```sql
INSERT INTO users(user_id,
                  age,
                  status)
VALUES ("bcd001",
45,
"A")
```
**MongoDB**
```js
db.users.insert(
{ user_id:"bcd001", age:45, status:"A"}
)
```
### 查询所有的数据
**SQL**
```sql
SELECT *
FROM users
```
**MongoDB**
```js
db.users.find()
```
### 查询所有指定字段的数据（查询主键）
**SQL**
```sql
SELECT id,
       user_id,
       status
FROM users
```
**MongoDB**
```js
db.users.find(
{},
{ user_id:1, status:1}
)
```
### 查询所有指定字段的数据（不查询主键）
**SQL**
```sql
SELECT user_id, status
FROM users
```
**MongoDB**
```js
db.users.find(
{},
{ user_id:1, status:1, _id:0}
)
```
### 条件查询（字段为相应的内容）
**SQL**
```sql
SELECT *
FROM users
WHERE status ="A"
```
**MongoDB**
```js
db.users.find(
{ status:"A"}
)
```
### 条件查询指定字段（字段为相应的内容）
**SQL**
```sql
SELECT user_id, status
FROM users
WHERE status ="A"
```
**MongoDB**
```js
db.users.find(
{ status:"A"},
{ user_id:1, status:1, _id:0}
)
```
### 条件查询（字段不含相应的内容）
**SQL**
```sql
SELECT *
FROM users
WHERE status !="A"
```
**MongoDB**
```js
db.users.find(
{ status:{ $ne:"A"}}
)
```
### 条件查询（与查询）

**SQL**
```sql
SELECT *
FROM users
WHERE status ="A"
AND age =50
```
**MongoDB**
```js
    db.users.find(
    { status:"A",
        age:50}
    )
```

### 条件查询（或查询）
**SQL**
```sql
SELECT *
FROM users
WHERE status ="A"
OR age =50
```
**MongoDB**
```js
db.users.find(
{ $or:[{ status:"A"},
{ age:50}]}
)
```

### 条件查询（大于关系）
**SQL**
```sql
SELECT *
FROM users
WHERE age >25
```
**MongoDB**
```js
db.users.find(
{ age:{ $gt:25}}
)
```

### 条件查询（小于关系）
**SQL**
```sql
SELECT *
FROM users
WHERE age <25
```
**MongoDB**
```js
db.users.find(
{ age:{ $lt:25}}
)
```

### 条件查询（区间关系）
**SQL**
```sql
SELECT *
FROM users
WHERE age >25
AND   age <=50
```
**MongoDB**
```js
db.users.find(
{ age:{ $gt:25, $lte:50}}
)
```

### 模糊查询（包含）
**SQL**
```sql
SELECT *
FROM users
WHERE user_id like "%bc%"
```

**MongoDB**
```js
db.users.find({ user_id:/bc/})
```

### 模糊查询（起始）
**SQL**
```sql
SELECT *
FROM users
WHERE user_id like "bc%"
```
**MongoDB**
```js
db.users.find({ user_id:/^bc/})
```

### 升序处理查询结果
**SQL**
```sql
SELECT *
FROM users
WHERE status ="A"
ORDER BY user_id ASC
```
**MongoDB**
```js
db.users.find({ status:"A"}).sort({ user_id:1})
```

### 降序处理查询结果
**SQL**
```sql
SELECT *
FROM users
WHERE status ="A"
ORDER BY user_id DESC
```
**MongoDB**
```js
db.users.find({ status:"A"}).sort({ user_id:-1})
```

### 统计（所有条目）
**SQL**
```sql
SELECT COUNT(*)
FROM users
```
**MongoDB**
```js
db.users.count()
// 或者
db.users.find().count()
```

### 统计（指定字段）
**SQL**
```sql
SELECT COUNT(user_id)
FROM users
```
**MongoDB**
```js
db.users.count({ user_id:{ $exists:true}})
// 或者
db.users.find({ user_id:{ $exists:true}}).count()
```

### 统计符合指定条件的条目
**SQL**
```sql
SELECT COUNT(*)
FROM users
WHERE age >30
```
**MongoDB**
```js
db.users.count({ age:{ $gt:30}})
// 或者
db.users.find({ age:{ $gt:30}}).count()
```

### 去重查询
**SQL**
```sql
SELECT DISTINCT(status)
FROM users
```
**MongoDB**
```js
db.users.distinct("status")
```
### 查询单条数据
**SQL**
```sql
SELECT *
FROM users
LIMIT 1
```
**MongoDB**
```js
db.users.findOne()
// 或者
db.users.find().limit(1)
```
### 分页并跳过指定页数
**SQL**
```sql
SELECT *
FROM users
LIMIT 5
SKIP 10
```
**MongoDB**
```js
db.users.find().limit(5).skip(10)
```
### 语法性能测试
**SQL**
```sql
EXPLAIN SELECT *
FROM users
WHERE status ="A"
```
**MongoDB**
```js
db.users.find({ status:"A"}).explain()
```

### 更新（静态值）

**SQL**
```sql
UPDATE users
SET status ="C"
WHERE age >25
```
**MongoDB**
```js
db.users.update(
{ age:{ $gt:25}},
{ $set:{ status:"C"}},
{ multi:true}
)
```

### 更新（动态值）

**SQL**
```sql
UPDATE users 
SET age = age +3
WHERE status ="A"
```
**MongoDB**
```js
db.users.update(
{ status:"A"},
{ $inc:{ age:3}},
{ multi:true}
)
```

### 条件删除

**SQL**
```sql
DELETE FROM users
WHERE status ="D"
```
**MongoDB**
```js
db.users.remove({ status:"D"})
```
### 删除（所有记录）

**SQL**
```sql
DELETE FROM users
```
**MongoDB**
```
db.users.remove({})
```