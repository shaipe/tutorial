

docker run -d \
--name es \
-p 9200:9200 \
-p 9300:9300 \
-e discovery.type="single-node" \
-e ES_JAVA_OPTS="-Xms256m -Xmx256m" \
elasticsearch:7.9.3



148.70.29.135



| 名称    | 适合类型                | 文件分布                    | 复杂程度 | 备份机制                | 通讯协议 | 社区活跃度 | 开发语言 |
| ------- | ----------------------- | --------------------------- | -------- | ----------------------- | -------- | ---------- | -------- |
| FastDFS | 4KB-500MB跨集群的小文件 | 小文件合并存储不分片处理    | 简单     | 组内冗余备份            | api http | 国内用户多 | C语言    |
| TFS     | 单集群的中小文件        | 小文件合并，以Block组织分片 | 复杂     | Block存储多份，主辅灾备 | api http | 少         | C++      |
| HDFS    | 大文件                  | 大文件分片分块存储          | 简单     | 多副本                  | 原生api  | 较多       | java     |
| Ceph    | 单集群的大中小文件      | OSD一主多从                 | 复杂     | 多副本                  | 原生api  | 较多       | C++      |

## ES操作

### 获取所有的索引

```bash
curl -XGET 'localhost:9200/_cat/indices?v&pretty'
```

### Create an Index（创建索引）

curl -XPUT 'localhost:9200/customer?pretty&pretty'

Index and Query a Document（索引和查询文档）

curl -XPUT 'localhost:9200/customer/external/1?pretty&pretty' -d'
{
  "name": "John Doe"
}'