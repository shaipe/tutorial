# Ubuntu下安装ES


##### 1.1.下载安装包（版本7.12.0）解压

```sh
# 下载安装包
wget https://artifacts.elastic.co/downloads/elasticsearch/elasticsearch-7.12.0-amd64.deb
# 验证下载文件的有效性判断文件
wget https://artifacts.elastic.co/downloads/elasticsearch/elasticsearch-7.12.0-amd64.deb.sha512

# tar -zxvf elasticsearch-7.12.0-linux-x86_64.tar.gz 是这对压缩文件的处理方式

# 安装es
sudo dpkg -i elasticsearch-7.12.0-amd64.deb
# 安装完成后的程序目录为：/usr/share/elasticsearch/
# 安装完成后的配置文件目录为： /etc/elasticsearch/
```

##### 1.2.修改配置文件，elasticsearch.yml

```yaml
#集群名字 同一个集群名字必须一致
cluster.name: es-cluster 
#当前节点的名字
node.name: node-1 
#对所有IP开放
# network.host: 0.0.0.0 #配置此地址则需要注意1.4步骤
network.host: 192.168.17.191 #本机地址（配置本地地址则不需要注意1.4步骤）
#HTTP端口号
http.port: 9200
#elasticsearch数据文件存放目录
path.data: /usr/share/elasticsearch/data
#elasticsearch日志文件存放目录
path.logs: /usr/share/elasticsearch/logs
#服务发现（参数为ip or 域名）
discovery.seed_hosts: ["192.168.17.191"] 
#确定哪些节点参与第一次的主节点选举（填写内容为node.name参数）
cluster.initial_master_nodes: ["node-1"]
```

##### 1.3.配置完之后，因为ElasticSearch使用非root用户启动，所以创建一个用户。

```yaml
# 创建用户
useradd esuser
# 设置密码
passwd esuser
chinasie
# 赋予用户权限
chown -R esuser:esuser /usr/share/elasticsearch
# 给配置目录权限
chown -R esuser:esuser /etc/elasticsearch
# 给环境变量文件权限 
chown -R esuser:esuser /etc/default/elasticsearch

chown -R esuser:esuser /var/log/elasticsearch
```

##### 1.4 启动问题处理

```
问题1: 

max file descriptors [4096] for elasticsearch process is too low, increase to at least [65536]

每个进程最大同时打开文件数太小，可通过下面2个命令查看当前数量

ulimit -Hn
ulimit -Sn

解决方式：
修改/etc/security/limits.conf文件，增加配置，用户退出后重新登录生效

*               soft    nofile          65536
*               hard    nofile          65536

问题2:
max number of threads [3818] for user [es] is too low, increase to at least [4096]

最大线程个数太低。修改配置文件/etc/security/limits.conf（和问题1是一个文件），增加配置

*               soft    nproc           4096
*               hard    nproc           4096

通过命令查看
ulimit -Hu
ulimit -Su

问题3:

max virtual memory areas vm.max_map_count [65530] is too low, increase to at least [262144]
 
修改/etc/sysctl.conf文件，增加配置vm.max_map_count=262144

执行命令sysctl -p生效

问题4:

the default discovery settings are unsuitable for production use; at least one of [discovery.seed_hosts, discovery.seed_providers, cluster.initial_master_nodes] must be configured

配置文件 elasticsearch.yml 文件，添加一下行即可解决

cluster.initial_master_nodes: ["{node.name}"] #node-name值
```

##### 1.5.启动

```shell
# 切换用户
su esuser
# 启动 -d表示后台启动
./bin/elasticsearch -d
```