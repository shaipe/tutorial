# course
平常学习中收集的教程整理

[MarkDown语法学习](https://github.com/shaipe/course/blob/master/markdown.md)
[Nginx使用和配置](nginx.md)
[正则表达式教程](regx.md)

## 数据库

[Mac系统中Mariadb安装使用](https://github.com/shaipe/course/blob/master/dbase/mac-mariadb.md)

[Mac系统中Postgresql安装使用](https://github.com/shaipe/course/blob/master/dbase/mac-postgresql.md)

[Mac系统中Mongodb安装使用](https://github.com/shaipe/course/blob/master/dbase/mac-mongodb.md)

## Docker
[Dockerfile语法学习](https://github.com/shaipe/course/blob/master/docker/dockerfile.md)

[Docker常用命令](https://github.com/shaipe/course/blob/master/docker/shell.md)

[Docker安装和使用Gitlab](https://github.com/shaipe/course/blob/master/docker/gitlab.md)

[Docker安装和使用Mariadb](https://github.com/shaipe/course/blob/master/docker/mariadb.md)

[Docker中安装和使用Redis](https://github.com/shaipe/course/blob/master/docker/shell.md)

## Git
[Git常用命令](https://github.com/shaipe/course/blob/master/git/git-command.md)

[Gitlab服务管理学习](https://github.com/shaipe/course/blob/master/git/gitlab.md)

## Java

[Centos下Java环境搭建](https://github.com/shaipe/course/blob/master/java/centos-java.md)

[Java项目发布](https://github.com/shaipe/course/blob/master/java/java-publish.md)

[Linux下Tomcat和Java环境搭建](https://github.com/shaipe/course/blob/master/java/linux-java-tomcat.md)

[Java命名规范](https://github.com/shaipe/course/blob/master/java/命名规范.md)

## Mac
[Mac系统各种清理](https://github.com/shaipe/course/blob/master/mac/mac-clear.md)

[Mac系统工具收集](https://github.com/shaipe/course/blob/master/mac/tool.md)
[Mac系统中apache的配置使用](https://github.com/shaipe/course/blob/master/apache/mac-apache.md)

## Linux Tutorial
[Linux学习教程](https://github.com/shaipe/Linux-Tutorial)

## Pyhton Tutorial
[Python 学习教程](./python)

## Go Lang Tutorial
[Go语言学习教程](./go/)
