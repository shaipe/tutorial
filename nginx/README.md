# Nginx

## Docker

### 安装

```bash
docker pull nginx

```
### 处理配置文件和默认主页

```bash
# 启动一个不做任何挂载的nginx容器,并停止后自动删除容器
docker run --rm --name nginx -p 80:80 -d nginx

# 将nginx自带的配置和html拷贝到宿主机的docker指定目录中
docker cp nginx:/etc/nginx /srv/docker/nginx/conf
cd /srv/docker/nginx # 进入宿主机的nginx目录
# mv nginx conf # 修改拷贝过来的nginx配置目录名为conf

# 拷贝默认首页到宿主机
docker cp nginx:/usr/share/nginx/html /srv/docker/nginx/html

# 停止临时nginx
docker stop nginx
```

### 运行

```bash
docker run \
--publish 80:80 \
--publish 443:443 \
--publish 8081-8099:8081-8099 \
--name nginx \
-v /srv/docker/nginx/log/:/var/log/nginx \
-v /srv/docker/nginx/html:/usr/share/nginx/html \
-v /srv/docker/nginx/conf:/etc/nginx \
--restart always \
-d nginx

# 重新加载配置文件
docker exec -it nginx /etc/init.d/nginx reload
```

### 更新Nginx配置

```bash
docker exec -it nginx nginx -t  # 测试配置是否正确
```

创建完 nginx 的配置文件之后,我们要让配置生效, 必须让 nginx 加载conf.d 目录下面的配置.

```conf
server {
    listen  80;
    server_name  docker.xxxxx.net;
    access_log /var/log/nginx/docker.access.log main;
    error_log /var/log/nginx/docker.error.log error;
    location / {
        proxy_set_header  Host  $http_host;
        proxy_set_header  X-Real-IP  $remote_addr;
        proxy_set_header  X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_pass  http://172.16.1.37:9000;
    }
}

```

```bash
docker exec -it nginx /etc/init.d/nginx reload
```

## Centos

### 安装

```
    yum install -y openssl # 安装ssl模块
    yum install -y zlib    # zlib模块
    yum install -y nginx   # 安装 nginx
```

### 启动

```bash
    systemctl start nginx.service   # 启动
    systemctl stop nginx.service    # 停止
    systemctl restart nginx.service # 重启
    systemctl enable nginx.service # 开机自启动
```

### 参数

```bash
# nginx在centos下的配置目录为 /etc/nginx/ 
nginx -参数
#  -c：使用指定的配置文件而不是conf目录下的nginx.conf 。
#  -t：测试配置文件是否正确，在运行时需要重新加载配置的时候，此命令非常重要，用来检测所修改的配置文件是否有语法错误。
#  -s：reload 重载
#  -s：stop 停止
```

##  Mac

### Nginx信息

名称 | 说明
---|---
Version | 1.12.1
use prot | 8010
program dir | /usr/local/cellar/nginx
config path | /usr/local/etc/nginx/nginx.conf

### Nginx 启动停止

```bash
sudo nginx -t #测试配置文件是否有错
sudo nginx  # 启动Nginx服务
sudo nginx -s stop # 停止Nginx服务
sudo nginx -s reload   # 重启nignx服务
```

### 设置随系统启动
通过brew install nginx后设置开机启动项


```bash
sudo cp /usr/local/opt/nginx/*.plist /Library/LaunchDaemons #拷贝启动文件到系统启动目录
sudo launchctl load -w /Library/LaunchDaemons/homebrew.mxcl.nginx.plist #注册为系统服务,添加随系统启动
sudo launchctl unload -w /Library/LaunchDaemons/com.nginx.plist  #卸载系统服务
normanygtekimbp:LaunchDaemons normanyang$ ps -ef | grep nginx   #查看nginx是否启动
```

## Windows 

### nginx 启动，重启，停止 

```shell
nginx # 启动

nginx -s reload # 重启
nginx reload  #重启

# 通过进程结束的方式停止
```

### 站点配置

```shell
/conf/nginx.conf

Server #中进行配置

./nginx  #打开 nginx
nginx -s reload|reopen|stop|quit  #重新加载配置|重启|停止|退出 nginx
nginx -t   #测试配置是否有语法错误

nginx [-?hvVtq] [-s signal] [-c filename] [-p prefix] [-g directives]

-?,-h           : 打开帮助信息
-v              : 显示版本信息并退出
-V              : 显示版本和配置选项信息，然后退出
-t              : 检测配置文件是否有语法错误，然后退出
-q              : 在检测配置文件期间屏蔽非错误信息
-s signal       : 给一个 nginx 主进程发送信号：stop（停止）, quit（退出）, reopen（重启）, reload（重新加载配置文件）
-p prefix       : 设置前缀路径（默认是：/usr/local/nginx/）
-c filename     : 设置配置文件（默认是：/usr/local/nginx/conf/nginx.conf）
-g directives   : 设置配置文件外的全局指令

```

### 80端口被占用不能启动处理方式
```
看到80端口果真被占用。发现占用的pid是4，名字是System。怎么禁用呢？
 
1、打开注册表：regedit
 
2、找到：HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\services\HTTP
 
3、找到一个REG_DWORD类型的项Start，将其改为0
 
4、重启系统，System进程不会占用80端口
 
重启之后，start nginx.exe 。在浏览器中，输入127.0.01，即可看到亲爱的“Welcome to nginx!” 了。
```

nginx配置gzip压缩

      默认情况下，Nginx的gzip压缩是关闭的，也只对只对text/html进行压缩，需要在编辑nginx.conf文件，在http段加入一下配置，常用配置片段如下：
    
      gzip    on;
      gzip_comp_level  6;    # 压缩比例，比例越大，压缩时间越长。默认是1
      gzip_types    text/xml text/plain text/css application/javascript application/x-javascript application/rss+xml;     # 哪些文件可以被压缩
      gzip_disable    "MSIE [1-6]\.";     # IE6无效

http的vary头在nginx中的配置方法

gzip_vary on

## Nginx配置


### 反向代理配置

现在我们就可以来设置nginx反向代理了, 其实也非常的简单, 直接在它的配置文件下创建一个.conf 的配置就可以了.

```conf
$ vi /app/nginx/conf.d/test.conf
# 复制下面的并粘贴上去

server {
    listen 80;
    server_name 域名;    # 把域名替换成你自己的
    location / {
    proxy_redirect off;  
        proxy_set_header Host $host;  
        proxy_set_header X-Real-IP $remote_addr;  
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;  
        proxy_pass http://ip:port;      # 这里设置你要代理的ip+端口
      }
}
```


```bash
##定义nginx运行的用户各用户组
user nginx nginx;
##nginx进程数，建议设置与cpu核心数一致
worker_processes 1;
worker_cpu_affinity 00000001 00000010 00000100 00001000 00010000 00100000 01000000 10000000;


##全局错误日志定义类型[ debug | info | notice | warn | error | crit ]

#error_log logs/error.log;
#error_log logs/error.log notice;
#error_log logs/error.log info;


##一个nginx进程打开的最多文件描述符数目，理论值应该是最多打开文件数（系统的值ulimit -n）与nginx进程数相除，但是nginx分配请求并不均匀，所以建议与ulimit -n的值保持一致。
worker_rlimit_nofile 65535;


##进程文件
#pid logs/nginx.pid;


##工作模式与连接数上限
events {
　　##参考事件模型，use [ kqueue | rtsig | epoll | /dev/poll | select | poll ]; epoll模型是Linux 2.6以上版本内核中的高性能网络I/O模型，如果跑在FreeBSD上面，就用kqueue模型。
　　use epoll;
　　##单个进程的最大连接数
　　worker_connections 65535;
}


##设置http服务器
http {
　　##引入外置配置文件
　　include /etc/nginx/conf.d/*.conf;


　　##文件扩展名与文件类型映射表
　　include mime.types;


　　##默认文件类型
　　default_type application/octet-stream;


　　##默认编码
　　#charset utf-8;
　　##服务器名字的hash表大小
　　#server_name_hash_bucket_size 128;


　　##上传文件大小限制   建议打开
　　client_header_buffer_size 32K;


　　##设定请求缓存 建议打开
　　large_client_header_buffers 4 64K;


　　##最大缓存
　　client_max_body_size 20M;


client_header_timeout        20;


　　##日志格式设定
　　#log_format main '$remote_addr - $remote_user [$time_local] "$request" '
　　# '$status $body_bytes_sent "$http_referer" '
　　# '"$http_user_agent" "$http_x_forwarded_for"';

　　##访问日志
　　#access_log logs/access.log main;


　　##开启高效文件传输模式sendfile指令指定nginx是否调用sendfile函数来输出文件，对于普通应用设为 on，如果用来进行下载等应用磁盘IO重负载应用，可设置为off，以平衡磁盘与网络I/O处理速度，降低系统的负载。注意：如　　　　果图片显示不正常把这个改成off。
　　sendfile on;
　　##开启目录列表访问，合适下载服务器，默认关闭
　　#autoindex on;


　　##防止网络阻塞  建议打开
　　tcp_nopush on;
　　##防止网络阻塞  建议打开
　　tcp_nodelay on;


　　##长链接超时时间，单位是秒，为0，无超时　　
　　keepalive_timeout 65;


　　##gzip模块设置
　　##开启gzip压缩输出     建议打开
　　gzip on;
　　##最小压缩文件大小    建议打开
　　gzip_min_length 1k;
　　##压缩缓冲区   建议打开
　　gzip_buffers 4 16k;
　　##压缩版本（默认1.1，前端如果squid2.5请使用1.0）   建议打开
　　gzip_http_version 1.0;
　　##压缩等级
　　gzip_comp_level 2;     建议打开
　　##压缩类型，默认就已经包含了textxml,默认不用写，写上去也没有问题，会有一个warn    建议打开
　　gzip_types text/plain application/x-javascript text/css application/xml;
　　gzip_vary on;
　　##开启连接限制ip连接数使用
　　#limit_zone crawler $binary_remote_addr 10m;


##反向代理缓存Proxy Cache配置
proxy_temp_path  /opt/cache/nginx/temp;
proxy_cache_path /opt/cache/nginx/cache levels=1:2 keys_zone=infcache:600m inactive=1d max_size=2g;
proxy_cache_path  /opt/cache/nginx/proxy_cache_image  levels=1:2   keys_zone=cache_image:3000m inactive=1d max_size=10g;
proxy_connect_timeout 30; 
proxy_read_timeout        60; 
proxy_send_timeout        20; 
proxy_buffer_size        96k; 
proxy_buffers        8 256k; 
proxy_busy_buffers_size        512k; 
proxy_temp_file_write_size        512k;


#proxy_cache_path配置
#keys_zone=infcache:600m 表示这个zone名称为infcache，分配的内存大小为600MB
#/opt/cache/nginx/cache 表示cache这个zone的文件要存放的目录
#levels=1:2 表示缓存目录的第一级目录是1个字符，第二级目录是2个字符，即/data/ngx_cache/cache1/a/1b这种形式
#inactive=1d 表示这个zone中的缓存文件如果在1天内都没有被访问，那么文件会被cache manager进程删除掉
#max_size=10g 表示这个zone的硬盘容量为10GB
 
　　##FastCGI相关参数是为了改善网站的性能：减少资源占用，提高访问速度。
　　fastcgi_connect_timeout 300;
　　fastcgi_send_timeout 300;
　　fastcgi_read_timeout 300;
　　fastcgi_buffer_size 64k;
　　fastcgi_buffers 4 64k;
　　fastcgi_busy_buffers_size 128k;
　　fastcgi_temp_file_write_size 128k;
 
　　##负载均衡,weight权重，权值越高被分配到的几率越大
　　upstream myserver{
　　　　server 192.168.1.10:8080 weight=3;
　　　　server 192.168.1.11:8080 weight=4;
　　　　server 192.168.1.12:8080 weight=1;
　　}


　　##虚拟主机配置
　　server {
　　　　##监听端口
　　　　listen 80;


　　　　##域名可以有多个，用空格隔开
　　　　server_name localhost;


　　　　#charset koi8-r;
　　　　##定义本虚拟主机的访问日志
　　　　#access_log logs/host.access.log main;


　　　　location / {
　　　　　　root html;
　　　　　　index index.html index.htm;
　　　　}
　　　　##图片缓存时间设置
　　　　location ~.*.(gif|jpg|jpeg|png|bmp|swf)${
　　　　　　expires 10d;
　　　　}　　
　　　　##js和CSS缓存时间设置
　　　　location ~.*.(js|css)?${
　　　　　　expires 1h;
　　　　}
　　　　#error_page 404 /404.html;
　　　　# redirect server error pages to the static page /50x.html
　　　　#error_page 500 502 503 504 /50x.html;
　　　　location = /50x.html {
　　　　　　root html;
　　　　}
　　　　# proxy the PHP scripts to Apache listening on 127.0.0.1:80
　　　　#
　　　　#location ~ \.php$ {
　　　　　　# proxy_pass http://127.0.0.1;
　　　　#}
　　　　# pass the PHP scripts to FastCGI server listening on 127.0.0.1:9000
　　　　#
　　　　#location ~ \.php$ {
　　　　　　# root html;
　　　　　　# fastcgi_pass 127.0.0.1:9000;
　　　　　　# fastcgi_index index.php;
　　　　　# fastcgi_param SCRIPT_FILENAME /scripts$fastcgi_script_name;
　　　　　　# include fastcgi_params;
　　　　#}
　　　　# deny access to .htaccess files, if Apache's document root
　　　　# concurs with nginx's one
　　　　#
　　　　#location ~ /\.ht {
　　　　　　# deny all;
　　　　#}
　　　　
　　　　##对 "/" 启用反向代理
　　　　location / {
　　　　　　##或者使用
　　　　　　#proxy_pass http://myserver;
　　　　　　proxy_pass http://127.0.0.1:88;
　　　　　　proxy_redirect off;
　　　　　　proxy_set_header X-Real-IP $remote_addr;#后端的Web服务器可以通过X-Forwarded-For获取用户真实IP
　　　　　　proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
　　　　　　#以下是一些反向代理的配置，可选。
　　　　　　proxy_set_header Host $host;
　　　　　　client_max_body_size 10m; 　　　　　　#允许客户端请求的最大单文件字节数
　　　　　　client_body_buffer_size 128k; 　　　　　#缓冲区代理缓冲用户端请求的最大字节数，
　　　　　　proxy_connect_timeout 90; 　　　　　　#nginx跟后端服务器连接超时时间(代理连接超时)
　　　　　　proxy_send_timeout 90;　　　　　　　　 #后端服务器数据回传时间(代理发送超时)
　　　　　　proxy_read_timeout 90;　　　　　　　　 #连接成功后，后端服务器响应时间(代理接收超时)
　　　　　　proxy_buffer_size 4k; 　　　　　　　　　　#设置代理服务器（nginx）保存用户头信息的缓冲区大小
　　　　　　proxy_buffers 4 32k;　　　　　　　　　　 #proxy_buffers缓冲区，网页平均在32k以下的设置
　　　　　　proxy_busy_buffers_size 64k; 　　　　　　#高负荷下缓冲大小（proxy_buffers*2）
　　　　　　proxy_temp_file_write_size 64k;　　　　　　#设定缓存文件夹大小，大于这个值，将从upstream服务器传
　　　　}
　　　　##设定查看Nginx状态的地址
　　　　location /NginxStatus {
　　　　　　stub_status on;
　　　　　　access_log on;
　　　　　　auth_basic "NginxStatus";
　　　　　　auth_basic_user_file confpasswd;
　　　　　　#htpasswd文件的内容可以用apache提供的htpasswd工具来产生。
　　　　}
　　　　##本地动静分离反向代理配置
　　　　#所有jsp的页面均交由tomcat或resin处理
　　　　location ~ .(jsp|jspx|do)?$ {
　　　　　　proxy_set_header Host $host;
　　　　　　proxy_set_header X-Real-IP $remote_addr;
　　　　　　proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
　　　　　　proxy_pass http://127.0.0.1:8080;
　　　　}
　　　　##所有静态文件由nginx直接读取不经过tomcat或resin
　　　　location ~ .*.(htm|html|gif|jpg|jpeg|png|bmp|swf|ioc|rar|zip|txt|flv|mid|doc|ppt|pdf|xls|mp3|wma)$
　　　　　　{ expires 15d; }
　　　　location ~ .*.(js|css)?$
　　　　　　{ expires 1h; }
　　}
}

```


### 学习资料

[Nginx+Keepalived搭建高可用负载均衡集群](https://blog.51cto.com/qicheng0211/1695674)

[Nginx 高可用集群解决方案 Nginx + Keepalived](https://zhuanlan.zhihu.com/p/108577218)

[Nginx配置——搭建 Nginx 高可用集群（双机热备）](https://blog.csdn.net/zxd1435513775/article/details/102508573)



### Nginx 配置多个证书

http://nginx.org/download/nginx-1.20.0.tar.gz

https://www.openssl.org/source/openssl-1.1.1k.tar.gz

./configure --user=nginx --group=nginx --with-http_ssl_module --with-openssl=../openssl-1.1.1k --with-openssl-opt=enable-tlsext 





./configure --prefix=/usr/local/nginx --with-http_ssl_module \
--with-openssl=../openssl-1.1.1k \
--with-openssl-opt="enable-tlsext"



make && mkie install



vim /etc/profile 

*##* 

export PATH=<nginx-install-dir:default:/usr/local/nginx/sbin>:$PATH 

*##*

```
user  nginx;
worker_processes  1;

error_log  /var/log/nginx/error.log warn;
pid        /var/run/nginx.pid;


events {
    worker_connections  1024;
}


http {
    include       /etc/nginx/mime.types;
    default_type  application/octet-stream;

    log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
                      '$status $body_bytes_sent "$http_referer" '
                      '"$http_user_agent" "$http_x_forwarded_for"';

    access_log  /var/log/nginx/access.log  main;

    sendfile        on;
    #tcp_nopush     on;

    keepalive_timeout  65;

    #gzip  on;

    include /etc/nginx/conf.d/*.conf;
}
```



#### 问题场景

服务器重启后，重启nginx时报错nginx: [error] open() "/usr/local/nginx/logs/nginx.pid" failed (2: No such file or directory)，进入到logs目录发现确实没有nginx.pid文件



```bash
cd /usr/local/nginx/sbin/
./nginx -s reload
ngx_http_fastdfs_set pid=1412
ngx_http_fastdfs_set pid=1412
ngx_http_fastdfs_set pid=1412
ngx_http_fastdfs_set pid=1412
nginx: [error] open() "/usr/local/nginx/logs/nginx.pid" failed (2: No such file or directory)
```

#### 解决办法

使用指定nginx.conf文件的方式重启nginx



```bash
/usr/local/nginx/sbin/nginx -c /usr/local/nginx/conf/nginx.conf
```

此时去logs目录下查看发现nginx.pid文件已经生成了



```conf
# 此配置可以禁止无论输入http的二级都可以跳转到默认配置上
server {
        listen       80 default_server;
        server_name  _;
        return 403;
    }
```