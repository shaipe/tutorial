# 使用Tauri + vue3开发桌面应用

此文档用于对node已有一些了解，在此就有写关于node,yarn,npm的安装和使用问题

## 创建 vue应用

#### 首先使用yarn或npm安装vue的cli工具

```bash
# 安装全局的vue客户端工具
yarn global add @vue/cli

# 创建vue项目
yarn create @vitejs/app project-name

# 进入项目目录
cd project-name

# 安装依赖
yarn

# 运行vue项目
yarn dev
```

## 初始化tauri

#### 引入tauri

```bash
# 添加tauri包引用
yarn add -D @tauri-apps/cli
```



#### modify package.json

```json
{
  // This content is just a sample
  "scripts": {
    "tauri": "tauri"
  }
}
```

#### 初始化和检测tauri引入的正确性

```bash

# 初始化tauri
# 添加
yarn tauri init

yarn tauri info
```

