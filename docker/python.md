# Python for Docker

## 编写Dockerfile

```shell
# 
FROM centos:latest
# 
MAINTAINER Shaipe
LABEL name="python3" license="MIT" build-date="20181101"
# define python version
ENV VERSION 3.7.1
# define python version download url
ENV PYTHON_URL https://www.python.org/ftp/python/$VERSION/Python-$VERSION.tgz
# 
ENV PYTHON_HOME /usr/local/python3
ENV PATH $PATH:$PYTHON_HOME/bin:

RUN yum update -y && cd /usr/local/softs && wget $PYTHON_URL && tar -zxvf Python-$VERSION.tgz && cd Python-$VERSION && mkdir -p $PYTHON_HOME && yum install libffi-devel -y && ./configure --prefix=/usr/local/python3 && make && make install
RUN rm -rf /usr/local/softs/*

CMD ["python3", "-V"]
```
