# Docker 中使用gitlab

## 手动备份

```bash
# 第一种进行入容器执行命令的方法进行手工备份
docker exec -it 容器名或容器id bash # 进入容器
gitlab-rake gitlab:backup:create    # 执行gitlab备份命令

# 第二种直接使用外部命令执行,一次完成
docker exec 容器名或容器id gitlab-rake gitlab:backup:create
```

## 自动备份

需要使用linux的crontab的自动执行命令来进行备份
**注意这里是在宿主机上设置,在gitlab的docker中是没有crontab的**
1. 创建手动备份执行脚本

```shell
#! /bin/bash
case "$1" in 
    start)
            docker exec gitlab-ce11.2.3 gitlab-rake gitlab:backup:create
            ;;
esac
```

2. 给脚本可执行权限

```bash
chmod +x /root/gitlab_backup.sh
```

2. 创建定时执行计划

```bash
# 使用crontab -e 进入定时任务编辑界面，新增如下内容
crontab -e
# 编辑定时任务
0 2 * * * /root/gitlab_backup.sh start
```

## Crontab 命令说明

crontab -e 进入
*  *  *  *  *  command
分  时  日  月  周  命令

其中，
第1列表示分钟，1~59，每分钟用*表示
第2列表示小时，1~23，（0表示0点）
第3列表示日期，1~31
第4列表示月份，1~12
第5列表示星期，0~6（0表示星期天）
第六列表示要运行的命令。


1.  /root/bin/autoback.sh # 添加自动备份执行文件
2.  /root/bin/autorun.sh    # 添加自动运行扫行文件
3. 使用crontab -e 添加了第天2点执行自动备份gitlab文件
4. 在docker中 pull redis 并添加了名为docker-ridis 的redis容器
5. 在/ect/rc.d/rc.local中添加了开机自动执行的/root/bin/autorun.sh脚本

### 创建容器脚本

```bash
sudo docker run -i \
    --hostname centos \
    --publish 443:443 --publish 80:80 --publish 12233:22 \
    --name gitlab \
    --restart always \
    --volume /srv/gitlab/config:/etc/gitlab \
    --volume /srv/gitlab/logs:/var/log/gitlab \
    --volume /srv/gitlab/data:/var/opt/gitlab \
    --volume /srv/gitlab/logs/reconfigure:/var/log/gitlab/reconfigure \
    gitlab/gitlab-ce:11.2.3-ce.0


docker run -d \
-p 9443:443 \
-p 980:80 \
-p 9222:22 \
--name gitlab \
--restart always \
-v /srv/gitlab/config:/etc/gitlab \
-v /srv/gitlab/logs:/var/log/gitlab \
-v /srv/gitlab/data:/var/opt/gitlab \
gitlab/gitlab-ce
```

### 修改gitlab的域名

```bash
# 进入容器
sudo docker exec -it gitlab bash

# 修改配置文件
vi /opt/gitlab/embedded/service/gitlab-rails/config/gitlab.yml

# 找到以下内容进行修改
# gitlab:
# 	host: xxx.ccx.com

# 重启服务
gitlab-ctl restart
```





### 1、 编辑gitlab.yml配置文件



```undefined
vim /opt/gitlab/embedded/service/gitlab-rails/config/gitlab.yml
```

**`找到host，并修改为你要配置的域名或IP`**



```bash
 ## GitLab settings
  gitlab:
    ## Web server settings (note: host is the FQDN, do not include http://)
    host: 192.168.0.201
    port: 80
    https: false
```

### 2、 编辑gitlab.rb文件



```undefined
vim /etc/gitlab/gitlab.rb
```

**`找到external_url，修改成对应的域名或IP`**

```bash
## Url on which GitLab will be reachable.
## For more details on configuring external_url see:
## https://gitlab.com/gitlab-org/omnibus-gitlab/blob/master/doc/settings/configuration.md#configuring-the-external-url-for-gitlab
external_url 'http://192.168.0.201'
```

### 3、 重启GitLab服务



```undefined
gitlab-ctl restart
```





### 管理员root密码找回

或者我用docker创建gitlab第一次登陆时没有弹出root用户初始化界面时，按照以下方法可以重置管理员密码

```bash
# 进入docker 容器

docker exec -it gitlab（容器名字） /bin/bash

# 启用docker里面gitlab的ruby
gitlab-rails console -e production

# 找到管理员用户
user = User.where(id: 1).first

# 更改密码
user.password = 'abcd1234'
user.password_confirmation = 'abcd1234'

# 记得保存
user.save!
```



### 从远程服务器拉文件到本地

scp root@192.168.4.136:/srv/gitlab//data/backups/1548910439_2019_01_31_11.2.3_gitlab_backup.tar ~/workspace/