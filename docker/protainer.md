# Portainer 安装部署

## 安装

```bash
docker pull portainer/portainer

```

## 创建容器

```bash
docker run -d \
-p 9000:9000 \
--name portainer \
--restart always \
-v /var/run/docker.sock:/var/run/docker.sock \
-v /srv/portainer/data:/data \
portainer/portainer
```