# ubuntu rust

## build

```bash
docker build -t shaipe/rust-ubuntu:latest ./
```

## Installation

```bash

# 运行docker
docker run -idt \
--name urust \
--restart always \
-v ~/workspace/rust:/data \
-w /data \
shaipe/ubuntu \
/bin/bash

# 进入docker环境
docker exec -it rust-ubuntu bash

# 系统升级
apt-get update && apt-get install curl && apt-get install vim

# install rust
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh

# config PATH
vim ~/.bashrc
# insert path
export PATH="$HOME/.cargo/bin:$PATH"

source ~/.bashrc

# install ssh cert
apt-get install -y ca-certificates
# open ssl
apt-get install -y openssl
# install cc builder not install complie error: linker `cc` not found
# To install gcc on Ubuntu, simply run:
apt install build-essential
apt install libssl-dev
apt install openssl-devel
# install and config openssl
apt install pkg-config

```

## config cargo source

```
# write `$HOME/.cargo/config` in file
[source.crates-io]
registry = "https://github.com/rust-lang/crates.io-index"

# replace you like source
replace-with = 'sjtu'
#replace-with = 'ustc'

# qinghua
[source.tuna]
registry = "https://mirrors.tuna.tsinghua.edu.cn/git/crates.io-index.git"

# zhong ke da
[source.ustc]
registry = "git://mirrors.ustc.edu.cn/crates.io-index"

# shanghai jiaotong 
[source.sjtu]
registry = "https://mirrors.sjtug.sjtu.edu.cn/git/crates.io-index"

# rustcc comtriy
[source.rustcc]
registry = "git://crates.rustcc.cn/crates.io-index"
```



## commit Image

docker commit -m 'update' rust-ubuntu shaipe/rust-ubuntu 

docker push shaipe/rust-ubuntu 


## use image 

```bash
docker run -idt \
--name urust \
--restart always \
-v ~/workspace/rust:/data \
shaipe/rust-ubuntu \
/bin/bash
```