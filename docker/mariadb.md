# Mariadb 在docker中配置

## Mariadb 安装

```bash
docker pull Mariadb

# 修改已存在容器的时区
cp /usr/share/zoneinfo/PRC /etc/localtime
# 或者
ln -sf /usr/share/zoneinfo/Asia/Shanghai /etc/localtime
# 退出
exit
```

## Mariadb 配置

### 第一步创建一个mariadb容器

```bash
# MYSQL_ROOT_PASSWORD=mysql.root 这里是给定mariadb中root的初始密码
docker run  --name mariadb \
-p 3306:3306 \
-v /srv/mariadb/data:/var/lib/mysql \
-e MYSQL_ROOT_PASSWORD=mysql.root \
--restart always \
-d mariadb
```

### 第二步把mariadb自动生成的my.cnf拷贝到映射目录

```bash
# 拷贝文件
docker cp mariadb3306:/etc/mysql/my.cnf ~/docker mariadb/conf/ 
# 停止刚创建的容器
docker stop mariadb3306
# 删除开始创建的容器
docker rm mariadb3306

```

### 第三步修改my.cnf

```bash
vim ~/docker/mariadb/conf/my.cnf

# [mysqld] 下添加配置项
# ignore table names
lower_case_table_names = 1
# set time zone 
default-time_zone=+8:00

```

### 最后生新创建容器

```bash
# 重新创建容器并挂载外部配置文件
docker run  \
--name mariadb-3306 \
-p 3306:3306 \
--restart always \
-v /srv/mariadb/conf:/etc/mysql/conf.d \
-v /srv/mariadb/data:/var/lib/mysql \
-e MYSQL_ROOT_PASSWORD=mysql.shaipe.root \
-d mariadb

docker run  \
--name mysql-3307 \
-p 3307:3306 \
--rm \
-v /srv/mysql/3307/data:/var/lib/mysql \
-e MYSQL_ROOT_PASSWORD=<youpassword> \
-d mysql:5.7.20
```
