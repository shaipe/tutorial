Alpine Linux 在Docker中的使用
===

## 容器中配置开机自启动

```bash
##直接使用sh环境是无法使用init系统的
docker run -itd --name alpine --privileged alpine:3.8 ash
#后面环境改为init才可以使用init系统
docker run -itd --name alpine --privileged alpine:3.8 init
#进入容器
docker exec -it alpine ash
1.进入容器安装OpenRC系统
apk update 
apk add openrc --no-cache
#设置 local 服务开机启动
rc-update add local
#安装bash环境，下次就可以使用bash环境进入容器
apk add bash --no-cache
2.创建启动文件
#启动文件在/etc/local.d目录
#启动文件的后缀必须为.start
#Alpine Linux自带了nohup后台守护
cat > /etc/local.d/test.start <<"EOF"
/bin/echo 'Test Docker Alpine linux'>/test.txt
EOF
chmod +x /etc/local.d/test.start
```

### 使用过程

```bash
#检查/etc/apk/repositories中有没有/alpine/edge/community，没有则添加
#http://dl-cdn.alpinelinux.org/alpine/edge/community
apk update
apk add docker --no-cache
#将docker加入开机自启
rc-update add docker boot
#启动docker
rc-service docker start
#错误：ERROR: docker needs service(s) cgroups
根据https://bugs.alpinelinux.org/issues/8916这个说法是community与main搭配有点问题
解决办法是升级openrc
apk add --upgrade openrc
#重启
rc-service docker restart
```
