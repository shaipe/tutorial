Docker使用过程中的常用命令
===

### Dockerr 配置安装
#### 1. 仓库配置
```bash
[root@localhost ~]# vim /etc/yum.repos.d/docker.repo  #创建docker仓库
```
添加以下内容：
```bash
[docker-repo]
name=Docker Repository
baseurl=https://yum.dockerproject.org/repo/main/centos/7/
enabled=1
gpgcheck=1
gpgkey=https://yum.dockerproject.org/gpg
```

#### 2. 使用YUM安装Docker
```bash
[root@localhost ~]# yum install docker -y
```
#### 3.启动Docker并设置为开机自启动

[root@localhost ~]# systemctl stop firewalld.service 
[root@localhost ~]# setenforce 0
[root@localhost ~]# systemctl start docker.service
[root@localhost ~]# systemctl enable docker.service
#### 4.检查Docker版本可以查看Docker服务
```bash
[root@localhost ~]# docker version
Client:     #docker客户端
 Version:         1.13.1
 API version:     1.26
 Package version: docker-1.13.1-68.gitdded712.el7.centos.x86_64
 Go version:      go1.9.4
 Git commit:      dded712/1.13.1
 Built:           Tue Jul 17 18:34:48 2018
 OS/Arch:         linux/amd64

Server:    #docker服务端
 Version:         1.13.1
 API version:     1.26 (minimum version 1.12)
 Package version: docker-1.13.1-68.gitdded712.el7.centos.x86_64
 Go version:      go1.9.4
 Git commit:      dded712/1.13.1
 Built:           Tue Jul 17 18:34:48 2018
 OS/Arch:         linux/amd64
 Experimental:    false
```
注解：
安装好的Docker系统有两个程序：Docker服务端和Docker客户端。其中Docker服务端是一个服务进程，管理着所有的容器。Docker客户端则扮演着Docker服务端的远程控制器，可以用来控制Docker的服务端进程。大部分情况下Docker服务端和客户端运行在一台机器上。
Docker镜像操作
Docker运行容器前需要本地存在对应的镜像，如果不存在本地镜像Docker就会尝试从默认镜像仓库下载https://hub.docker.com

### Docker 操作
#### 1. 搜索镜像

**命令格式：``docker search`` 关键字**

在使用下载镜像前，可以使用docker search命令，搜索远端官方仓库中的共享镜像
```bash
[root@localhost ~]# docker search httpd   #搜索关键字为httpd的镜像
INDEX       NAME                                              DESCRIPTION                                     STARS     OFFICIAL   AUTOMATED
docker.io   docker.io/httpd                                   The Apache HTTP Server Project                  1885      [OK]       
docker.io   docker.io/hypriot/rpi-busybox-httpd               Raspberry Pi compatible Docker Image with ...   41                   
docker.io   docker.io/centos/httpd                                                                            19                   [OK]
docker.io   docker.io/centos/httpd-24-centos7                 Platform for running Apache httpd 2.4 or b...   14                   
docker.io   docker.io/armhf/httpd                             The Apache HTTP Server Project                  8                    
docker.io   docker.io/macadmins/netboot-httpd                 use in combination with bruienne/bsdpy          6                    [OK]
docker.io   docker.io/salim1983hoop/httpd24                   Dockerfile running apache config                2                    [OK]
docker.io   docker.io/epflidevelop/os-wp-httpd                WP httpd                                        1                    [OK]
docker.io   docker.io/fboaventura/dckr-httpd                  Small footprint http server to use with ot...   1                    [OK]
docker.io   docker.io/lead4good/httpd-fpm                     httpd server which connects via fcgi proxy...   1                    [OK]
docker.io   docker.io/tplatform/aws-linux-httpd24-php70       aws-linux-httpd24-php70                         1                    [OK]
docker.io   docker.io/tplatform/aws-linux-httpd24-php71-fpm   aws-linux-httpd24-php71-fpm                     1                    [OK]
......//省略
```
参数注解：
- NAME:镜像名称
- DESCRIPTION：描述信息
- STARS ：星级
- OFFICIAL：是否官方创建
- AUTOMATED：是否主动创建

默认的输出结果会按照星级评价进行排序，表示该镜像受欢迎程度，在下载镜像时，可以参考这一项，在搜索时还可以使用-s或者--stars=x显示指定星级以上的镜像，星级越高表示越受欢迎。

#### 2. 获取镜像

搜索到符合需求的镜像，可以使用docker pull命令从网络下载镜像到本地使用。

命令格式：``docker pull`` 仓库名称 [：标签]

```bash
[root@localhost ~]# docker pull docker.io/httpd  #下载镜像docker.io/httpd
Using default tag: latest  #最新版本
Trying to pull repository docker.io/library/httpd ... 
latest: Pulling from docker.io/library/httpd
d660b1f15b9b: Pull complete 
aa1c79a2fa37: Pull complete 
f5f6514c0aff: Pull complete 
676d3dd26040: Pull complete 
4fdddf845a1b: Pull complete 
520c4b04fe88: Pull complete 
5387b1b7893c: Pull complete 
Digest: sha256:8c84e065bdf72b4909bd55a348d5e91fe265e08d6b28ed9104bfdcac9206dcc8
Status: Downloaded newer image for docker.io/httpd:latest   #下载成功
```
从整个下载的过程可以看出，镜像文件由若干层（Layer）组成，称之为AUFS（联合文件系统），是实现增量保存与更新的基础，下载过程中会输出镜像的各层信息。
注意：
如果下载镜像时不指定标签，则会默认下载仓库中最新版本的镜像，即选择标latest标签，也可以通过指定的标签来下载特点版本的某一镜像。这里的标签（tag）就是用来区分镜像版本的。
#### 3. 查看镜像信息

命令语法：``docker images`` 仓库名称 :[标签]
```bash
[root@localhost ~]# docker images  #查看本地所有镜像
REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
docker.io/httpd     latest              11426a19f1a2        9 days ago          178 MB
```
参数注解：
- REPOSITORY：镜像属于的仓库
- TAG ：镜像的标记信息，标记同一仓库中的不同镜像。
- IMAGE ID ：镜像的唯一ID号，唯一标识了该镜像。
- CREATED ：镜像创建时间
- SIZE ：镜像大小

用户还可以根据镜像的唯一标识ID号，获取镜像详细信息。

命令格式：``docker inspect`` 镜像ID号
```bash
[root@localhost ~]# docker inspect 11426a19f1a2   
```

为本地镜像添加新的标签

命令格式：``docker tag`` 名称 :[标签] 新名称 :[新标签]
```bash
[root@localhost ~]# docker tag docker.io/httpd httpd:httpd
[root@localhost ~]# docker images | grep httpd
docker.io/httpd     latest              11426a19f1a2        9 days ago          178 MB
httpd               httpd               11426a19f1a2        9 days ago          178 MB
```

#### 4. 删除镜像

删除镜像的操作方法有两种：使用镜像的标签删除镜像；使用镜像的ID删除镜像。

命令格式：``docker rmi`` 仓库名称 : 标签
```bash
[root@localhost ~]# docker rmi 11426a19f1a2  #删除镜像的ID号

[root@localhost ~]# docker rmi httpd:httpd   #删除镜像的标签
Untagged: httpd:httpd
[root@localhost ~]# docker images | grep httpd
docker.io/httpd     latest              11426a19f1a2        9 days ago          178 MB
```
**注意：**

1）当一个镜像有多个标签的时候，docker rmi 命令只是删除该镜像多个标签中的指定标签，不会影响镜像文件，但当该镜像只剩下一个标签的时候就要小心了，再使用删除命令就会彻底删除该镜像。

2）当使用docker rmi命令后面跟上镜像的ID号时，必须确保该镜像没有被容器使用才能进行，删除时系统会先删除掉指向该镜像的所有标签，然后删除该镜像文件本身。如果该镜像已经被容器使用，要先删除依赖该镜像的使用容器，再删除镜像。

#### 5. 存储镜像和载入镜像

命令格式:``docker save -o ``存储文件名 存储的镜像
```bash
[root@localhost ~]# docker save -o httpd docker.io/httpd #本地镜像存出文件为httpd
[root@localhost ~]# ls -l httpd
-rw-------. 1 root root 186415616 8月  10 11:24 httpd
```
载入镜像

命令格式：``docker load`` < 存出文件
或者         ``docker --input`` 存出的文件

#### 6. 上传镜像

本地存储的镜像越来越多，就需要指定一个专门的地方存放这些镜像——仓库。目前比较方便的就是公共仓库，默认上传到Docker Hub官方仓库，需要注册使用公共仓库的账号，可以使用docker login命令来输入用户名、密码和邮箱来完成注册登录。在上传镜像之前还需要对本地镜像添加新的标签，然后再使用docker push命令进行上传。

命令格式：``docker push`` 仓库名称 : 标签
```bash
[root@localhost ~]# docker tag docker.io/httpd docke/httpd:centos7 #添加新标签
[root@localhost ~]# docker login   #登录
Username: docke
Password: 

[root@localhost ~]# docker push docke/httpd:centos7   #上传镜像
```

### Docker容器操作
镜像是只读模板，而容器会给这个只读模板一个额外的可写层。

#### 1. 容器的创建与启动

容器的创建就是将镜像加载到容器的过程。新创建的容器默认处于停止状态，不允许任何程序，需要在其中发起一个进程来启动容器，这个进程是该容器的唯一进程，当该进程结束的时候，容器也会完全停止。停止的容器可以重新启动并保留原来的修改。

命令格式：``docker create`` [选项] 镜像 运行的程序

##### 1）创建新容器

-i 让容器的输入保持打开
-t 让Docker分配一个伪终端
```bash
[root@localhost ~]# docker create -it docker.io/httpd /bin/bash
96a826f750d53024dc63c3328c4ac45ae2a499b9ae613d700da38e7be99e452d   #新容器的唯一ID
```
##### 2）使用docker ps命令查看容器的运行状态，-a 列出系统最近一次启动的容器。
```bash
[root@localhost ~]# docker ps -a

CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS              PORTS               NAMES
96a826f750d5        docker.io/httpd     "/bin/bash"         4 minutes ago       Created                                 happy_gates
```
参数注释：
- CONTAINER ID ：容器的ID号
- IMAGE ：加载的镜像
- COMMAND ：运行的程序
- CREATED ：创建的时间
- STATUS  ：目前所处的状态、端口映射

##### 3）启动停止状态的容器

命令格式：``docker start`` 容器的ID/ 名称
```bash
[root@localhost ~]# docker start 96a826f750d5 
96a826f750d5
[root@localhost ~]# docker ps -a
CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS              PORTS               NAMES
96a826f750d5        docker.io/httpd     "/bin/bash"         9 minutes ago       Up 13 seconds       80/tcp              happy_gates
```
##### 4）创建并启动容器，可以指向docker run 命令，等同于先执行docker create 命令，再执行docker start 命令。注意：只有后面的命令运行结束，容器就会停止。
```bash
[root@localhost ~]# docker run centos /usr/bin/bash -c ls / #创建、开启centos容器并查询镜像的根目录
anaconda-post.log
bin
dev
etc
home
lib
lib64
media
mnt
opt
proc
root
run
sbin
srv
sys
tmp
usr
var

[root@localhost ~]# docker ps -a  #查看容器的运行状态
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS                      PORTS               NAMES
db67843e7d92        centos              "/usr/bin/bash -c ..."   31 seconds ago      Exited (0) 30 seconds ago                       festive_goldberg
ae4c24239956        httpd               "/bin/bash"              6 minutes ago       Up 5 minutes                80/tcp              nervous_kalam
```
让容器再后台持续运行
```bash
[root@localhost ~]# docker run -d centose /usr/bin/bash -c "while true;do echo hello;done"
```
#### 2. 容器的运行与终止

命令格式：``docker stop`` 容器的ID/ 名称
```bash
[root@localhost ~]# docker stop ae4c24239956 
ae4c24239956
[root@localhost ~]# docker ps -a
CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS                     PORTS               NAMES
ae4c24239956        httpd               "/bin/bash"         31 minutes ago      Exited (0) 2 seconds ago                       nervous_kalam
```

#### 3.容器的进入

命令格式：``docker exec`` -it 容器 ID/ 名称 /bin/bash

进入正在运行的容器
```bash
[root@localhost ~]# docker exec -it ae4c24239956 /bin/bash   #进入容器
```
用户可以通过所创建的终端来输入命令，通过exit命令退出容器：
```bash
root@ae4c24239956:/usr/local/apache2# ls
bin  build  cgi-bin  conf  error  htdocs  icons  include  logs  modules
root@ae4c24239956:/usr/local/apache2# exit   
exit
```
#### 4. 容器的导出与导入

用户可以将任何一个Docker容器从一台机器迁移到另一台机器。在迁移的过程中，首先需要将已经创建好的容器导出为文件，可以使用docker export命令实现，无论这个容器是处于运行状态还是停止状态均可导出。导出之后可将导出文件传输到其他机器，通过相应的导入命令实现容器的迁移。

命令格式：``docker export`` 容器 ID/ 名称 > 文件名

导出ae4c24239956 容器到文件 centos7tar
```bash
[root@localhost ~]#  docker export ae4c24239956 > centos7tar
[root@localhost ~]# ls -l centos7tar 
-rw-r--r--. 1 root root 182633984 8月  11 18:23 centos7tar
```
导出的文件从A机器拷贝到B机器，之后使用docker import命令导入，成为镜像。

命令格式：``cat 文件名 | docker import`` - 生成的镜像名称 : 标签

导入文件 centos7tar成为本地镜像
```bash
[root@localhost ~]#  cat centos7tar | docker import - centos7:tes
[root@localhost ~]#  docker images | grep centos7
centos7             test                442bc7482423        5 minutes ago       174MB
```
#### 5.容器的删除

命令格式：``docker rm`` 容器ID/ 名称
```bash
[root@localhost ~]#  docker rm ae4c24239956
```
**注意：**
如果要删除一个正在运行的容器，可以添加-f选项强制删除，但建议先将容器停止再做删除操作。

### Docker资源控制
#### 1.对CPU的控制

##### 1）限制CPU使用速率

在Docker中可以通过 --cpu-quota选项来限制CPU的使用率，CPU的百分比是以1000为单位的。
```bash
docker run --cpu-quota 20000 容器名 # CPU的使用率限定为20%
```
在CentOS中还可以通过修改对应的Cgrep配置文件/sys/fs/cgroup/cpu/docker/容器编号/cpu.cfs_quota_us的值来实现，直接执行echo命令将设定值导入到此文件中就会立即生效。
```bash
[root@localhost ~]# echo 20000 > /sys/fs/cgroup/cpu/docker/ae4c24239956ee20786a966cd4db9a459a459e2c47faaf03977ab88d03b7a834/cpu.cfs_quota_us
```
##### 2）多任务按比例分享CPU

当有多个容器任务运行时，很难计算CPU的使用率。为了使容器合理使用CPU资源，可以通过--cpu-share选项设置CPU按比例共享CPU资源，这种方式还可以实现CPU使用率的动态调整。

比如：运行三个容器A、B、C，占用CPU资源的比例为1:1:2，可以这样执行：
```bash
docker run --cpu-share 1024 容器A
docker run --cpu-share 1024 容器B
docker run --cpu-share 2048 容器C
```
##### 3）限制CPU的内核使用

在Docker中可以使用--cpuset-cpus选项来使某些程序独享CPU内核，以便提高其处理速度。

如果服务器有16个内核，那么CPU的编号为0~15,使容器绑定1~5个内核使用，则：
```bash
docker run --cpuset-cpus 0,1,2,3,4 容器名  
```
建议：
尽量使用绑定内核的方式分配CPU资源给容器进程使用，然后再配合 --cpu-share选项动态调整CPU使用资源的比例。

#### 2.对内存使用的限制

在Docker中可以通过docker run -m命令来限制容器内存使用量，相应的Cgroup配置文件为/sys/fs/cgroup/memory/memory.limit_in_bytes 。但是需要注意：一旦容器Cgroup使用的内存超过了限制的容量，Linux内核将会尝试收回这些内存，如果仍旧没法控制内存使用在限制范围之内，进程就会被杀死。

例如：限制容器的内存为512M
```bash
docker run -m 512m 容器名
```
#### 3. 对blkio的限制

如果是在一台服务器上进行容器的混合部署，那么会出现同时有几个程序写磁盘数据的情况，这时可以通过 --device-write-iops选项来限制写入的iops，相应的还有 --device-read-bps选项可以限制读取的iops。但是这种方法只能针对blkio限制的是设备（device）,而不是分区。相应Cgroup写配置文件/sys/fs/cgroup/blkio/docker/容器ID/blkio.throttle.write_iops_device。

例如：限制容器的/dev/sda1 的写入iops为1MB
```bash
docker run --device-write-bps /dev/sda1:1mb 容器名
```