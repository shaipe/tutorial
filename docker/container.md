# Container

目录

===

- [Ftp](#vsftpd)
- [Gitlab](#Gitlab)
- [mops](#mops)
- [Mariadb](#Mariadb)
- [Redis](#Redis)
- [Portainer](#Portainer)
- [MongoDB](#MongoDB)
- [Zookeeper](#Zookeeper)

## Vsftpd

```bash
docker run -d \
-v /srv/ftp:/home/vsftpd \
-p 20:20 -p 21:21 -p 21100-21110:21100-21110 \
--name vsftpd \
--restart=always \
-e FTP_USER=user \
-e FTP_PASS=password \
-e PASV_ADDRESS=127.0.0.1 \
-e PASV_MIN_PORT=21100 \
-e PASV_MAX_PORT=21110 \
fauria/vsftpd

# chown -R ftp:ftp /home/vsftpd

docker run -d \
-p 20:20 -p 21:21 \
-p 21100-21110:21100-21110 \
-v /srv/docker/nginx/html:/home/vsftpd \
-e FTP_USER=front \
-e FTP_PASS=chinasie \
-e PASV_ADDRESS=192.168.17.213 \
-e PASV_MIN_PORT=21100 \
-e PASV_MAX_PORT=21110 \
--name ftp \
--restart=always \
fauria/vsftpd

# 上面这个run是最近运行通过的，如果遇到上传时权限不正确
# 进入容器内部vim /etc/vsftpd/vsftpd.conf 修改ftp服务配置
# 禁止匿名用户使用
anonymous_enable=NO
# 上传目录文件的权限为775
local_umask=002
```

```bash
docker run -d \
-v /mnt/ftp:/home/vsftpd -p 20:20 -p 21:21 \
-p 47400-47470:47400-47470 \
-e FTP_USER=test \
-e FTP_PASS=test \
-e PASV_ADDRESS=0.0.0.0 \
--name ftp \
--restart=always \
bogem/ftp
```

### Gitlab

```bash
sudo docker run -i \
    --hostname centos \
    --publish 443:443 --publish 80:80 --publish 12233:22 \
    --name gitlab \
    --restart always \
    --volume /srv/gitlab/config:/etc/gitlab \
    --volume /srv/gitlab/logs:/var/log/gitlab \
    --volume /srv/gitlab/data:/var/opt/gitlab \
    --volume /srv/gitlab/logs/reconfigure:/var/log/gitlab/reconfigure \
    gitlab/gitlab-ce:11.2.3-ce.0
```

### Python版本的监控系统

```bash
# 创建ops容器
docker run -d \
-p 8001:8000 \
--name mops \
-v /srv/mops/www:/usr/src/app \
--restart=always \
--link mongo-27017:mongo \
shaipe/xpf-py:1.0.3
```

## Mariadb

```bash
# 重新创建容器并挂载外部配置文件
docker run  \
--name mariadb \
-p 3306:3306 \
--restart always \
-v /mnt/mariadb/conf:/etc/mysql/conf.d \
-v /mnt/mariadb/data:/var/lib/mysql \
-e MYSQL_ROOT_PASSWORD=mysql.root_1230 \
-d mariadb

# mac
docker run -it \
--name mariadb \
-p 3306:3306 \
--restart always \
-v ~/docker/mariadb:/var/lib/mysql \
-e MYSQL_ROOT_PASSWORD=mysql.root \
-d mariadb

docker run  \
--name mysql-3307 \
-p 3307:3306 \
--rm \
-v /srv/mysql/3307/data:/var/lib/mysql \
-e MYSQL_ROOT_PASSWORD=<youpassword> \
-d mysql:5.7.20

# 124
docker run  \
--name mariadb \
-p 3306:3306 \
--restart always \
-v /mnt/docker/mariadb/data:/var/lib/mysql \
-e MYSQL_ROOT_PASSWORD=mysql.root \
-d mariadb
```

### Redis

```bash
# mac
docker run -d \
-v ~/docker/redis/conf/redis.conf:/etc/redis/conf/redis.conf \
-v ~/docker/redis/data:/data \
--name redis-6379 \
--restart always \
-p 6379:6379 \
--requirepass "password" \
redis redis-server --appendonly yes --requirepass "redis.root"

# centos 最后是带密码的访问方式
docker run -d \
-v /mnt/docker/redis/conf/redis.conf:/etc/redis/conf/redis.conf \
-v /mnt/docker/redis/data:/data \
--name redis-6379 \
--restart always \
-p 6379:6379 \
redis redis-server --appendonly yes --requirepass "redis.root"

docker run –name redis6700 \
–privileged=true \
-p 6700:6379 \
-v $PWD/data:/data \
-v $PWD/redis.log:/var/log/redis/redis.log \
-d redis redis-server /data/redis.conf


docker run \
--name redis-test \
-p 6379:6379 -d \
--restart=always \
redis:latest redis-server --appendonly yes --requirepass "your password"
# -p 6379:6379 :将容器内端口映射到宿主机端口(右边映射到左边)
# redis-server –appendonly yes : 在容器执行redis-server启动命令，并打开redis持久化配置
# requirepass “your passwd” :设置认证密码
# –restart=always : 随docker启动而启动

```

### Portainer

```bash
docker run -itd \
-p 9000:9000 \
--name portainer \
--restart always \
-v /var/run/docker.sock:/var/run/docker.sock \
-v /srv/portainer/data:/data \
portainer/portainer



```

### MongoDB

```bash
# mac
docker run -d \
-p 27017:27017 \
-v ~/docker/mongo/conf:/data/conf \
-v ~/docker/mongo/data:/data/db \
--name mongo \
--restart always \
mongo:latest

# centos
docker run -d \
--name mongo-27017 \
--restart always \
-p 27017:27017 \
-v /mnt/docker/mongo/conf:/data/conf \
-v /mnt/docker/mongo/data:/data/db \
mongo --auth

# 使用 如下命令给定用户名和密码
# docker exec -it mongo-27017 bash
# mongo
# use.admin  
# db.createUser({
#   user: 'admin',  // 用户名
#   pwd: '123456',  // 密码
#   roles:[{
#     role: 'root',  // 角色
#     db: 'admin'  // 数据库
#   }]
# })

# 124
docker run -d \
--name mongo \
--restart always \
-p 27017:27017 \
-v /mnt/mongo/mongo/conf:/data/conf \
-v /mnt/mongo/mongo/data:/data/db \
mongo


```

### Alpine

```bash
docker run -d \
--name alpine \
--restart always \
-p 7001-7005:7001-7005 \
-v ~/docker/alpine/data:/data \
alpine
```

### centos

```bash
docker run -itd \
-p 8099:80 \
--name centos7 \
--restart always \
-v ~/docker/centos:/data \
centos:latest

# 运行进入centos系统
docker exec -it centos bash
```

### nginx

```bash
docker run \
--publish 801:80 \
--publish 443:443 \
--name nginx \
-v ~/docker/nginx/log/:/var/log/nginx \
-v ~/docker/nginx/html:/usr/share/nginx/html \
-v ~/docker/nginx/conf/nginx.conf:/etc/nginx/nginx.conf \
-v ~/docker/nginx/conf/conf.d:/etc/nginx/conf.d \
--restart always \
-d nginx:1.15-alpine
```

### rust

```bash
docker run -itd \
--name rust2 \
--restart always \
-v ~/workspace/rust/:/data \
shaipe/urust:latest
```

### alex

```bash
docker run -it \
--name alexandrie \
-p 8300:3000 \
--restart always -d \
mediaio/alexandrie bash



docker run -it \
--name alexandrie1 \
-p 8301:3000 \
--restart always -d \
-v  ~/docker/alex:/home/alex \
rtohaan/alexandrie bash
```

### Zookeeper

```bash
docker run -it \
--name zookeeper \
-p 2181:2181 -p 2888:2888 -p 3888:3888 -p 3080:8080 \
--restart always -d \
zookeeper

```

### Elasticsearch

```bash
docker network create somenetwork

docker run -d --name es --net somenetwork -p 9200:9200 -p 9300:9300 -e "discovery.type=single-node" elasticsearch:7.11.2

docker run -d --name es -p 9200:9200 -p 9300:9300 -e "discovery.type=single-node" elasticsearch:7.11.2

docker run -it --name es -p 9200:9200 -p 9300:9300 -e "discovery.type=single-node" -v ~/docker/es:/usr/share/elasticsearch/data --restart always -d docker.elastic.co/elasticsearch/elasticsearch:7.12.1

```

