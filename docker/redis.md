# Redis 安装及使用操作


## Redis 在Docker中的使用

### Docker安装

```bash
docker pull Redis # 安装最新版本的redis
```

### 创建存放Redis的目录

```bash
# 创建存放 redis 容器的目录
mkdir /home/docker/redis/{conf,data}
cd /home/docker/redis
```

### 获取Redis的默认配置模板

```bash
# 获取 redis 的默认配置模版
# 这里主要是想设置下 redis 的 log / password / appendonly
# redis 的 docker 运行参数提供了 --appendonly yes 但没 password
wget https://raw.githubusercontent.com/antirez/redis/4.0/redis.conf -O conf/redis.conf

# 直接替换编辑
sed -i 's/logfile ""/logfile "access.log"' conf/redis.conf
sed -i 's/# requirepass foobared/requirepass 123456' conf/redis.conf
sed -i 's/appendonly no/appendonly yes' conf/redis.conf

# 这里可能还需配置一些 bind protected-mode
```

### Redis运行

```bash
# mac
docker run -d \
-v ~/docker/redis/conf/redis.conf:/etc/redis/conf/redis.conf \
-v ~/docker/redis/data:/data \
--name redis-6379 \
--restart always \
-p 6379:6379 \
--requirepass "password" \
redis

# centos 最后是带密码的访问方式
docker run -d \
-v /srv/redis/conf/redis.conf:/etc/redis/conf/redis.conf \
-v /srv/redis/data:/data \
--name redis-6379 \
--restart always \
-p 6379:6379 \
redis redis-server --appendonly yes --requirepass "htredis"

docker run –name redis6700 \
–privileged=true \
-p 6700:6379 \
-v $PWD/data:/data \
-v $PWD/redis.log:/var/log/redis/redis.log \
-d redis redis-server /data/redis.conf


docker run \
--name redis-test \
-p 6379:6379 -d \
--restart=always \
redis:latest redis-server --appendonly yes --requirepass "your password"
# -p 6379:6379 :将容器内端口映射到宿主机端口(右边映射到左边) 
# redis-server –appendonly yes : 在容器执行redis-server启动命令，并打开redis持久化配置 
# requirepass “your passwd” :设置认证密码 
# –restart=always : 随docker启动而启动

```
### 外部访问容器服务

```bash
# redis-cli 访问
docker run -it --link myredis:redis --rm redis redis-cli -h redis -p 6379
# -it 交互的虚拟终端
# --rm 退出是删除此容器
```

## Redis 主从配置

### 查看MasterId

```bash
docker inspect redis 
#Networks 可以得到 redis master 的 ip 地址

# 修改 redis-slave 的配置文件
# 主地址
slaveof master-ip master-port
# 主认证
masterauth
```

```bash
docker inspect redis #Networks
可以得到 redis master 的 ip 地址
"NetworkSettings": {
            "Ports": {
                "6379/tcp": [
                    {
                        "HostIp": "0.0.0.0",
                        "HostPort": "6379"
                    }
                ]
            },
            ...
            "Gateway": "192.168.0.1",
            ...
            "IPAddress": "192.168.0.3",#ip
            ...
            "Networks": {
                "bridge": {
                    ...
                    "Gateway": "192.168.0.1",
                    "IPAddress": "192.168.0.3",#ip
                    ...
                }
            }
        }


修改 redis-slave 的配置文件
# 主地址
slaveof master-ip master-port
# 主认证
masterauth

```

## Redis.conf 的参数配置说明

序号|参数名|参数值|说明
--|--|--|--
1.|daemonize| no|Redis默认不是以守护进程的方式运行，可以通过该配置项修改，使用yes启用守护进程
2.|pidfile |/var/run/redis.pid|当Redis以守护进程方式运行时，Redis默认会把pid写入/var/run/redis.pid文件，可以通过pidfile指定
3.|port |6379|指定Redis监听端口，默认端口为6379，作者在自己的一篇博文中解释了为什么选用6379作为默认端口，因为6379在手机按键上MERZ对应的号码，而MERZ取自意大利歌女Alessia Merz的名字
4.| bind |127.0.0.1|绑定的主机地址
5.| timeout |300|当 客户端闲置多长时间后关闭连接，如果指定为0，表示关闭该功能
6.|loglevel |verbose|指定日志记录级别，Redis总共支持四个级别：debug、verbose、notice、warning，默认为verbose
7.|logfile |stdout|日志记录方式，默认为标准输出，如果配置Redis为守护进程方式运行，而这里又配置为日志记录方式为标准输出，则日志将会发送给/dev/null
8.|databases |16|设置数据库的数量，默认数据库为0，可以使用SELECT \<dbid>命令在连接上指定数据库id
9.|save |900 1|指定在多长时间内，有多少次更新操作，就将数据同步到数据文件，可以多个条件配合 save \<seconds> \<changes> Redis默认配置文件中提供了三个条件：save 900 1 save 300 10 save 60 10000 分别表示900秒（15分钟）内有1个更改，300秒（5分钟）内有10个更改以及60秒内有10000个更改。
10.|rdbcompression | yes|指定存储至本地数据库时是否压缩数据，默认为yes，Redis采用LZF压缩，如果为了节省CPU时间，可以关闭该选项，但会导致数据库文件变的巨大
11.|dbfilename |dump.rdb|指定本地数据库文件名，默认值为dump.rdb
12.|dir |./|指定本地数据库存放目录
13.|slaveof | \<masterip> \<masterport> | 设置当本机为slav服务时，设置master服务的IP地址及端口，在Redis启动时，它会自动从master进行数据同步
14.|masterauth |<master-password>|当master服务设置了密码保护时，slav服务连接master的密码
15.|requirepass |foobared|设置Redis连接密码，如果配置了连接密码，客户端在连接Redis时需要通过AUTH \<password>命令提供密码，默认关闭
16.|maxclients |128|设置同一时间最大客户端连接数，默认无限制，Redis可以同时打开的客户端连接数为Redis进程可以打开的最大文件描述符数，如果设置 maxclients 0，表示不作限制。当客户端连接数到达限制时，Redis会关闭新的连接并向客户端返回max number of clients reached错误信息
17.|maxmemory | \<bytes>|指定Redis最大内存限制，Redis在启动时会把数据加载到内存中，达到最大内存后，Redis会先尝试清除已到期或即将到期的Key，当此方法处理 后，仍然到达最大内存设置，将无法再进行写入操作，但仍然可以进行读取操作。Redis新的vm机制，会把Key存放内存，Value会存放在swap区
18.|appendonly| no|指定是否在每次更新操作后进行日志记录，Redis在默认情况下是异步的把数据写入磁盘，如果不开启，可能会在断电时导致一段时间内的数据丢失。因为 redis本身同步数据文件是按上面save条件来同步的，所以有的数据会在一段时间内只存在于内存中。默认为no
19.|appendfilename |appendonly.aof|指定更新日志文件名，默认为appendonly.aof
20.|appendfsync |everysec|指定更新日志条件，共有3个可选值： <br/> no：表示等操作系统进行数据缓存同步到磁盘（快） always：表示每次更新操作后手动调用fsync()将数据写到磁盘（慢，安全） <br/> everysec：表示每秒同步一次（折中，默认值）
21.|vm-enabled| no|指定是否启用虚拟内存机制，默认值为no，简单的介绍一下，VM机制将数据分页存放，由Redis将访问量较少的页即冷数据swap到磁盘上，访问多的页面由磁盘自动换出到内存中（在后面的文章我会仔细分析Redis的VM机制）
22.|vm-swap-file |/tmp/redis.swap|虚拟内存文件路径，默认值为/tmp/redis.swap，不可多个Redis实例共享
23.|vm-max-memory |0|将所有大于vm-max-memory的数据存入虚拟内存,无论vm-max-memory设置多小,所有索引数据都是内存存储的(Redis的索引数据 就是keys),也就是说,当vm-max-memory设置为0的时候,其实是所有value都存在于磁盘。默认值为0
24.| vm-page-size |32|Redis swap文件分成了很多的page，一个对象可以保存在多个page上面，但一个page上不能被多个对象共享，vm-page-size是要根据存储的 数据大小来设定的，作者建议如果存储很多小对象，page大小最好设置为32或者64bytes；如果存储很大大对象，则可以使用更大的page，如果不 确定，就使用默认值
25.|vm-pages |134217728|设置swap文件中的page数量，由于页表（一种表示页面空闲或使用的bitmap）是在放在内存中的，，在磁盘上每8个pages将消耗1byte的内存。
26.|vm-max-threads |4|设置访问swap文件的线程数,最好不要超过机器的核数,如果设置为0,那么所有对swap文件的操作都是串行的，可能会造成比较长时间的延迟。默认值为4
27.|glueoutputbuf |yes|设置在向客户端应答时，是否把较小的包合并为一个包发送，默认为开启
28.|hash-max-zipmap-value |512|指定在超过一定的数量或者最大的元素超过某一临界值时，采用一种特殊的哈希算法
29.|hash-max-zipmap-entries |64|指定在超过一定的数量或者最大的元素超过某一临界值时，采用一种特殊的哈希算法
30.|activerehashing |yes|指定是否激活重置哈希，默认为开启（后面在介绍Redis的哈希算法时具体介绍）
31.|include |/path/to/local.conf|指定包含其它的配置文件，可以在同一主机上多个Redis实例之间使用同一份配置文件，而同时各个实例又拥有自己的特定配置文件

### Redis 客户端连接


Redis 命令
Redis 命令用于在 redis 服务上执行操作。

要在 redis 服务上执行命令需要一个 redis 客户端。Redis 客户端在我们之前下载的的 redis 的安装包中。

##### 语法
Redis 客户端的基本语法为：

```sh
$ redis-cli
```
实例
以下实例讲解了如何启动 redis 客户端：

启动 redis 客户端，打开终端并输入命令 redis-cli。该命令会连接本地的 redis 服务。
```sh
$redis-cli
redis 127.0.0.1:6379>
redis 127.0.0.1:6379> PING

PONG
```
在以上实例中我们连接到本地的 redis 服务并执行 PING 命令，该命令用于检测 redis 服务是否启动。

在远程服务上执行命令
如果需要在远程 redis 服务上执行命令，同样我们使用的也是 redis-cli 命令。

语法
```sh
$ redis-cli -h host -p port -a password
```
实例
以下实例演示了如何连接到主机为 127.0.0.1，端口为 6379 ，密码为 mypass 的 redis 服务上。
```sh
$redis-cli -h 127.0.0.1 -p 6379 -a "mypass"
redis 127.0.0.1:6379>
redis 127.0.0.1:6379> PING

PONG
```