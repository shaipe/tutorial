


### Vsftpd

```bash
docker run -d \
-v /mnt/60G/docker/ftp:/home/vsftpd \
-p 20:20 -p 21:21 -p 21100-21110:21100-21110 \
--name vsftpd \
--restart=always \
-e FTP_USER=xp \
-e FTP_PASS=myxiepeng \
-e PASV_ADDRESS=127.0.0.1 \
-e PASV_MIN_PORT=21100 \
-e PASV_MAX_PORT=21110 \
fauria/vsftpd
```

### Portainer

```bash
docker run -itd \
-p 9000:9000 \
--name portainer \
--restart always \
-v /var/run/docker.sock:/var/run/docker.sock \
-v /srv/portainer/data:/data \
portainer/portainer
```

### xfs

```bash
docker run -itd \
--name xfs \
-p 8085:8085 \
-v /mnt/60G/docker/xfs:/usr/src \
-w /usr/src \
--restart=always \
shaipe/ubuntu ./xfs_server
```

### nginx

```bash
docker run \
--publish 80:80 \
--publish 443:443 \
--name nginx \
-v /mnt/60G/docker/nginx/log/:/var/log/nginx \
-v /mnt/60G/docker/nginx/html:/usr/share/nginx/html \
-v /mnt/60G/docker/nginx/conf/nginx.conf:/etc/nginx/nginx.conf \
-v /mnt/60G/docker/nginx/conf/conf.d:/etc/nginx/conf.d \
-v /mnt/60G/docker/nginx/conf:/etc/nginx \
--restart always \
-d nginx
```