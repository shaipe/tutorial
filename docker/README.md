
<p align="center">
<img width="130" align="center" src="../img/logo.svg"/>
</p>
<h1 align="center">Docker教程</h1>


Docker 容器是一个开源的应用容器引擎，让开发者可以打包他们的应用以及依赖包到一个可移植的容器中，然后发布到任何流行的Linux机器上，也可以实现虚拟化。容器是完全使用沙箱机制，相互之间不会有任何接口（类似 iPhone 的 app）。几乎没有性能开销,可以很容易地在机器和数据中心中运行。最重要的是,他们不依赖于任何语言、框架包括系统。

- Docker并不是全能的，设计之初也不是KVM之类虚拟化手段的替代品，简单总结几点：

    - 1）Docker是基于Linux 64bit的，无法在32bit的linux/Windows/unix环境下使用。
    - 2）LXC是基于Cgroup等Linux kernel功能的，因此Container的Guest系统只能是Linux base的。
    - 3）隔离性相比KVM之类的虚拟化方案还是有些欠缺，所有container公用一部分的运行库。网络管理相对简单，主要是基于namespace隔离。
    - 4）cgroup的cpu和cpuset提供的cpu功能相比KVM的等虚拟化方案相比难以度量(所以dotcloud主要是按内存收费)。
    - 5）docker对disk的管理比较有限。
    - 6）container随着用户进程的停止而销毁，container中的log等用户数据不便收集。

目录
===
- [安装Docker](#安装Docker)
  - [Docker在Centos中使用](#Docker在Centos中使用)
  -[Docker在Windows中使用](#Docker在Windows中使用)
  -[设置开机自启动](#设置开机自启动)
- [Docker使用](#Docker使用)
  - [Docker中安装镜像](#Docker中安装镜像)
  - [Docker中运行容器](#Docker中运行容器)
- [容器管理](#容器管理)
  - [容器服务管理](#容器服务管理)
  - [进入容器](#进入容器)
  - [查看Docker容器日志](#查看Docker容器日志)
- [文件拷贝](#文件拷贝)
- [参考资料](#参考资料)
  - [官方英文资源](#官方英文资源)
  - [中文资源](#中文资源)
  - [其他资源](#其他资源)

## 安装Docker

### Docker在Centos中使用

官方网站：[https://docs.docker.com/engine/installation/linux/docker-ce/centos/](https://docs.docker.com/engine/installation/linux/docker-ce/centos/)

#### 第一种方式
```bash
curl -fsSL https://get.docker.com -o get-docker.sh
$ sudo sh get-docker.sh
```

#### 第二种安装方式



##### 1. 卸载老版本Docker
```sh
sudo yum remove docker \
              docker-common \
              docker-selinux \
              docker-engine
```

##### 2. 设置仓库
```sh
sudo yum install -y yum-utils \
  device-mapper-persistent-data \
  lvm2

sudo yum-config-manager \
    --add-repo \
    https://download.docker.com/linux/centos/docker-ce.repo
```

##### 3. 安装Docker
```sh
sudo yum install docker-ce
```

##### 4. 启动Docker
```sh
sudo systemctl start docker
```

##### 5. 卸载Docker CE
```sh
sudo yum remove docker-ce 
sudo rm -rf /var/lib/docker
```

### Docker在Windows中使用

1. 在windows中启动好之后 docker pull xxx 报错如下错误解决办法

no matching manifest for unknown in the manifest list enties;

是因为docker的配置文件中的: experimental: false导致

docker在windows下的配置目录为: c:\programdata\docker\config\dadmon.json

修改: experimental: true

通过 docker info 查看是否配置成功

### 设置开机自启动

```bash
systemctl enable docker

Created symlink from /etc/systemd/system/multi-user.target.wants/docker.service to /usr/lib/systemd/system/docker.service.
```

## Docker使用

### Docker中安装镜像

```bash
docker pull 镜像名
# 例如:
# 搜索镜像
docker search keywords // 例如搜索jumpserver
docker pull nginx
```

## Docker中运行容器

**注意:**
在使用run -d 参数让容器在后台运行的时候,部分基础容器需要配合-i -t 一起使用,否则会出容器一直Restarting的问题

```bash
# 将镜像启动为容器
# -i 表示容器的标准输入打开 
# -t 表示分配的伪终端
# -d 表示后台启动
docker run ...

# 查看已启动的docker列表
docker ps 
# 删除镜像
docker rmi aming_centos:235123
# 进入docker
docker attach 实例名或id
# 将镜像导出为tar包
docker save -o /opt/docker-centos-httpd.tar centos:httpd
# 将tar包导入为本地镜像
docker load -i /opt/docker-centos-httpd.tar
# 将镜像centos:httpd运行为容器，将80端口映射到本机的9000，启动httpd
docker run -d -p 9000:80 centos:httpd /bin/sh -c /usr/local/bin/start.sh

# 镜像启动为容器之后，进入一个启动的容器
docker exec [OPTIONS] CONTAINER COMMAND [ARG...] [flags]

# 支持systemctl
docker run --privileged -d -p 10080:80 centos /sbin/init 


```

## 容器管理

容器就像一个类的实例

```bash
# 列出本机正在运行的容器
docker container ls
# 列出本机所有容器，包括终止运行的容器
docker container ls --all
docker start [containerID/Names] # 启动容器
docker stop [containerID/Names]  # 停止容器
docker rm [containerID/Names]    # 删除容器
docker logs [containerID/Names]  # 查看日志
docker exec -it [containerID/Names] /bin/bash  # 进入容器

# 从正在运行的 Docker 容器里面，将文件拷贝到本机，注意后面有个【点】拷贝到当前目录
docker container cp [containID]:[/path/to/file] .

docker run centos echo "hello world"  # 在docker容器中运行hello world!
docker run centos yum install -y wget # 在docker容器中，安装wget软件
docker ps                           # 列出包括未运行的容器
docker ps -a                        # 查看所有容器(包括正在运行和已停止的)
docker logs my-nginx                # 查看 my-nginx 容器日志

docker run -i -t centos /bin/bash   # 启动一个容器
docker inspect centos     # 检查运行中的镜像
docker commit 8bd centos  # 保存对容器的修改
docker commit -m "n changed" my-nginx my-nginx-image # 使用已经存在的容器创建一个镜像
docker inspect -f {{.State.Pid}} 44fc0f0582d9 # 获取id为 44fc0f0582d9 的PID进程编号
# 下载指定版本容器镜像
docker pull gitlab/gitlab-ce:11.2.3-ce.0

# 获取关联的镜像
docker image inspect --format='{{.RepoTags}} {{.Id}} {{.Parent}}' $(docker image ls -q --filter since=xxxx)
```

### 容器服务管理

```bash
docker run -itd --name my-nginx2 nginx # 通过nginx镜像，【创建】容器名为 my-nginx2 的容器
docker start my-nginx --restart=always    # 【启动策略】一个已经存在的容器启动添加策略
                               # no - 容器不重启
                               # on-failure - 容器推出状态非0时重启
                               # always - 始终重启
docker start my-nginx               # 【启动】一个已经存在的容器
docker restart my-nginx             # 【重启】容器
docker stop my-nginx                # 【停止运行】一个容器
docker kill my-nginx                # 【杀死】一个运行中的容器
docker rename my-nginx new-nginx    # 【重命名】容器
docker rm new-nginx                 # 【删除】容器
```

### 进入容器

1. 创建一个守护状态的Docker容器

```bash
docker run -itd my-nginx /bin/bash
```

2. 使用`docker ps`查看到该容器信息

```bash
docker ps
# CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS              PORTS               NAMES
# 6bd0496da64f        nginx               "/bin/bash"         20 seconds ago      Up 18 seconds       80/tcp              high_shirley
```

3. 使用`docker exec`命令进入一个已经在运行的容器

```bash
docker exec -it 6bd0496da64f /bin/bash
```

通常有下面几种方式进入Docker的容器，推荐使用`exec`，使用`attach`一直进入失败。

- 使用`docker attach`
- 使用`SSH` [为什么不需要在 Docker 容器中运行 sshd](http://www.oschina.net/translate/why-you-dont-need-to-run-sshd-in-docker?cmp)
- 使用`nsenter`进入Docker容器，[nsenter官方仓库](https://github.com/jpetazzo/nsenter)
- 使用`docker exec`，在`1.3.*`之后提供了一个新的命令`exec`用于进入容器

### 查看Docker容器日志

```
docker logs [OPTIONS] CONTAINER
  Options:
        --details        显示更多的信息
    -f, --follow         跟踪实时日志
        --since string   显示自某个timestamp之后的日志，或相对时间，如42m（即42分钟）
        --tail string    从日志末尾显示多少行日志， 默认是all
    -t, --timestamps     显示时间戳
        --until string   显示自某个timestamp之前的日志，或相对时间，如42m（即42分钟）
```

**示例**
```bash
# 查看指定时间后的日志，只显示最后100行：
docker logs -f -t --since="2018-02-08" --tail=100 CONTAINER_ID

# 查看最近30分钟的日志:
docker logs --since 30m CONTAINER_ID

# 查看某时间之后的日志：
docker logs -t --since="2018-02-08T13:23:37" CONTAINER_ID

# 查看某时间段日志：
docker logs -t --since="2018-02-08T13:23:37" --until "2018-02-09T12:23:37" CONTAINER_ID
```

## 文件拷贝

从主机复制到容器 `sudo docker cp host_path containerID:container_path`  
从容器复制到主机 `sudo docker cp containerID:container_path host_path`


## 参考资料

### 官方英文资源

- Docker官网：http://www.docker.com
- Docker windows入门：https://docs.docker.com/windows/
- Docker Linux 入门：https://docs.docker.com/linux/
- Docker mac 入门：https://docs.docker.com/mac/
- Docker 用户指引：https://docs.docker.com/engine/userguide/
- Docker 官方博客：http://blog.docker.com/
- Docker Hub: https://hub.docker.com/
- Docker开源： https://www.docker.com/open-source

### 中文资源

- Docker中文网站：http://www.docker.org.cn
- Docker中文文档：http://www.dockerinfo.net/document
- Docker安装手册：http://www.docker.org.cn/book/install.html
- 一小时Docker教程 ：https://blog.csphere.cn/archives/22
- Docker中文指南：http://www.widuu.com/chinese_docker/index.html

### 其它资源

- [Docker 快速手册！](https://github.com/eon01/DockerCheatSheet)
- [Docker 教程](http://www.runoob.com/docker/docker-tutorial.html)
- [Docker 从入门到实践](https://www.gitbook.com/book/yeasy/docker_practice)
- [MySQL Docker 单一机器上如何配置自动备份](http://blog.csdn.net/zhangchao19890805/article/details/52756865)
- [使用Docker Compose管理多个容器](http://dockone.io/article/834)
- https://segmentfault.com/t/docker
- https://github.com/docker/docker
- https://wiki.openstack.org/wiki/Docker
- https://wiki.archlinux.org/index.php/Docker

## Installation
- [Linux-Install](linux-install.md)
- [docker compose](./compose.md)


### 官方文档

- [Centos](https://docs.docker.com/install/linux/docker-ce/centos/)
- [Debian](https://docs.docker.com/install/linux/docker-ce/debian/)
- [Ubuntu](https://docs.docker.com/engine/install/ubuntu/)
- [fedora](https://docs.docker.com/install/linux/docker-ce/fedora/)
- [MacOS](https://docs.docker.com/docker-for-mac/install/)
- [Windows](https://docs.docker.com/docker-for-windows/install/)