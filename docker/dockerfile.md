# DockerFile

Dockerfile是由一系列命令和参数构成的脚本，这些命令应用于基础镜像并最终创建一个新的镜像。

## 指令详解

### FROM

```bash
FROM <image>
```
FROM指定构建镜像的基础源镜像，如果本地没有指定的镜像，则会自动从 Docker 的公共库 pull 镜像下来。
FROM必须是 Dockerfile 中非注释行的第一个指令，即一个 Dockerfile 从FROM语句开始。
如果FROM语句没有指定镜像标签，则默认使用latest标签。
FROM可以在一个 Dockerfile 中出现多次，如果有需求在一个 Dockerfile 中创建多个镜像。

### MAINTAINER
```bash
MAINTAINER <name>
```
指定创建镜像的用户

### RUN
```bash
RUN "executable", "param1", "param2"
```
每条RUN指令将在当前镜像基础上执行指定命令，并提交为新的镜像，后续的RUN都在之前RUN提交后的镜像为基础，镜像是分层的，可以通过一个镜像的任何一个历史提交点来创建，类似源码的版本控制。

### CMD 容器启动指令

CMD的目的是为了在启动容器时提供一个默认的命令执行选项。如果用户启动容器时指定了运行的命令，则会覆盖掉CMD指定的命令。
CMD指定在 Dockerfile 中只能使用一次，如果有多个，则只有最后一个会生效。
```bash
CMD ["executable","param1","param2"]
CMD ["param1","param2"]
CMD command param1 param2 (shell form)
```
RUN 和CMD的区别：
CMD会在启动容器的时候执行，build 时不执行。
RUN只是在构建镜像的时候执行

### EXPOSE 暴露端口
```bash
EXPOSE <port> [<port>...]
```
告诉 Docker 服务端容器对外映射的本地端口，需要在 docker run 的时候使用-p或者-P选项生效

### ENV 设置环境变量
```bash
ENV <key> <value>       # 只能设置一个变量
ENV <key>=<value> ...   # 允许一次设置多个变量
```
指定一个环境变量，会被后续RUN指令使用，可以在容器内被脚本或者程序调用。

### COPY 复制文件
```bash
COPY <src>... <dest>
COPY ["<src>"..."<dest>"]
```
COPY 和 ``RUN`` 
COPY复制新文件或者目录到目标容器指定路径中 。
用法和功能同ADD，区别在于不能用URL，ADD功能更强大些。

### ADD 复制文件
```bash
ADD <src>... <dest>
```
ADD复制本地主机文件、目录到目标容器的文件系统中。
如果源是一个URL，该URL的内容将被下载并复制到目标容器中。


### ENTRYPOINT 入口点
```bash
ENTRYPOINT ["executable", "param1", "param2"]
ENTRYPOINT command param1 param2 (shell form)
```
配置容器启动后执行的命令，并且不可被 docker run 提供的参数覆盖，而CMD是可以被覆盖的。如果需要覆盖，则可以使用docker run --entrypoint选项。
每个 Dockerfile 中只能有一个ENTRYPOINT，当指定多个时，只有最后一个生效。

疑问: ENTRYPOINT 和 CMD 可同时存在吗？
测试结果：可以的。
两者使用场景：
ENTRYPOINT 用于稳定-不被修改的执行命令。
CMD 用于 可变的命令。

### VOLUME 定义匿名卷
```bash
VOLUME ["/data"]
```
将本地主机目录挂载到目标容器中
将其他容器挂载的挂载点 挂载到目标容器中

### USER 指定当前用户
```bash
USER mysql
```
指定运行容器时的用户名或 UID，
在这之后的命令如RUN、CMD、ENTRYPOINT也会使用指定用户

### WORKDIR 指定工作目录
```bash
WORKDIR /path/to/workdir
```
切换目录，相当于cd

### ONBUILD 为他人服务项

```bash
ONBUILD [INSTRUCTION]
```
使用该dockerfile生成的镜像A，并不执行ONBUILD中命令
如再来个dockerfile 基础镜像为镜像A时，生成的镜像B时就会执行ONBUILD中的命令

## 示例

```shell

FROM centos:latest
MAINTAINER lixr
RUN yum -y update
RUN yum -y install systemd systemd-libs
RUN yum clean all;
VOLUME [ "/sys/fs/cgroup" ]
CMD ["/usr/sbin/init"]

```




如何编写最佳的Dockerfile
译者按: Dockerfile的语法非常简单，然而如何加快镜像构建速度，如何减少Docker镜像的大小却不是那么直观，需要积累实践经验。这篇博客可以帮助你快速掌握编写Dockerfile的技巧。

原文: How to write excellent Dockerfiles

译者: Fundebug

为了保证可读性，本文采用意译而非直译。另外，本文版权归原作者所有，翻译仅用于学习。

我已经使用Docker有一段时间了，其中编写Dockerfile是非常重要的一部分工作。在这篇博客中，我打算分享一些建议，帮助大家编写更好的Dockerfile。

目标:
更快的构建速度
更小的Docker镜像大小
更少的Docker镜像层
充分利用镜像缓存
增加Dockerfile可读性
让Docker容器使用起来更简单
总结
编写.dockerignore文件
容器只运行单个应用
将多个RUN指令合并为一个
基础镜像的标签不要用latest
每个RUN指令后删除多余文件
选择合适的基础镜像(alpine版本最好)
设置WORKDIR和CMD
使用ENTRYPOINT (可选)
在entrypoint脚本中使用exec
COPY与ADD优先使用前者
合理调整COPY与RUN的顺序
设置默认的环境变量，映射端口和数据卷
使用LABEL设置镜像元数据
添加HEALTHCHECK
示例
示例Dockerfile犯了几乎所有的错(当然我是故意的)。接下来，我会一步步优化它。假设我们需要使用Docker运行一个Node.js应用，下面就是它的Dockerfile(CMD指令太复杂了，所以我简化了，它是错误的，仅供参考)。

FROM ubuntu

ADD . /app

RUN apt-get update  
RUN apt-get upgrade -y  
RUN apt-get install -y nodejs ssh mysql  
RUN cd /app && npm install

# this should start three processes, mysql and ssh
# in the background and node app in foreground
# isn't it beautifully terrible? <3
CMD mysql & sshd & npm start
构建镜像:

docker build -t wtf .
1. 编写.dockerignore文件
构建镜像时，Docker需要先准备context ，将所有需要的文件收集到进程中。默认的context包含Dockerfile目录中的所有文件，但是实际上，我们并不需要.git目录，node_modules目录等内容。 .dockerignore 的作用和语法类似于 .gitignore，可以忽略一些不需要的文件，这样可以有效加快镜像构建时间，同时减少Docker镜像的大小。示例如下:

.git/
node_modules/
2. 容器只运行单个应用
从技术角度讲，你可以在Docker容器中运行多个进程。你可以将数据库，前端，后端，ssh，supervisor都运行在同一个Docker容器中。但是，这会让你非常痛苦:

非常长的构建时间(修改前端之后，整个后端也需要重新构建)
非常大的镜像大小
多个应用的日志难以处理(不能直接使用stdout，否则多个应用的日志会混合到一起)
横向扩展时非常浪费资源(不同的应用需要运行的容器数并不相同)
僵尸进程问题 - 你需要选择合适的init进程
因此，我建议大家为每个应用构建单独的Docker镜像，然后使用 Docker Compose 运行多个Docker容器。

现在，我从Dockerfile中删除一些不需要的安装包，另外，SSH可以用docker exec替代。示例如下：

FROM ubuntu

ADD . /app

RUN apt-get update  
RUN apt-get upgrade -y

# we should remove ssh and mysql, and use
# separate container for database 
RUN apt-get install -y nodejs  # ssh mysql  
RUN cd /app && npm install

CMD npm start
3. 将多个RUN指令合并为一个
Docker镜像是分层的，下面这些知识点非常重要:

Dockerfile中的每个指令都会创建一个新的镜像层。
镜像层将被缓存和复用
当Dockerfile的指令修改了，复制的文件变化了，或者构建镜像时指定的变量不同了，对应的镜像层缓存就会失效
某一层的镜像缓存失效之后，它之后的镜像层缓存都会失效
镜像层是不可变的，如果我们再某一层中添加一个文件，然后在下一层中删除它，则镜像中依然会包含该文件(只是这个文件在Docker容器中不可见了)。
Docker镜像类似于洋葱。它们都有很多层。为了修改内层，则需要将外面的层都删掉。记住这一点的话，其他内容就很好理解了。

现在，我们将所有的RUN指令合并为一个。同时把apt-get upgrade删除，因为它会使得镜像构建非常不确定(我们只需要依赖基础镜像的更新就好了)

FROM ubuntu

ADD . /app

RUN apt-get update \  
    && apt-get install -y nodejs \
    && cd /app \
    && npm install

CMD npm start
记住一点，我们只能将变化频率一样的指令合并在一起。将node.js安装与npm模块安装放在一起的话，则每次修改源代码，都需要重新安装node.js，这显然不合适。因此，正确的写法是这样的:

FROM ubuntu

RUN apt-get update && apt-get install -y nodejs  
ADD . /app  
RUN cd /app && npm install

CMD npm start
4. 基础镜像的标签不要用latest
当镜像没有指定标签时，将默认使用latest 标签。因此， FROM ubuntu 指令等同于FROM ubuntu:latest。当时，当镜像更新时，latest标签会指向不同的镜像，这时构建镜像有可能失败。如果你的确需要使用最新版的基础镜像，可以使用latest标签，否则的话，最好指定确定的镜像标签。

示例Dockerfile应该使用16.04作为标签。

FROM ubuntu:16.04  # it's that easy!

RUN apt-get update && apt-get install -y nodejs  
ADD . /app  
RUN cd /app && npm install

CMD npm start
5. 每个RUN指令后删除多余文件
假设我们更新了apt-get源，下载，解压并安装了一些软件包，它们都保存在/var/lib/apt/lists/目录中。但是，运行应用时Docker镜像中并不需要这些文件。我们最好将它们删除，因为它会使Docker镜像变大。

示例Dockerfile中，我们可以删除/var/lib/apt/lists/目录中的文件(它们是由apt-get update生成的)。

FROM ubuntu:16.04

RUN apt-get update \  
    && apt-get install -y nodejs \
    # added lines
    && rm -rf /var/lib/apt/lists/*

ADD . /app  
RUN cd /app && npm install

CMD npm start
6. 选择合适的基础镜像(alpine版本最好)
在示例中，我们选择了ubuntu作为基础镜像。但是我们只需要运行node程序，有必要使用一个通用的基础镜像吗？node镜像应该是更好的选择。

FROM node

ADD . /app  
# we don't need to install node 
# anymore and use apt-get
RUN cd /app && npm install

CMD npm start
更好的选择是alpine版本的node镜像。alpine是一个极小化的Linux发行版，只有4MB，这让它非常适合作为基础镜像。

FROM node:7-alpine

ADD . /app  
RUN cd /app && npm install

CMD npm start
apk是Alpine的包管理工具。它与apt-get有些不同，但是非常容易上手。另外，它还有一些非常有用的特性，比如no-cache和 --virtual选项，它们都可以帮助我们减少镜像的大小。

7. 设置WORKDIR和 CMD
WORKDIR指令可以设置默认目录，也就是运行RUN / CMD / ENTRYPOINT指令的地方。

CMD指令可以设置容器创建是执行的默认命令。另外，你应该讲命令写在一个数组中，数组中每个元素为命令的每个单词(参考官方文档)。

FROM node:7-alpine

WORKDIR /app  
ADD . /app  
RUN npm install

CMD ["npm", "start"]
8. 使用ENTRYPOINT (可选)
ENTRYPOINT指令并不是必须的，因为它会增加复杂度。ENTRYPOINT是一个脚本，它会默认执行，并且将指定的命令错误其参数。它通常用于构建可执行的Docker镜像。entrypoint.sh如下:

#!/usr/bin/env sh
# $0 is a script name, 
# $1, $2, $3 etc are passed arguments
# $1 is our command
CMD=$1

case "$CMD" in  
  "dev" )
    npm install
    export NODE_ENV=development
    exec npm run dev
    ;;

  "start" )
    # we can modify files here, using ENV variables passed in 
    # "docker create" command. It can't be done during build process.
    echo "db: $DATABASE_ADDRESS" >> /app/config.yml
    export NODE_ENV=production
    exec npm start
    ;;

   * )
    # Run custom command. Thanks to this line we can still use 
    # "docker run our_image /bin/bash" and it will work
    exec $CMD ${@:2}
    ;;
esac
示例Dockerfile:

FROM node:7-alpine

WORKDIR /app  
ADD . /app  
RUN npm install

ENTRYPOINT ["./entrypoint.sh"]  
CMD ["start"]
可以使用如下命令运行该镜像:

# 运行开发版本
docker run our-app dev 

# 运行生产版本
docker run our-app start 

# 运行bash
docker run -it our-app /bin/bash
9. 在entrypoint脚本中使用exec
在前文的entrypoint脚本中，我使用了exec命令运行node应用。不使用exec的话，我们则不能顺利地关闭容器，因为SIGTERM信号会被bash脚本进程吞没。exec命令启动的进程可以取代脚本进程，因此所有的信号都会正常工作。

10. COPY与ADD优先使用前者
COPY指令非常简单，仅用于将文件拷贝到镜像中。ADD相对来讲复杂一些，可以用于下载远程文件以及解压压缩包(参考官方文档)。

FROM node:7-alpine

WORKDIR /app

COPY . /app  
RUN npm install

ENTRYPOINT ["./entrypoint.sh"]  
CMD ["start"]
11. 合理调整COPY与RUN的顺序
我们应该把变化最少的部分放在Dockerfile的前面，这样可以充分利用镜像缓存。

示例中，源代码会经常变化，则每次构建镜像时都需要重新安装NPM模块，这显然不是我们希望看到的。因此我们可以先拷贝package.json，然后安装NPM模块，最后才拷贝其余的源代码。这样的话，即使源代码变化，也不需要重新安装NPM模块。

FROM node:7-alpine

WORKDIR /app

COPY package.json /app  
RUN npm install  
COPY . /app

ENTRYPOINT ["./entrypoint.sh"]  
CMD ["start"]
12. 设置默认的环境变量，映射端口和数据卷
运行Docker容器时很可能需要一些环境变量。在Dockerfile设置默认的环境变量是一种很好的方式。另外，我们应该在Dockerfile中设置映射端口和数据卷。示例如下:

FROM node:7-alpine

ENV PROJECT_DIR=/app

WORKDIR $PROJECT_DIR

COPY package.json $PROJECT_DIR  
RUN npm install  
COPY . $PROJECT_DIR

ENV MEDIA_DIR=/media \  
    NODE_ENV=production \
    APP_PORT=3000

VOLUME $MEDIA_DIR  
EXPOSE $APP_PORT

ENTRYPOINT ["./entrypoint.sh"]  
CMD ["start"]
ENV指令指定的环境变量在容器中可以使用。如果你只是需要指定构建镜像时的变量，你可以使用ARG指令。

13. 使用LABEL设置镜像元数据
使用LABEL指令，可以为镜像设置元数据，例如镜像创建者或者镜像说明。旧版的Dockerfile语法使用MAINTAINER指令指定镜像创建者，但是它已经被弃用了。有时，一些外部程序需要用到镜像的元数据，例如nvidia-docker需要用到com.nvidia.volumes.needed。示例如下:

FROM node:7-alpine  
LABEL maintainer "jakub.skalecki@example.com"  
...
14. 添加HEALTHCHECK
运行容器时，可以指定--restart always选项。这样的话，容器崩溃时，Docker守护进程(docker daemon)会重启容器。对于需要长时间运行的容器，这个选项非常有用。但是，如果容器的确在运行，但是不可(陷入死循环，配置错误)用怎么办？使用HEALTHCHECK指令可以让Docker周期性的检查容器的健康状况。我们只需要指定一个命令，如果一切正常的话返回0，否则返回1。对HEALTHCHECK感兴趣的话，可以参考这篇博客。示例如下:

FROM node:7-alpine  
LABEL maintainer "jakub.skalecki@example.com"

ENV PROJECT_DIR=/app  
WORKDIR $PROJECT_DIR

COPY package.json $PROJECT_DIR  
RUN npm install  
COPY . $PROJECT_DIR

ENV MEDIA_DIR=/media \  
    NODE_ENV=production \
    APP_PORT=3000

VOLUME $MEDIA_DIR  
EXPOSE $APP_PORT  
HEALTHCHECK CMD curl --fail http://localhost:$APP_PORT || exit 1

ENTRYPOINT ["./entrypoint.sh"]  
CMD ["start"]
当请求失败时，curl --fail 命令返回非0状态。


使用Dockerfile构建Docker镜像
docker-logo.png

Docker中有个非常重要的概念叫做——镜像（Image）。Docker 镜像是一个特殊的文件系统，除了提供容器运行时所需的程序、库、资源、配置等文件外，还包含了一些为运行时准备的一些配置参数（如匿名卷、环境变量、用户等）。镜像不包含任何动态数据，其内容在构建之后也不会被改变。

镜像的定制实际上就是定制每一层所添加的配置、文件。如果我们可以把每一层修改、安装、构建、操作的命令都写入一个脚本，用这个脚本来构建、定制镜像，那么之前提及的无法重复的问题、镜像构建透明性的问题、体积的问题就都会解决。这个脚本就是 Dockerfile。

Dockerfile 是一个文本文件，其内包含了一条条的指令(Instruction)，每一条指令构建一层，因此每一条指令的内容，就是描述该层应当如何构建。

Dockerfile语法说明
1、FROM: 指定基础镜像。
定制镜像的时候都是以一个镜像为基础，在这个基础上面进行定制。FROM在Dockerfile中是必须的指令，而且必须是第一条指令。

1）在Docker Hub上有非常多的官方镜像，比如服务类(nginx/redis)、语言类(node/openjdk/python)、操作系统类(ubuntu/debian/centos)等，我们可以直接拿来使用。
2）除了选择现有的镜像作为基础镜像外，Docker还存在一个特殊的镜像，名为scratch。这个镜像是虚拟的概念，并不实际存在，它表示一个空白的镜像。
2、RUN: 执行命令
run指令是用来执行命令行命令的，由于命令行的强大能力，run指令在定制镜像时是最常用的指令之一。其格式有两种：

shell格式： RUN 命令，就像直接在命令行中输入的命令一样，如RUN echo 'hello, world!' > hello.txt
exec格式：RUN ['可执行文件', '参数1', '参数2']，类似于函数调用，将可执行文件和参数分开，如RUN [ "sh", "-c", "echo $HOME" ]
Dockerfile中每一个指令都会建立一层，RUN也不例外。每一个RUN的行为，就和刚才我们手工建立镜像的过程一样:新建立一层，在其上执行这些命令，执行结束后，commit这一层的修改，构成新的镜像。所以我们在使用的时候尽可能将指令进行整合（可以使用&&将各个所需命令串联起来）。

3、CMD：容器启动命令
CMD 指令的格式和 RUN 相似，也是两种格式：

shell 格式：CMD <命令>
exec 格式：CMD ["可执行文件", "参数1", "参数2"...]
在指定了 ENTRYPOINT 指令后，用 CMD 指定具体的参数。参数列表格式：CMD ["参数1", "参数2"...]

4、COPY：复制文件
和 RUN 指令一样，也有两种格式，一种类似于命令行，一种类似于函数调用：

COPY <源路径>... <目标路径>
COPY ["<源路径1>",... "<目标路径>"]
COPY 指令将从构建上下文目录中<源路径>的文件复制到新的一层的镜像内的<目标路径>位置。

5、ENV: 设置环境变量
格式有两种：

ENV <key> <value>
ENV <key1>=<value1> <key2>=<value2>...
这个指令很简单，就是设置环境变量而已，无论是后面的其它指令，如RUN，还是运行时的应用，都可以直接使用这里定义的环境变量。

ENV VERSION=1.0 DEBUG=on \
    NAME="Happy Feet"
这个例子中演示了如何换行，以及对含有空格的值用双引号括起来的办法，这和 Shell 下的行为是一致的。

6、EXPOSE: 声明端口
格式为 EXPOSE <端口1> [<端口2>...]。

EXPOSE指令是声明运行时容器提供服务端口，这只是一个声明，在运行时并不会因为这个声明应用就会开启这个端口的服务。在Dockerfile中写入这样的声明有两个好处，一个是帮助镜像使用者理解这个镜像服务的守护端口，以方便配置映射；另一个用处则是在运行时使用随机端口映射时，也就是docker run -P时，会自动随机映射EXPOSE的端口。

要将EXPOSE和在运行时使用-p <宿主端口>:<容器端口>区分开来。-p，是映射宿主端口和容器端口，换句话说，就是将容器的对应端口服务公开给外界访问，而EXPOSE仅仅是声明容器打算使用什么端口而已，并不会自动在宿主进行端口映射。

7、WORKDIR: 指定工作目录
格式为 WORKDIR <工作目录路径>。

使用 WORKDIR 指令可以来指定工作目录（或者称为当前目录），以后各层的当前目录就被改为指定的目录，如该目录不存在，WORKDIR 会帮你建立目录。

构建Node web服务镜像
docker-node-logo

1、使用Express创建一个示例项目
(1) 安装express应用生成器工具
npm install express-generator -g
(2) 创建示例项目
express myapp
2、创建.dockerignore文件
在myapp文件夹下面，新建一个.dockerignore的文件，相当于.gitignore，可以将一些不需要的文件进行忽略。示例代码如下：

node_modules/  
.git
.gitignore
.idea
.DS_Store
*.swp
*.log
3、创建Dockerfile文件
在myapp文件夹下面，新建Dockerfile文件，添加如下代码：

FROM daocloud.io/node:5
MAINTAINER hhtczengjing@gmail.com
ENV PORT 3000
COPY . /app
WORKDIR /app
RUN npm install --registry=https://registry.npm.taobao.org
EXPOSE 3000
CMD ["npm", "start"]
4、构建镜像
docker build -t myapp:1.0.0 .


在终端执行这行命令之后，使用docker images可以查看到当前所有的镜像：



5、创建容器
docker run -d -p 3000:3000 myapp:1.0.0
浏览器访问：http://localhost:3000， 如果出现下面的界面表示成功：



参考资料
1、《Dockerfie 官方文档》

2、《Dockerfile 最佳实践文档》

3、《入门实战：使用Docker构建一个nodejs服务》

4、《在Docker中运行Node.js的Web应用》

5、《Docker — 从入门到实践》