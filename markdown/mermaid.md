Markdown绘图
==========

### 介绍

采用markdwon对项目进行管理，采用MD的flow，mermaid， markdown的相关语法来编写项目文档

### 甘特图相关教程文档

`mermaid`：图中红框第一行内容与最后一行内容，表示当前使用的语言为mermaid。
`gantt`：表示图表类型为甘特图
`dateFormat`：指定日期格式（Y表示年，M表示月，D表示日）。YYYY-MM-DD（2109-03-14），YY-MM-DD(19-03-14)
`section`： 项目关键字，空格之后输入项目名称。
`任务行`： section后每一行为一个任务。第一个字符串为任务名称，之后以:开头，添加任务属性。
接下来用下面代码实例说明常见任务属性。

#### 任务状态：

``done`` 已完成
``active`` 正在进行
``crit`` 关键任务

默认任务都为待完成状态

`任务描述`：在des1、des2位置添加任务描述，其它任务引用时直接引用des1就可以。
`after`:  描述任务时间关系。des3, after des2表示des3紧跟在des2之后。

`任务时长`有三种方式：

时间范围 如：2020-10-06,2020-10-08
指定天数 如：5d
指定开始日期+天数 如：2020-10-06，5d

**PS: 所有关键字之间用 “,” 分隔，关键字需要属性时用空格分隔(如：after des2,)。


#### 示例

```mermaid
gantt         
       dateFormat  YYYY-MM-DD   
       title 使用mermaid语言定制甘特图
       section 任务1
       已完成的任务           :done,    des1, 2020-10-06,2020-10-08
       正在进行的任务               :active,  des2, 2020-10-09, 3d
       待完成任务1               :         des3, after des2, 5d
       待完成任务2              :         des4, after des3, 5d
```


#### 日常项目管理记录

```mermaid
gantt         
       dateFormat  YYYY-MM-DD   
       title 使用mermaid语言定制甘特图

       section 任务1
       已完成的任务           :done,    des1, 2020-10-06,2020-10-08
       正在进行的任务               :active,  des2, 2020-10-09, 3d
       待完成任务1               :         des3, after des2, 5d
       待完成任务2              :         des4, after des3, 5d

       section 关键任务
       已完成的关键任务 :crit, done, 2020-10-06,24h
       已完成的关键任务2         :crit, done, after des1, 2d
       正在进行的关键任务             :crit, active, 3d
       待完成的关键任务        :crit, 5d
       待完成任务           :2d
       待完成任务2                      :1d

       section 文档编写
       描述甘特图语法               :active, a1, after des1, 3d
       完成甘特图实例1      :after a1  , 20h
       完成甘特图实例2    :doc1, after a1  , 48h
```



#### 其他图形教程

#### kdkds

### sflja


##### 表格

<style>th{text-align:center}</style>
<table>
<thead>
<tr>
<th>A</th><th>B</th><th>C</th>
</tr>
</thead>
<tbody>
<tr>
<td>占位符</td><td colspan="2">合并了两列</td>
</tr>
<tr>
<td rowspan="2" colspan="2">合并了两行两列</td><td>$F(x)=x^2$</td>
</tr>
<tr>
<td><b>加粗</b></td>
</tr>
</tbody>
<table>

##### 结构图

```mermaid
graph TB
t-->k
t-->c
t-->x
t-->dds
```