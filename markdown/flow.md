# Markdown flow 流程图

markdown使用flow来画流程图 在markdown语法中，流程图的画法和代码段类似，也就是说，流程图是写在两个 ``` 之间的。 基本使用下面的六种类型，名“符”其实。

### 操作块

- start 开始
- end 结束
- operation 普通操作块
- subroutine 子任务块
- condition 判断块
- inputoutput 输入输出块

流程图的语法大体分为两段，第一段用来定义元素，第二段用来连接元素：

```flow
st=>start: Start
in=>inputoutput: input
e=>end: End
st->in
in->e
```

### 操作块说明
```
#st 是变量名，类似php中数组索引，可以用英文，别定义的太离谱就行，如op_this,cond_echo
#start 操作模块名，像数据类型 Int String，如：开始，结束，判断。命名严格，区别大小写。
# : 后面是要显示的文字
```

```yuml
// {type:activity}
// {generate:true}

(start)-><a>[kettle empty]->(Fill Kettle)->|b|
<a>[kettle full]->|b|->(Boil Kettle)->|c|
|b|->(Add Tea Bag)->(Add Milk)->|c|->(Pour Water)
(Pour Water)->(end)
```